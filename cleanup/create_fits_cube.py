#! /usr/bin/env python
import pyfits
from argparse import ArgumentParser
import numpy as np
import sys
import os

outdir="/net/node131/data/users/lofareor/NCP/images/L90490"
outname="L90490_cube.fits"



parser = ArgumentParser("Create cube of list of fitsfiles");
parser.add_argument('-i','--fitslist', nargs='+',help='fitsfiles (wildcards possible) or file with list of fitsfiles',dest="fitslist",default='')
parser.add_argument('-o','--outfile', help='name of the output cube',dest='outfile',default=outname)
parser.add_argument('-d','--outdir', help='directory for the output cube',dest='outdir',default=outdir)
parser.add_argument('-a','--axis', help='axis to concatenate',dest='axis',default="FREQ")
parser.add_argument('--add_zeros_for_missing_subbands', help='add zeros for missing subbands',action='store_true')
parser.add_argument('-f','--fillvalue', help='fillvalue for missing subbands default=0',dest='fillvalue',default=0,type=float)
parser.add_argument('--add_zeros_pattern', help='pattern to search for in filestring (preceding 3digit SB nr',dest='pattern',default='SB')
parser.add_argument('-s','--f0', type=float, dest='f0', help='starting frequency (MHz). Default=115',default=0.)
parser.add_argument('-e','--fe', type=float, dest='fe', help='end frequency (MHz). Default=170',default=170.)

def main(argv):
    args=parser.parse_args(argv)
    print args
    fitslist=[i for  i in args.fitslist if os.path.isfile(i)]
    if len(fitslist)==0:
        if os.path.isfile(args.fitslist[0]):
            myf=open(args.fitslist[0])
            fitslist=[i.strip() for  i in myf if os.path.isfile(i.strip())]
    tmpheader=pyfits.open(fitslist[0])[0].header   
    nraxis=tmpheader['NAXIS']
    
    freqaxis=[i for i in range(1,nraxis+1) if args.axis in tmpheader['CTYPE%d'%i] ][0]
    frequnit=tmpheader['CUNIT%d'%(freqaxis)].strip()
    freqfactor=1.
    if frequnit=='Hz':
        freqfactor=1.e6
    elif frequnit=='GHz':
        freqfactor=1.e-3
    print fitslist
    freqs=[pyfits.open(i)[0].header['CRVAL%d'%freqaxis]+(np.arange(pyfits.open(i)[0].header['NAXIS%d'%freqaxis])-pyfits.open(i)[0].header['CRPIX%d'%freqaxis]+1)*pyfits.open(i)[0].header['CDELT%d'%freqaxis] for i in fitslist]
    
    print "getting freqs",freqfactor,frequnit,args.f0*freqfactor,args.fe*freqfactor,freqs[0],freqs[-1]
    fritslist=[(i[0][0],)+i for i in zip(freqs,fitslist) if (i[0][-1]>args.f0*freqfactor and i[0][0]<args.fe*freqfactor) ]
    #freqs,fitslist=(list(i) for i in zip(*sorted(zip(freqs,fitslist))))
    tmp,freqs,fitslist=(list(i) for i in zip(*sorted(fritslist)))
    freqs=np.array(freqs).flatten()
    if args.add_zeros_for_missing_subbands:
        extrafreqs={}
        if args.pattern in fitslist[0]:
            sbs=[int(i[i.find(args.pattern)+len(args.pattern):i.find(args.pattern)+len(args.pattern)+3]) for i in fitslist]
            print freqs,sbs
            nrchan=sbs.count(sbs[0])
            freqstep1=freqs[1]-freqs[0]
            freqstep2=(freqs[nrchan]-freqs[0])/(sbs[nrchan]-sbs[0])
            for sbid in range(sbs[0],sbs[-1]):
                if not sbid in sbs:
                    freqstart=freqs[0]+freqstep2*(sbid-sbs[0])
                    extrafreqs[str(sbid)]=[freqstart+i*freqstep1 for i in range(nrchan)]
        else:
            print "cannot add zeros for missing SB",args.pattern,"not found in",fitslist[0]

    
    
    data_0=pyfits.open(fitslist[0])[0].data
    if args.add_zeros_for_missing_subbands:
        zerosdata=np.zeros_like(data_0)+args.fillvalue
        nfreqs=[freqs[0]]
        oldsb=sbs[0]
    print "processing", len(fitslist),"files:"
    fitslist.sort(key=lambda f: int(filter(str.isdigit, f)))
    for ifile,myf in enumerate(fitslist[1:]):
        if ifile == 0:
		print(data_0.shape)
		storeShape = np.asarray(data_0.shape)
		storeShape[1] = len(fitslist)
		print(storeShape)
		data = np.zeros(storeShape)
		data[:, ifile, ...] = data_0
		data_0 = None

        print("Processing File #{0}".format(myf))

        if args.add_zeros_for_missing_subbands:
            if sbs[ifile+1]>oldsb+1:
                for sbid in range(oldsb+1,sbs[ifile+1]):
                    nfreqs+=extrafreqs[str(sbid)]
                    for ichan in range(nrchan):
                        data=np.concatenate((data,zerosdata),axis=nraxis-freqaxis)
            nfreqs.append(freqs[ifile+1])
            oldsb=sbs[ifile+1]
        data[:, ifile + 1, ...] = pyfits.open(myf)[0].data
        
	#if np.any(np.isnan(ndata)):
        #    ndata[:]=0.
    
    if args.add_zeros_for_missing_subbands:
        freqs=nfreqs
    tmpheader['NAXIS%d'%freqaxis]=len(freqs)
    approxfreq=np.linspace(freqs[0],freqs[-1],len(freqs))
    tmpheader['CRVAL%d'%freqaxis]=approxfreq[0]
    tmpheader['CRPIX%d'%freqaxis]=1
    tmpheader['CDELT%d'%freqaxis]=approxfreq[1]-approxfreq[0]
    if os.path.isfile(args.outdir+"/"+args.outfile):
        os.remove(args.outdir+"/"+args.outfile)
    pyfits.writeto(args.outdir+"/"+args.outfile,data,tmpheader)
    pyfits.append(args.outdir+"/"+args.outfile,np.array(freqs)) #add real frequency array as extension to the file


if __name__ == '__main__':
    main(sys.argv[1:])    


