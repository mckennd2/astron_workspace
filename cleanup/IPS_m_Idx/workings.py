from __future__ import print_function

import numpy as np
from scipy.signal import welch, kaiser
import scipy.signal as spys
import matplotlib.pyplot as plt
from sys import stdout

import h5py

def initialiseSetup():
	fid = h5py.File("../L649109_3C48_CS_148to167MHz.h5", 'r')

	dt = fid.attrs.items()[31][1] / 1e6


	return fid, dt

def powerIntegral(welchPower, welchOffset, freqArr, cutoffIdx, mode = 0):
	if type(welchOffset) == np.ndarray:
		if mode == 0:
			powerArr = []
			for offset in iter(welchOffset):
				powerArr.append(powerWorker(welchPower, offset, freqArr, cutoffIdx))


		if mode == 1:
			welchOffset = np.mean(welchOffset, axis = 1)
			power = powerWorker(welchPower, welchOffset, freqArr, cutoffIdx)

			powerArr = np.asarray([power])


		if mode == 2:
			welchOffset = np.min(welchOffset, axis = 1)
			power = powerWorker(welchPower, welchOffset, freqArr, cutoffIdx)

			powerArr = np.asarray([power])

	else:
		power = powerWorker(welchPower, welchOffset, freqArr, cutoffIdx)
		powerArr = np.asarray([power])

	return powerArr

def powerWorker(welchPower, welchOffset, freqArr, cutoffIdx):
	truePower = welchPower - welchOffset
	return np.trapz(truePower[:cutoffIdx], freqArr[:cutoffIdx])

def meanIntensitySq(mainBeam, offsetBeam, mode = [0]):
	if type(mode) != list:
		mode = [mode]
	if len(mode) == 2:
		percentile= mode[1]
	mode = mode[0]

	if type(offsetBeam) == np.ndarray:

		if mode == 0:

			meanIntensityArr = []
			for offBeam in iter(offsetBeam):
				meanIntensityArr.append(meanIntensitySqWorker(mainBeam, offBeam))

			meanIntensity = np.asarray(meanIntensityArr)

		if mode == 1:
			offBeam_M = np.mean(offsetBeam, axis = 0)
			meanIntensity = meanIntensitySqWorker(mainBeam, offBeam_M)

		if mode == 2:
			offBeam_M = np.min(offsetBeam, axis = 0)
			meanIntensity = meanIntensitySqWorker(mainBeam, offBeam_M)

		if mode == 3:
			offBeam_P = np.percentile(offsetBeam, percentile, axis = 0)

			meanIntensity = meanIntensitySqWorker(mainBeam, offBeam_P)

	else:
		meanIntensity = meanIntensitySqWorker(mainBeam, offsetBeam)

	sqVal = np.square(meanIntensity)
	return sqVal

def meanIntensitySqWorker(mainBeam, offsetBeam):
	meanPerFreq = np.mean(mainBeam, axis=1) - np.mean(offsetBeam, axis =1)
	#print("Mean Intensity Sq")
	#print(meanPerFreq.shape)
	meanIntensity = np.sum(meanPerFreq)

	return meanIntensity


def mIdxVal(dataBeam, dataOff, freqArr, percentile, dt, auto=True, passedValue = 0., modePower = 0, modeInt = [0,10]):
	print("## Performing Main Beam FFT ##", end = '\r')
	stdout.flush()

	print(dataBeam.shape, dataOff.shape)
	pFreqM, pWelchM = welch(np.percentile(dataBeam, percentile, axis = 0), fs = 1. / dt, 
		nperseg = 4096, return_onesided=True, window = ("kaiser", 8.6))
	pFreqO = []
	pWelchO = []
	if len(dataOff.shape) != 3:
		dataOff = np.asarray([dataOff, np.zeros(dataOff.shape)])

	for idx, offBeam in enumerate(dataOff):
		print("## Performing Off Beam {0} FFT ##".format(idx + 1), end = '\r')
		stdout.flush()
		pFreqOTmp, pWelchOTmp = welch(np.percentile(offBeam, percentile, axis = 0), fs = 1. / dt, 
			nperseg = 4096, return_onesided=True, window = ("kaiser", 8.6))

		pFreqO.append(pFreqOTmp)
		pWelchO.append(pWelchOTmp)

	####
	# Possibly more efficient option, but lacks per-beam fft status tracking
	# offSet = np.transpose(dataOff, axes = (1,0,2))
	# pFreqO, pWelchO = welch(np.percentile(offSet, percentile, axis = 0), fs = 1. / dt, 
	#		nperseg = 4096, return_onesided=True, window = ("kaiser", 8.6))
	####


	pFreqO = np.asarray(pFreqO)
	pWelchO = np.asarray(pWelchO)

	print("## Determining Cutoff Frequency ##", end = '\r')
	stdout.flush()
	if auto:
		if passedValue != 0.:
			cutoffFreq = passedValue
		else:
			cutoffFreq = determineCutoff(pWelchM, 0.0005, 25)
	else:
		plt.loglog(pWelchM)
		plt.show()
		cutoffFreq = int(raw_input("Cut off Idx?"))

	print("## Performing Power and Intensity Analysis in modes {0} and {1} ##".format(modePower, modeInt))

	powerInt = powerIntegral(pWelchM, pWelchO, pFreqM, cutoffFreq, modePower)
	meanIntensitySquare = meanIntensitySq(dataBeam, dataOff, modeInt)
	#print(powerInt, meanIntensitySquare)

	return powerInt / meanIntensitySquare, [cutoffFreq, powerInt, meanIntensitySquare]


def dataAcq(fid, segment, separation, offsetBeams = 6):
	####
	# Acquire the data for both the main beam and offset beams for a given spectrum from hardcoded
	# 	links to a h5py file for the observation. Ensure the hardcoded values are correct for your dataset.
	####

	# Determine the size of the dataset accessible
	limit = fid["DYNSPEC_000/DATA"].shape[0]

	# Create an empty list to store the n off beam observations
	dataOffset = []

	# Ensure we aren't begining the data access above this limit, otherwise numpy will compain.
	if (segment) * (separation ) > limit:
		print("\n\n\n\n\n\n\n\n#####\nAttempting to Access Non-existant Data. Exiting...\n#####")
		return [],[]

	# Extract the on-xis beam as it's own variable.
	dataMain = fid["DYNSPEC_000/DATA"][separation * segment: separation * (segment +1),:,0].transpose()
	
	# For each request off axis beam, iterate through the system to extract the requested data and save them
	#	to the previously defined dataOffset list.
	for i in range(offsetBeams):
		dataOffset.append(fid.items()[2 + i][1].items()[3][1]
			[separation * segment: separation * (segment + 1), :, 0].transpose())

	dataOffset = np.asarray(dataOffset)
	# Return the data requested.
	return dataMain, dataOffset

def rollingAverage(dataSet, n):
	####
	# For a given numpy array, convert each value into a rolling average of the n nearby values on
	# 	each side of the data point. Constructed to be used for large data sets with lossy information 
	#	at the start and end.
	####

	# Setup a 0 array to store the average values
	newSet = np.zeros(dataSet.shape)

	# Iterate over the given dataset (skipping the first n+1 and last n+1 pieces of data)
	
	# This could be improved by providing the local values instead of a 0, but np.copy() is 
	# 	significantly slower than getting a zero'd array.
	for i in range(len(dataSet) - n -1)[n+1:]:
		# Store the local value (faster than np.copy())
		newSet[i] = dataSet[i]

		# Add the n nearby values on each side of the current point
		for i1 in range(n)[1:]:
			newSet[i] += dataSet[i + i1]
			newSet[i] += dataSet[i - i1]

	# Determine the average of the values
	newSet /= (2 * n + 1)

	# Return the averaged data set.
	return newSet


def rollingAverage2Dim(dataSet, n):
	####
	# For a given numpy array, convert each value into a rolling average of the n nearby values on
	# 	each side of the data point. Constructed to be used for large data sets with lossy information 
	#	at the start and end.
	#
	# NOTE: WILL WRAP AT EDGES, UNLIKE ROLLINGAVERAGE FUNCTION
	####

	# Setup a 0 array to store the average values
	newSet = np.zeros(dataSet.shape)

	for x in range(dataSet.shape[0] -n)[n:]:
#		if x % 10 == 0:
#			print("HIT")
		for y in range(dataSet.shape[1] - n)[n:]:
			newSet[x,y] += np.mean(dataSet[x-n:x+n, y] + dataSet[x, y-n:y+n])



	# Return the averaged data set.
	return newSet


def determineCutoff(dataSet, threshold, nAvg):
	####
	# Determine the cutoff point for the Fresnel knee in a power plot to determine the
	# 	index of the cutoff frequency for the power integral.
	####

	#Conver the data set to a log form, easier to analyse
	logData = np.log(dataSet)

	# Get a rolling average over n data points the smooth out the curve and make the transition more pronouced,
	avgData = rollingAverage(logData, nAvg)


	# Skipping the first 50 data points due to the asymptote at low frequencies, iterate
	#	through the remainder of the data
	for i in np.arange(50, len(dataSet)):
		print("##	Determining Cutoff frequency (Slope) ##", end = '\r')
		stdout.flush()
		# Determine the slope at the given point
		slope = (avgData[i] - avgData[i -1]) / avgData[i]
		# Check if the slope is below our threshold
		if abs(slope) < threshold:

			# Log for manual analysis if needed (to catch extreme outliers)
			print('\n', slope, i)
			return i

	# Show the plot in the extreme case that we don't reach the threshold (must be a very different
	#	plot or extremely noisey)
	print("Threshold not hit")
	plt.plot(logData)
	plt.show()
	return i

def lazyAnalysis(fid, segments = 68, separations = 60000, 
	percentile = 10, dt = 0.01048576):

	offAxisBeams = len(fid.items()) - 2 # Sub 2 for the SYS_LOG and main beam groups
	mValues = np.zeros([segments, offAxisBeams])

	#DEBUG
	powerLog = []
	intensityLog = []

	freqArr = fid["DYNSPEC_000/COORDINATES/SPECTRAL"].attrs.items()[6][1]

	for i in range(segments):
		dataMain, dataOff = dataAcq(fid, i, separations, 6)



		mValues[i], cutoffFreq = mIdxVal(dataMain, dataOff, freqArr, percentile, dt, True, mode = 0)
	
		powerLog.append(cutoffFreq[1])
		intensityLog.append(cutoffFreq[2])

	powerLog = np.asarray(powerLog)
	intensityLog = np.asarray(intensityLog)

	mValues = np.sqrt(mValues)

	return mValues, powerLog, intensityLog

def lazyAnalysisClean(main, off, freqArr, segments = 69, separations = 60000, 
	percentile = 10, dt = 0.01048576):

	offAxisBeams = len(fid.items()) - 2 # Sub 2 for the SYS_LOG and main beam groups
	mValues = np.zeros([segments, offAxisBeams])

	#DEBUG
	powerLog = []
	intensityLog = []

	for i in range(segments):
		print("### Preparing Segement {0} ###".format(i +1))
		dataMain = np.transpose(main[i * separations: (i+1) * separations, ...], axes = (1,0,2))[:,:,0]
		dataOff = np.transpose(off[i * separations: (i+1) * separations, ...], axes =(2,1,0))
		print("## Begining mIdx Calcuation ##", end = '\r')
		stdout.flush()
		mValues[i], cutoffFreq = mIdxVal(dataMain, dataOff, freqArr, percentile, dt, True, modeInt = 0)
	
		powerLog.append(cutoffFreq[1])
		intensityLog.append(cutoffFreq[2])

	powerLog = np.asarray(powerLog)
	intensityLog = np.asarray(intensityLog)

	mValues = np.sqrt(mValues)

	return mValues, powerLog, intensityLog


def evenLazierAnalysis(fid):
	mValues, powerLog, intensityLog = lazyAnalysis(fid)

	plt.figure(1)
	plt.plot(range(mValues.shape[0]), mValues)
	plt.title("Raw Power, Raw Intensity")

	plt.figure(2)
	plt.plot(range(powerLog.shape[0]), powerLog)
	plt.title("Power History")

	plt.figure(3)
	plt.plot(range(intensityLog.shape[0]), intensityLog)
	plt.title("Intensity History")
	plt.show()


'''
def segmentFlagger(fid, threshold = 2., chunksize = 60000):
#	dataPoints = fid["DYNSPEC_000/DATA"].shape[0]
#
#	segments = int(np.ceil(dataPoints / chunksize))
#
#	segments = 10
#	mVal, __, __ = lazyAnalysis(fid, segments, chunksize)
#	print(mVal)
#
#	loggedArray = []
#
#	for i in range(segments)[1:]:
#		for i1 in range(mVal.shape[1]):
#			if any(mVal[i,i1] > mVal[i] * threshold):
#				print("FLAG")
#				loggedArray.append([[i,i1], mVal[i,i1]])
#				print(loggedArray)

#	print(loggedArray)

#	for segment in loggedArray:
#		relevantData = dataAcq(fid, segment[0][0], chunksize, 6)
#
#		mainData = relevantData[0]
#		offsetData = relevantData[1][segment[0][1]]

#		plt.figure(1)
#		mainDataN = mainData.copy() / np.median(mainData)
#		plt.imshow(mainDataN, vmax = 1.1, vmin = 0.9, aspect = "auto")
#		plt.title("Main Data")
#
#		plt.figure(2)
#		offsetDataN = offsetData.copy() / np.median(offsetData)
#		plt.imshow(offsetDataN, vmax = 1.1, vmin = 0.9, aspect = "auto")
#		plt.title("Off-Axis Beam")
#
#		plt.figure(3)
#		plt.imshow(mainDataN - offsetDataN, vmin = 3., aspect = "auto")
#
#		flaggerChild(fid, mainDataN, 0.5)
#		flaggerChild(fid, offsetDataN, 0.5)
#		plt.show()
'''


'''
def flaggerChild(mainWorkspace, threshold):
	mainWorkspace = mainWorkspace.copy()

	shiftedMain = np.roll(mainWorkspace, 1, axis = 1)
	shiftedMain[:,0] = 0.
	shiftedMain[:, -1] = 0.

	approxSlope = mainWorkspace - shiftedMain
	print(approxSlope.shape)
	flaggedPoints = approxSlope > threshold
	print(flaggedPoints)

	plt.imshow(flaggedPoints, aspect = "auto")
	plt.show()
'''

#def flagger(data, trigger):
#
#	shape = data.shape
#	flaggedArr = np.zeros(shape, dtype=np.bool_)
#	for channel in range(shape[0]):
#		triggered = False
#
#		for sample in range(shape[1]-1)[1:]:
#			if data[channel, sample] * (1 + trigger) < data[channel, sample +1]:
#				triggered = True
#			if data[channel, sample] * (1 - trigger) > data[channel, sample +1]:
#				triggered = False
#
#			flaggedArr[channel, sample] = triggered
#
#	return flaggedArr


#def flagger2(data,trigger):
#	shape = data.shape
#	flaggedArr = np.zeros(shape, dtype=np.bool_)
#	for channel in range(shape[0]):
#		triggered = False
#
#		for sample in range(shape[1]-1)[1:]:
#			if (data[channel, sample] + trigger < data[channel+1, sample]) or (data[channel, sample] + trigger < data[channel-1, sample] ):
#				triggered = True
#			if (data[channel, sample] - trigger > data[channel+1, sample]) or (data[channel, sample] - trigger < data[channel-1, sample] ):
#				triggered = False
#
#			flaggedArr[channel, sample] = triggered
#
#	return flaggedArr


def flagger2V(data,trigger):
	# KNOWN ISSUE: Cannot handle RFI source over more than 2 consequetive channels
	shape = data.shape


	shiftUp = np.roll(data, -1, axis=0)
	shiftDown = np.roll(data,1, axis = 0)

	triggeredArr = (data > shiftUp + trigger).astype(int) + (data > shiftDown + trigger).astype(int)

	triggeredArr = np.clip(triggeredArr, -1., 1.)


	return triggeredArr

#def flaggerV(data,trigger):
#	shape = data.shape
#
#
#	shiftLeft = np.roll(data, 1, axis=1)
#	shiftRight = np.roll(data,-1, axis =1)
#
#	triggeredArr = (data > shiftLeft + trigger) + (data > shiftRight + trigger).astype(int)
#
#	triggeredArr = np.clip(triggeredArr, -1., 1.)
#
#	return triggeredArr

def temporalFlagger(data, percentile = 99.9):
	# Lazy mode

	medianVal = np.median(data, axis = 0)

	extremae = np.percentile(medianVal, percentile, axis = 0)

	flaggedTime = medianVal > extremae

	return flaggedTime.astype(int)

def gapSmoothener(data, flaggedPoints, nNearby = 1, nSample = 5, nIter = 3):
	### Rewrite as convolution problem
	work = data.copy()
	flaggedPoints = np.argwhere(flaggedPoints == 1.)
	for passVar in range(nIter):
		print("Pass {0} on data".format(passVar))
		for noisePoint in flaggedPoints:
			for delta in (np.arange(nNearby) +1):
				work[noisePoint[0], noisePoint[1]] = np.mean(work[noisePoint[0] - delta: noisePoint[0] + delta, noisePoint[1] - delta : noisePoint[1] + delta])

	return work

def gapSmoothener2(data, flaggedPoints, gaussianOrder = 3, nIter = 5):
	work = data.copy()
#	kernel = np.ones([gaussianOrder, gaussianOrder])
	kernel = np.asarray([[0.,0.,3.,0.,0.], [0.,0.,9.,0.,0.], [0.,1.,4.,1.,0.], [0.,0.,9.,0.,0.], [0.,0.,3.,0.,0.]])

	print(kernel, type(kernel))
	kernel /= np.sum(np.sum(kernel))

	flaggedPoints = np.argwhere(flaggedPoints == 1.)

	for n in range(nIter):
		print("Begining Pass {0}".format(n + 1), end = "\r")
		stdout.flush()
		blur = spys.convolve2d(work, kernel,  'same', 'symm')
		plt.imshow(blur, aspect = "auto", interpolation = "nearest", vmax = 1.2, vmin = 0.8)
		plt.show()
		for point in flaggedPoints:
			work[point[0], point[1]] = blur[point[0],point[1]]

	return work


def gapSmoothener2Temporal(data, flaggedPoints, gaussianOrder = 3, nIter = 5):
	# Efficiency can be improved b ylocalising 2 channels no each side of array to perform convolution
	# instead of applying convolution to the entire array.
	work = data.copy()
#	kernel = np.ones([gaussianOrder, gaussianOrder])
	kernel = np.asarray([[0.,1.,0.,1.,0.], [2.,4.,0.,4.,2.], [4.,8.,0.,8.,4.], 
		[0.,4.,0.,4.,0.], [0.,1.,0.,1.,0.]])

	kernel /= np.sum(np.sum(kernel))

	flaggedPoints = np.argwhere(flaggedPoints != 0.)

	for n in range(nIter):
		print("Begining Pass {0}				".format(n + 1), end = "\r")
		stdout.flush()
		blur = spys.convolve2d(work, kernel,  'same', 'symm')

		for point in flaggedPoints:
			work[:, point] = blur[:,point]

	return work

def gapSmoothener2Channel(data, flaggedPoints, gaussianOrder = 3, nIter = 5):
	work = data.copy()
#	kernel = np.ones([gaussianOrder, gaussianOrder])
	kernel = np.asarray([[0.,0.,3.,0.,0.], [0.,0.,9.,0.,0.], [0.,1.,4.,1.,0.], [0.,0.,9.,0.,0.], [0.,0.,3.,0.,0.]])

	kernel /= np.sum(np.sum(kernel))

	flaggedPoints = np.argwhere(flaggedPoints == 1.)

	for n in range(nIter):
		print("Begining Pass {0}				".format(n + 1), end = "\r")
		stdout.flush()
		blur = spys.convolve2d(work, kernel,  'same', 'symm')

		for point in flaggedPoints:
			work[point[0], point[1]] = blur[point[0],point[1]]

	return work

#def channelSmoothener(data, channelFlag, bias = 1):
#	work = data.copy()

#	flaggedPixels = np.argwhere(channelFlag != 0.)
#	flaggedPixels = flaggedPixels.tolist() ## Fuck numpy

#	for pixel in flaggedPixels:
#		bias = channelWorker(data, [pixel[0] + bias, pixel[1] + bias], flaggedPixels, [False, bias])
#		print("Offset Found {0} for pixel {1}".format(bias, pixel))
#		work[pixel[0], pixel[1]] = data[pixel[0] + bias, pixel[1] + bias]

#	return work

#def channelWorker(data, pixelLoc, flaggedPixels, flag = [False, 1]):
#	bias = flag[1]
#	print(pixelLoc, pixelLoc in flaggedPixels)
#	if (pixelLoc in flaggedPixels):
#		if not flag[0]:
#			channelWorker(data, [pixelLoc[0] + bias, pixelLoc[1] + bias], flaggedPixels, [True, bias + 1])
#		else:
#			channelWorker(data, [pixelLoc[0] - bias, pixelLoc[1] - bias], flaggedPixels, [False, bias + 1])

#	else:
#		return bias


def channelSmoothener(work, channelFlag, nIter):
	data = work.copy()
	offsetPixels = np.roll(np.roll(data, 1, axis = 1), 1, axis = 0) + np.roll(np.roll(data, -1, axis = 1), -1, axis = 0) + np.roll(np.roll(data, 1, axis = 0), -1, axis = 1) + np.roll(np.roll(data, -1, axis = 0), 1, axis = 1) + np.roll(data, 2, axis = 1) + np.roll(data, -2, axis = 1) + np.roll(data, 2, axis = 0) + np.roll(data, -2, axis = 0)

	offsetPixels /= 8
	offsetPixels *= channelFlag
	data += offsetPixels
	if nIter > 1:
		channelSmoothener(data,channelFlag, nIter -1)
	else:
		return data

def timeSmoothener(data, timeSamples, bias = 1):
	work = data.copy()
	samples = np.argwhere(timeSamples == 1)
	for sampleFix in samples:
		newCol = timeWorker(data, sampleFix + bias, samples, [False, bias])
		if newCol > data.shape[1] -1:
			newCol = timeWorker(data, sampleFix -50, samples, [False, bias])
		work[:, sampleFix] = data[:, int(newCol)][0]

	return work

def timeWorker(data, colN, samples, flag = [False, 1]):
	if colN > data.shape[1]:
		colN -= 25
	if colN in samples:
		if not flag[0]:
			colN = timeWorker(data,colN +1, samples, [True, 1])
			return colN
		else:
			colN = timeWorker(data, colN + flag[1], samples, [True, flag[1]+1])
			return colN
	else:
		return colN




def autoCleaner(inputIm):

	data = inputIm.copy()
	if np.max(data) > 10.:
		data = data.transpose() / np.median(data, axis = 1)
		data = data.transpose()

	print("Data Normalised                                      ", end = '\r')
	stdout.flush()
	
	channelFlag = flagger2V(data, 0.1)

	print("Channel Flagging Performed                                      ", end = '\r')
	stdout.flush()


	timeFlag = temporalFlagger(data, 99.9)
	
	print("Time flagging performed                                      ", end = '\r')
	stdout.flush()

	combinedFlag = channelFlag + timeFlag
	combinedFlag = np.clip(combinedFlag, -1., 1.)
	
	print("Flags combined                                      ", end = '\r')
	stdout.flush()

	nullPoints = 1. - combinedFlag
	removedNoiseImage = data * nullPoints
	
	print("Noise Removed                                      ", end = '\r')
	stdout.flush()

	timeHealed = timeSmoothener(removedNoiseImage, timeFlag, 1.)

	print("Time Healed (Pass 1)                                     ", end = '\r')
	stdout.flush()

	healedImage = channelSmoothener(timeHealed, channelFlag, 1.)

	print("Channel Healed (Pass 1)                                     ", end = '\r')
	stdout.flush()


	healedImage2 = gapSmoothener2Temporal(healedImage, timeFlag)
	
	print("Temporal Flags Healed                                      ", end = '\r')

	cleanImage = gapSmoothener2Channel(healedImage2, channelFlag)
	
	print("Temporal Flags Healed                                      ", end = '\r')
	stdout.flush()


	#for analysis reasons
	timeFlag = np.zeros(data.shape) + timeFlag
	return cleanImage, [channelFlag, timeFlag, combinedFlag, data, removedNoiseImage, timeHealed, healedImage, healedImage2, cleanImage]

#	return channelFlag, timeFlag, combinedFlag
def plotter(image, vma = 1.2, vmi = 0.8):
	plt.imshow(image, vmax = vma, vmin = vmi, aspect = "auto", interpolation = "nearest")
	plt.show()

def plotterM(imageArr, vma = 1.2, vmi = 0.8):
	print(imageArr)
	for i in range(len(imageArr)):
		print(imageArr[i].shape)
		plt.figure(i+1)
		plt.imshow(imageArr[i], vmax = vma, vmin = vmi, aspect = "auto", interpolation = "nearest")
	plt.show()

def autoProcess(fid, freqArr, dt, sample, chunksize = 60000):
	print("##	Cleaning Main beam	##")
	main, offArr = dataAcq(fid, sample, chunksize)

	mainCleaned = autoCleaner(main)[0]

#	print(mainCleaned.shape, main.shape)
	assert(mainCleaned.shape == main.shape)
	offCleaned = []

	for offbeam in offArr:
		print("                                      ", end = "\r")
		print("##	Cleaning offset beam {0}	##".format(len(offCleaned) + 1))
		offCleaned.append(autoCleaner(offbeam)[0])

	offCleaned = np.asarray(offCleaned)

#	print(offCleaned.shape, offArr.shape)
	assert(offCleaned.shape == offArr.shape)

	mValues, __ = mIdxVal(main, offArr, freqArr, 10, dt, True, modeInt = [0])
	mValuesClean, __ = mIdxVal(mainCleaned, offCleaned, freqArr, 10, dt, True, modeInt = [0])

	mValuesT, __ = mIdxVal(main, offArr, freqArr, 10, dt, True, modeInt = [3,10])
	mValuesCleanT, __ = mIdxVal(mainCleaned, offCleaned, freqArr, 10, dt, True, modeInt = [3,10])

	return mValues, mValuesClean, mValuesT, mValuesCleanT
	
def autoAutoProcess(fid, freqArr, dt, nSamples, chunksize):
	mValuesArr = []
	mValuesCleanArr = []
	mValuesTArr = []
	mValuesCleanTArr = []
	for i in range(nSamples):
		print("####		PROCESSING DATASET {0}		####".format(i))
		mValues, mValuesClean, mValuesT, mValuesCleanT = autoProcess(fid, freqArr, dt, i, chunksize)

		mValuesArr.append(mValues)
		mValuesCleanArr.append(mValuesClean)
		mValuesTArr.append(mValuesT)
		mValuesCleanTArr.append(mValuesCleanT)

	mValuesArr = np.asarray(mValuesArr)
	mValuesCleanArr = np.asarray(mValuesCleanArr)
	mValuesTArr = np.asarray(mValuesTArr)
	mValuesCleanTArr = np.asarray(mValuesCleanTArr)

	return mValuesArr, mValuesCleanArr, mValuesTArr, mValuesCleanTArr

def autoProcessBackup(fid, freqArr, dt, sample, chunksize = 60000):
	print("##	Cleaning Main beam	##")
	main, offArr = dataAcq(fid, sample, chunksize)

	mainCleaned = autoCleaner(main)[0]

	print(mainCleaned.shape)

#	print(mainCleaned.shape, main.shape)
	assert(mainCleaned.shape == main.shape)
	offCleaned = []

	for idx, offbeam in enumerate(offArr):
		print("                                      ", end = "\r")
		print("			##	Cleaning offset beam {0}	##".format(idx + 1))
		offCleaned.append(autoCleaner(offbeam)[0])

	offCleaned = np.asarray(offCleaned)

#	print(offCleaned.shape, offArr.shape)
	assert(offCleaned.shape == offArr.shape)

	return mainCleaned, offCleaned

def autoAutoProcessBackup(fid, freqArr, dt, nSamples, chunksize):
	mainShape = np.asarray(fid["DYNSPEC_000/DATA"].shape)
	print(mainShape)
	offShape = mainShape.copy()
	offShape[2] = mainShape[2] + len(fid.items()) - 3

	if offShape[0] > nSamples * chunksize:
		offShape[0] = nSamples * chunksize
		mainShape[0] = nSamples * chunksize

	print(mainShape, offShape)


	with h5py.File("/media/chrx/1336ADCD09F54879/Series/L649109_3C48_CS_148to167MHz_processed_2.h5", 'a') as newFid:
		mainSet = newFid.create_dataset("main_beam", shape = mainShape, dtype=np.float32, compression = "lzf")
		offSet = newFid.create_dataset("off_beams", shape = offShape, dtype=np.float32, compression = "lzf")

		for i in range(nSamples):
			print("                                      ", end = "\r")
			print("####			Cleaning Time Segment {0}			####".format(i + 1))
			mainCleaned, offCleaned = autoProcessBackup(fid, freqArr, dt, i, chunksize)
			mainSet[i * chunksize: (i + 1) * chunksize, :, 0] = mainCleaned.transpose()
			print(mainCleaned.shape)
			print(offCleaned.shape)
			if not i * chunksize > mainShape[0] + 2:
				transposedOff = np.zeros([chunksize, offShape[1], offShape[2]])
			else:
				transposedOff = np.zeros([offShape[0] - ((nSamples -1) * chunksize) - 1, offShape[1], offShape[2]])
			
			for idx, beam in enumerate(offCleaned):
				transposedOff[:beam.shape[1], :, idx] = beam.transpose()

			print(transposedOff.shape)
			if transposedOff.shape[0] != chunksize:
				offSet[i * chunksize:, :, :] = transposedOff
			else:
				offSet[i * chunksize: (i + 1) * chunksize, :, :] = transposedOff








fid, dt = initialiseSetup()
main, offset = dataAcq(fid, 0, 60000)
freqArr = fid["DYNSPEC_000/COORDINATES/SPECTRAL"].attrs.items()[6][1]
