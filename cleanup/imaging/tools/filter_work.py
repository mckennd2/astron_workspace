import numpy as np 
import scipy.signal as spys



def apply_filter(varArr, dataArr, axis = -1):
	return spys.lfilter(varArr[0], varArr[1], dataArr, axis)

def butterworth_filter(lowF, highF, sampleF, btypeVar = "bandpass", order = 2):
	nyquist = sampleF / 2.

	highF = np.min([highF, nyquist]) # Filter limit 

	if btypeVar == "highpass":
		freqRange = lowF / nyquist
	elif btypeVar == "lowpass":
		freqRange = highF / nyquist
	elif btypeVar == "bandpass":
		freqRange = np.asarray([lowF, highF]) / nyquist

	else:
		print("#!#!#! Unknown filter, defaulting to bandpass")
		freqRange = [lowCut, highCut]

	filterNumer, filterDenom = spys.butter(order, freqRange, btype = btypeVar)

	return [filterNumer, filterDenom]
