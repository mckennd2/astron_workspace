from __future__ import print_function

import h5py
import astropy.io.fits as fits
import numpy as np


def filePrep(fileLoc):
	if fileLoc[-4:] == "fits":
		return fits_data_loader(fileLoc)
	
	elif fileLoc[-2:] == "h5":
		return h5_data_loader(fileLoc, "HDU-0/DATA")

	else:
		print("Unknown File format for file {0}".format(fileLoc))
		return None

def fits_data_loader(fitsLoc, dataIdx = 0):
	return fits.open(fitsLoc)[dataIdx].data

def fits_attr_loader(fitsLoc, dataIdx = 0):
	return fits.open(fitsLoc)[dataIdx].header

def h5_data_loader(h5Loc, datasetName):
	return h5py.File(h5Loc)[datasetName]

def load_pixel(dataSet, pixel):
	return dataSet[..., pixel[0], pixel[1]]

def load_pixels(dataSet, pixels):
	sC, sT, sX, sY = pixels[0]
	eC, eT, eX, eY = pixels[1]

	return dataSet[sC:eC, sT:eT, sX:eX, sY:eY]

def construct_pixel_ref(coord1, coord2, channel = [None, None], time = [None, None]):
	startCoord = [channel[0], time[0], coord1[0], coord1[1]]
	endCoord = [channel[1], time[1], coord2[0], coord2[1]]

	return [startCoord, endCoord]


def inspect_pixel(fileLoc, pixelLoc, plot = False):
	dataRef = filePrep(fileLoc)

	pixelTime = load_pixel(dataRef, pixelLoc).reshape(-1)
	
	if plot:
		plt.plot(pixelTime)
		plt.show()


	return pixelTime

def inspect_area(fileLoc, pixelsCoords = None, plot = False):
	dataRef = filePrep(fileLoc)

	if pixelsCoords != None:
		pixelsCoords[1] += 1 # Account for slicing limits
	else:
		print("No coordinate passed, accessing all data.")
		pixelsCoord1, pixelCoord2 = [[None, None], [None, None]]
	pixelPrep = construct_pixel_ref(pixelsCoord1, pixelCoord2)

	areaTime = load_pixels(dataRef, pixelPrep)
	if plot:
		plt.imshow(areaTime[0][0], aspect = "auto")
		plt.show()

	return areaTime





def raw_fits_to_h5(inputFits, outputH5):
	with fits.open(inputFits) as fitsRef:
		with h5py.File(outputH5, 'a') as h5Ref:

			for idx1, HDU in enumerate(fitsref):
				__attrHandler(fitsRef, h5Ref)
				currDataset = h5Ref.create_dataset("HDU-{}/DATA".format(idx1), HDU.data)

			h5Ref.close()
		fitsRef.close()

def trim_fits_to_h5(inputFits, outputH5, startCoord, endCoord):
	with fits.open(inputFits) as fitsRef:
		with h5py.File(outputH5, 'a') as h5Ref:

			sC, sT, sX, sY = startCoord
			eC, eT, eX, eY = endCoord

			for idx1, HDU in enumerate(fitsref):
				__attrHandler(fitsRef, h5Ref)
				currDataset = h5Ref.create_dataset("HDU-{}/DATA".format(idx1), HDU.data[sC:eC, sT:eT, sX:eX, sY:eY])

			h5Ref.close()
		fitsRef.close()





def __attrHandler(fitsRef, h5Ref):
	for idx1, HDU in enumerate(fitsref):
		currDataset = h5Ref.create_dataset("HDU-{}".format(idx1), shape = [0], dtype = np.float8)
		for key in HDU.keys():
			currDataset.attrs.create(key, HDU[key])

			currDataset.attrs.create('filename', fitsRef.fileinfo(idx1)['filename'])
			currDataset.attrs.create('hdrLoc', fitsRef.fileinfo(idx1)['hdrLoc'])
			currDataset.attrs.create('resized', fitsRef.fileinfo(idx1)['resized'])

