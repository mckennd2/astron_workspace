from __future__ import print_function

import numpy as np
import scipy.signal as spys

from ..tools import fits_h5_tools as dataTools

import matplotlib.pyplot as plt

reload(dataTools)

def inspect_pixel(fileLoc, pixelLoc, plot = False):
	dataRef = __filePrep(fileLoc)

	pixelTime = dataTools.load_pixel(dataRef, pixelLoc).reshape(-1)
	if plot:
		plt.plot(pixelTime)
		plt.show()


	return pixelTime

def inspect_area(fileLoc, pixelsCoords = None, plot = False):
	dataRef = __filePrep(fileLoc)

	if pixelsCoords != None:
		pixelsCoords[1] += 1 # Account for slicing limits
	else:
		print("No coordinate passed, accessing all data.")
		pixelsCoords = [[None, None], [None, None]]
	pixelPrep = dataTools.construct_pixel_ref(pixelsCoords)

	areaTime = dataTools.load_pixels(dataRef, pixelPrep)
	if plot:
		plt.imshow(areaTime[0][0], aspect = "auto")
		plt.show()

	return areaTime

def analyse_pixel(timeSeries, offaxis):
	freqOutput, fftOutput = spys.welch(timeSeries, fs = 1. / 0.084, window = ("kaiser", 8.4), nperseg = 128, return_onesided = True)
	freqOutput2, fftOutput2 = spys.welch(offaxis, fs = 1. / 0.084, window = ("kaiser", 8.4), nperseg = 128, return_onesided = True)
	plt.title("Time Series")
	plt.plot(timeSeries)
	plt.figure(2)
	plt.loglog(freqOutput, fftOutput, label = "Raw")
	plt.loglog(freqOutput, fftOutput2, label = "Off Pixel Power")
	plt.legend()
	plt.title("FFT (LogLog)")
	plt.figure(3)
	plt.semilogx(fftOutput, label = "Raw")
	plt.semilogx(fftOutput2, label = "Off Pixel Power")
	plt.title("FFT Semi Log-X")
	plt.legend()
	plt.figure(4)
	plt.semilogx(freqOutput2, fftOutput2)
	plt.title("FFT Off source pixel")
	plt.show()

	
	integral = np.trapz(fftOutput[:13], freqOutput[:13])
	correction = np.mean(timeSeries - offaxis) ** 2
	return np.sqrt(integral / correction)

def __filePrep(fileLoc):
	if fileLoc[-4:] == "fits":
		return dataTools.fits_data_loader(fileLoc)
	
	elif fileLoc[-2:] == "h5":
		return dataTools.h5_data_loader(fileLoc, "HDU-0/DATA")

	else:
		print("Unknown File format for file {0}".format(fileLoc))
		return None
