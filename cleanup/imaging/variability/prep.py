import numpy as np 
import scipy.signal as spys

from ..tools import fits_h5_tools as dataTools


def load_and_clean(fileLoc, pixelLoc, sampleFreq, highF = 10., lowF = 0.1, order = 4.5):
	fileRef = dataTools.filePrep(fileLoc)

	if np.asarray(pixelLoc).shape != (2,):
		pixelPacker = [[None, None] for i in range(4)]
		for idx, element in enumerate(pixelLoc):
			pixelPacker[idx] = element

		pixelArr = dataTools.construct_pixel_ref(*pixelPacker)

		dataArr = dataTools.load_pixels(dataRef, pixelArr)
		axisVar = 1
	else:
		dataArr = dataTools.load_pixel(dataRef, pixelLoc)
		axisVar = -1

	cleanData = clean_data(dataArr, sampleFreq, highF, lowF, order, axis)


def clean_data(dataArr, sampleFreq, highF = 10.0, lowF = 0.1, order = 4.5, axis = -1):
	filterVar = __butterworth_filter(lowF, highF, sampleFreq, order)
	cleanData = __apply_filter(filterVar, dataArr)

	return cleanData

def __apply_filter(varArr, dataArr, axis = -1):
	return spys.lfilter(varArr[0], varArr[1], dataArr, axis)

def __butterworth_filter(lowF, highF, sampleF, order = 4.5):
	nyquist = sampelF / 2.

	lowCut = lowF / nyquist
	highCut = highF / nyquist

	filterNumer, filterDenom = spys.butter(order, [lowCut, highCut], btype = bandpass)

	return [filterNumer, filterDenom]