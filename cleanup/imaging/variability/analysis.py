import numpy as np 
import scipy.stats as spystat
import matplotlib.pyplot as plt
from ..tools import fits_h5_tools as dataFunc
from ..tools import filter_work as filterFunc

reload(dataFunc)
reload(filterFunc)
#n_ips: time scale of IPS in seconds, convert the sample counts

def std(dataArr, axisVar = None):	
	#return np.sqrt(np.sum(np.square(dataArr), axis = axisRef) / np.prod(shapeArr[axisVar]))
	return np.mean(abs(dataArr), axis = axisVar)

def higher_order_moments(dataArr, filterSettings):
	sampleFreq, pixelLoc, highF, lowF, order, axis = filterSettings
	meanVal = np.mean(dataArr)

	highPassVar = filterFunc.butterworth_filter(lowF, None, sampleFreq, "highpass", order)
	highPassArr = filterFunc.apply_filter(highPassVar, dataArr, 1)

	skewArr = spystat.skew(highPassArr, axis = 1)
	kurtosisArr = spystat.kurtosis(highPassArr, axis = 1)

	lowPassVar = filterFunc.butterworth_filter(None, highF, sampleFreq, "lowpass", order)
	filteredArr = filterFunc.apply_filter(lowPassVar, highPassArr)

	return skewArr, kurtosisArr, filteredArr, meanVal

def flag_values(dataArr, meanVal, stdMul = 5.):
	stdVal = std(dataArr)
	flags = ( dataArr - meanVal ) > ( stdMul * stdVal )
	
	return flags, stdVal

def scintillating_flux_density(dataArr, N, meanVal, stdMul = 5.):
	flags, stdVal = flag_values(dataArr, meanVal, stdMul)

	deltaS_scint = np.zeros(dataArr.shape)
	
	
	deltaS_scint[flags] = np.sqrt(np.square(dataArr[flags]) - np.square(meanVal))
	return deltaS_scint, stdVal, flags

def errorBars(dataArr, N, meanVal, n_ips = 1.5, stdMul = 5.):
	deltaS_scint, stdVal, flags = scintillating_flux_density(dataArr, N, meanVal, stdMul)
	

	errSys = np.sqrt(np.square(dataArr + stdVal) - np.square(meanVal)) - deltaS_scint
	errScint = np.sqrt(np.square(errSys) + np.divide(np.square(deltaS_scint), (N / n_ips)))

	return deltaS_scint, meanVal, stdVal, errSys, errScint, flags





def raw_generate_images(dataArr, n_ips = 1.5, stdMul = 5., filterSettings = None):
	filterSettings = __filter_settings(filterSettings)

	unpackArr = generate_images(dataArr, n_ips, stdMul, filterSettings)
	
	return unpackArr

def load_raw_and_generate_images(fileLoc, n_ips = 1.5, stdMul = 5., filterSettings = None):
	filterSettings = __filter_settings(filterSettings)

	dataArr = dataFunc.inspect_area(fileLoc, filterSettings[1])
	unpackArr = generate_images(dataArr, n_ips, stdMul, filterSettings)
	
	return unpackArr

def generate_images(dataArr, n_ips = 1.5, stdMul = 5., filterSettings = None):
	print(dataArr.shape)
	print(filterSettings[0])
	N = (dataArr.shape[0] * dataArr.shape[1])
	print(N)
	n_ips *= (1. / filterSettings[0]) # Convert from seconds to sample counts
	print(n_ips)
	skewArr, kurtosisArr, filteredArr, meanVal2 = higher_order_moments(dataArr, filterSettings)
	
	varIm = std(filteredArr, axisVar = (0,1))
	meanVal = np.mean(varIm)
	
	deltaS_scint, __, __, errSys, errScint, flags = errorBars(varIm, N, meanVal, n_ips, stdMul)

	return np.mean(dataArr, axis = 0), np.mean(dataArr, axis = (0,1)), np.mean(filteredArr, axis = 0), deltaS_scint, errSys, errScint, skewArr[0], kurtosisArr[0], flags




def filter_settings(sampleFreq = 0.083, pixelLoc = None, highF = 10., lowF = 0.1, order = 2, axis = -1):
	return [1. / sampleFreq, pixelLoc, highF, lowF, order, axis]

def __filter_settings(filterSettings = None):
	if filterSettings == None:
		filterSettings = filter_settings()

	return filterSettings

