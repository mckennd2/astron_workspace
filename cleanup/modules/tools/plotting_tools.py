import matplotlib.pyplot as pyplot

def plotter(image, vma = 1.2, vmi = 0.8):
	plt.imshow(image, vmax = vma, vmin = vmi, aspect = "auto", interpolation = "nearest")
	plt.show()

def plotterM(imageArr, vma = 1.2, vmi = 0.8):
	for i in range(len(imageArr)):
		print(imageArr[i].shape)
		plt.figure(i+1)
		plt.imshow(imageArr[i], vmax = vma, vmin = vmi, aspect = "auto", interpolation = "nearest")
	plt.show()