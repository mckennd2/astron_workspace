from __future__ import print_function
import numpy as np 
from sys import stdout

import scipy.signal as spys

import matplotlib.pyplot as plt

def determineCutoff(dataSet, thresholdVal, thresholdCounts, nAvg):
	####
	# Determine the cutoff point for the Fresnel knee in a power plot to determine the
	# 	index of the cutoff frequency for the power integral.
	####

	#Conver the data set to a log form, easier to analyse
	#logData = np.log(dataSet)

	# Get a rolling average over n data points the smooth out the curve and make the transition more pronouced,
	workSet = dataSet.copy()

	avgData = rollingAverage(workSet, nAvg)


	# Skipping the first 50 data points due to the asymptote at low frequencies, iterate
	#	through the remainder of the data
	print("##      Determining Cutoff frequency (Threshold) ##", end = '\r')
	stdout.flush()

	avgDataSlope = abs(avgData - np.roll(avgData, 1))
	
	trigger = avgDataSlope < thresholdVal
	triggerLoc = np.argwhere(trigger == True)
	plt.figure(2)
	print(trigger)
	plt.plot(trigger)
	plt.plot(avgData, c = 'r')
	plt.plot(avgData[triggerLoc], c = 'g')
	plt.show()

	if len(triggerLoc) > thresholdCounts:
		#print(triggerLoc[thresholdCounts])
		print(triggerLoc[thresholdCounts][0])
		return triggerLoc[thresholdCounts][0]
	else:
		print("Threshold Value not met at threshold counts, returning last value or last index")
		plt.figure(1)
		plt.plot(np.arange(2049), workSet)
		plt.figure(2)
		plt.plot(np.arange(2049), avgData)
		plt.show()
		if len(triggerLoc) == 0:
			return len(triggerLoc) -1
		else:
			return triggerLoc[-1]


def rollingAverage(dataSet, n):
	kernel = np.ones(int(n) * np.ones(len(dataSet.shape), dtype = int))
	kernel /= np.sum(kernel, axis = None) # Normalise the kernel

	dataSet /= np.mean(dataSet)

	return spys.convolve(dataSet, kernel,  'same', 'auto')




























# Depreciated Methods

def __rollingAverage(dataSet, n):
	####
	# For a given numpy array, convert each value into a rolling average of the n nearby values on
	# 	each side of the data point. Constructed to be used for large data sets with lossy information 
	#	at the start and end.
	####

	# Setup a 0 array to store the average values
	newSet = np.zeros(dataSet.shape)

	# Iterate over the given dataset (skipping the first n+1 and last n+1 pieces of data)
	
	# This could be improved by providing the local values instead of a 0, but np.copy() is 
	# 	significantly slower than getting a zero'd array.
	for i in range(len(dataSet) - n -1)[n+1:]:
		# Store the local value (faster than np.copy())
		newSet[i] = dataSet[i]

		# Add the n nearby values on each side of the current point
		for i1 in range(n)[1:]:
			newSet[i] += dataSet[i + i1]
			newSet[i] += dataSet[i - i1]

	# Determine the average of the values
	newSet /= (2 * n + 1)

	# Return the averaged data set.
	return newSet
