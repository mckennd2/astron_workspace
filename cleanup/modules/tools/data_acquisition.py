import h5py
import numpy as np 

def dataAcq(fid, segment, separation, offsetBeams):
	####
	# Acquire the data for both the main beam and offset beams for a given spectrum from hardcoded
	# 	links to a h5py file for the observation. Ensure the hardcoded values are correct for your dataset.
	####

	# Determine the size of the dataset accessible
	limit = fid["DYNSPEC_000/DATA"].shape[0]

	# Create an empty list to store the n off beam observations
	dataOffset = []

	# Ensure we aren't begining the data access above this limit, otherwise numpy will compain.
	if (segment) * (separation ) > limit:
		print("\n\n\n\n\n\n\n\n#####\nAttempting to Access Non-existant Data. Exiting...\n#####")
		return [],[]

	# Extract the on-xis beam as it's own variable.
	dataMain = fid["DYNSPEC_000/DATA"][separation * segment: separation * (segment +1),:,0].transpose()
	
	# For each request off axis beam, iterate through the system to extract the requested data and save them
	#	to the previously defined dataOffset list.
	print(offsetBeams)
	for i in range(offsetBeams):
		dataOffset.append(fid.items()[2 + i][1].items()[3][1]
			[separation * segment: separation * (segment + 1), :, 0].transpose())

	dataOffset = np.asarray(dataOffset)
	# Return the data requested.
	return dataMain, dataOffset

def dict_clean_creator(fileName, freqArrIndex, beamCount):
	dictArr = []
	with h5py.File(fileName, 'r') as readFile:
		for i in range(beamCount):
			if i == freqArrIndex:
				dictArr.append(readFile.items()[i][0].encode('ascii', 'ignore')
					+ "/" + readFile.items()[i][1].items()[1][0].encode('ascii', 'ignore'))
			else:
				dictArr.append(readFile.items()[i][0].encode('ascii', 'ignore')
					+ "/" + readFile.items()[i][1].items()[0][0].encode('ascii', 'ignore'))
		readFile.close()

	return dictArr

def rollingAverage(readFile, dictString, startIdx, endIdx, i, kernelLength):
	kernel = np.ones(kernelLength)
	kernel /= np.sum(kernel, axis = None)

	offset = int(np.round(kernelLength / 2.))

	limit = readFile[dictString].shape[0] / (endIdx - startIdx)
	print(limit, kernelLength)
	extremae = [0, limit]

	returnDataset = []

	beam = readFile[dictString][startIdx: endIdx, ...]

	if not i in extremae:
		beamForwardPad = __forwardPad(readFile, dictString, startIdx, offset, i)
		beamEndPad = __backPad(readFile, dictString, endIdx, offset, i)
	else:
		if i == min(extremae):
			beamEndPad = __backPad(readFile, dictString, endIdx, offset, i)
			beamForwardPad = beam[..., startIdx + offset: startIdx:-1]
		else:
			beamForwardPad = __forwardPad(readFile, dictString, startIdx, offset, i)
			beanEndPad = beam[..., endIdx: endIdx - offset:-1]

	print(beam.shape, beamForwardPad.shape, beamEndPad.shape)
	beamMod = np.concatenate([beamForwardPad, beam, beamEndPad], axis = 1)

	rollAvg = __rollingAverageWorker(beamMod, kernel)

	rollAvgCulled = rollAvg[..., offset : -1 * (offset)]

	assert((rollAvgCulled.shape == beam.shape))

	return rollAvgCulled

def __rollingAverageWorker(dataset, kernel):
	# Faster than convolve2d by a factor of 6???
	returnArr = []
	for idx, row in enumerate(dataset):
		returnArr.append(np.convolve(row, kernel, 'same'))

	return np.asarray(returnArr)

def __forwardPad(readFile, dictString, startIdx, offset, i):
	return readFile[dictString][startIdx - offset: startIdx, ...]
def __backPad(readFile, dictString, endIdx, offset, i):
	return readFile[dictString][endIdx: endIdx + offset, ...]
