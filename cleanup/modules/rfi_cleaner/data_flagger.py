import numpy as np
import matplotlib.pyplot as plt


def channelFlagger(data,trigger):
	# KNOWN ISSUE: Cannot handle RFI source over more than 2 consequetive channels

	###
	# Given a dataset and a threshold between channels, flag data for suspected RFI
	###


	# Generate arrays of the data shifted one up and one down
	shiftUp = np.roll(data, -1, axis=0)
	shiftDown = np.roll(data,1, axis = 0)

	# Check if either the channel above or below each point is above the threshold
	triggeredArr = ((data > shiftUp + trigger).astype(int) 
		+ (data > shiftDown + trigger).astype(int))

	# Limit return values to be true (1.) or false (0.), sequential channels may have 
	#	generated values of 2 we need to account for.
	triggeredArr = np.clip(triggeredArr, -1., 1.)

	# Return array of flags
	return triggeredArr.astype(int)


def channelFlagger_Simpl(data,triggerChannel, triggerThreshold):
	# KNOWN ISSUE: Cannot handle RFI source over more than 2 consequetive channels

	###
	# Given a dataset and a threshold between channels, flag data for suspected RFI
	###


	# Generate arrays of the data shifted one up and one down
	shiftUp = np.roll(data, -1, axis=0)
	shiftDown = np.roll(data,1, axis = 0)

	# Check if either the channel above or below each point is above the threshold
	triggeredArr = ((data > shiftUp + triggerChannel).astype(int) 
		+ (data > shiftDown + triggerChannel).astype(int))

	#plt.figure(1)
	#plt.imshow(data, aspect = "auto", interpolation = "nearest")
	#plt.figure(2)
	#plt.imshow(triggeredArr, aspect = "auto", interpolation = "nearest")

	triggeredArr += (data > np.percentile(data, triggerThreshold)).astype(int)
	#triggeredArr += (data > (1. + triggerDefault)).astype(int)

	#plt.figure(3)
	#plt.imshow(triggeredArr- 2. * (data > (1. + triggerDefault)).astype(int) , aspect = "auto", interpolation = "nearest")

	plt.show()
	# Limit return values to be true (1.) or false (0.), sequential channels may have 
	#	generated values of 2 we need to account for.
	triggeredArr = np.clip(triggeredArr, -1., 1.)
	
	testArr = np.sum(triggeredArr, axis = 1)
	triggeredArr[testArr > data.shape[1] * 0.66, :] = 1.

	# Return array of flags
	return triggeredArr.astype(int)

def temporalFlagger(data, percentile = 99.7):
	###
	# Given a data set and a percentile, see if a time sample's median is above
	#	the nth sample of an expect normal distrobution for the data. Flag if true.
	###

	# Get the median values for each time sample
	medianVal = np.median(data, axis = 0)

	# Determine the value corresponding to the percentile given in the normal distrobution
	extremae = np.percentile(medianVal, percentile, axis = 0)

	# Generate an array of true (1.) or false (0.) flags for if the data is beyond the
	#	given percentile.
	flaggedTime = medianVal > extremae

	# Return the array of flags
	return flaggedTime.astype(int)
