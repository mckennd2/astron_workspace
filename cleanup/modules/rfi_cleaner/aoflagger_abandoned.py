from __future__ import print_function
from sys import stdout

import numpy as np
import scipy.signal as spys
from numpy.lib.stride_tricks import as_strided

import matplotlib.pyplot as plt


def ScaleInvariantRankOperator(Omega, eta = 0.2): # returns: Omega Prime
	'''
	Implementation of Offringa et al's defining routine behind AOFlagger, 
		reimplemented in python to function on H5 datasets for the 
		dynamic spectrum pipeline. Written with intent for cleaning
		observations to perform interplanetary scintillation analysis,
		but should be general enough to work on any datasets.

	Inputs:
		REMOVED --- N: The size of the input array
		Omega: The input flags array
		eta: An 'agressiveness' parameter for the function.

	Returns:
		OmegaPrime: the output flags array

	'''
	N = Omega.shape[0]
	idxRange = np.arange(N)
	print(N)

	psi = np.ones(N) * (eta - 1.)
	psi[Omega.astype(bool)] = eta
#	plt.figure(1)
#	plt.plot(psi)

	M = np.concatenate([[0.], np.cumsum(psi)])
#	plt.figure(2)

	P = cuargmin(M[:N])
	Q = np.flipud(N - cuargmax(np.flipud(M[1:])))
#	plt.plot(P)
#	plt.plot(Q)
#	plt.plot(M[Q] - M[P])
#	plt.figure(1)
#	plt.plot((M[Q] - M[P]) >= 0.)
#	plt.show()
#	print(P.shape, Q.shape)
	OmegaPrime = np.zeros(N, dtype = int)
	OmegaPrime[(M[Q] - M[P]) >= 0.] = 1

	return OmegaPrime.astype(bool)

def SumThreshold(maskedArray, M, threshold, stDev = [], plot = False):
	'''
	Implementation of Offringa et al.'s SumThreshold method for RFI
		identification, reimplemented in python with intent to
		function oh H5 datasets.

	Inputs:
		inputRow: Current sample to test
		currentMask: Currently flagged data points
		M: length of the current sequence window
		chi: Who the fuck knows.

	Return:
		newMask: An updated version of currentMask
	'''
	pltc = maskedArray.copy()
	# Preparations
	maskedArray = maskedArray / np.ma.median(maskedArray)
	if stDev == []:
		stDev = customStd(maskedArray, median)
	#maskedArray = np.ma.concatenate([maskedArray, np.ones(M) * np.mean(maskedArray[-M * 2 + 1:])])	
	'''
	maskRoll = rolledArr(currentMaskPad)
	dataRoll = rolledArr(dataPad)
	segmentRoll = np.vectorize(multiRoller, otypes = [object])
	
	
	kernels = np.vstack(segmentRoll(maskRoll, np.arange(M)))[:, :-1 * M]
	segments = np.vstack(segmentRoll(dataRoll, np.arange(M)))[:, :-1 * M]

	products = np.sum((kernels * segments), axis = 0)
	counter = np.sum(kernels, axis = 0)
	'''
	products = np.ma.convolve(maskedArray, np.ones(M), "full", False)[M - 1:] # Index hack as "same" keyword is broken
	counter = np.convolve(1 - maskedArray.mask.astype(int), np.ones(M), "full")[M - 1:]
	
	counter[counter == 0.] = np.ma.masked
	rollingMean = np.ma.divide(products, counter)
	
	rollingMean[maskedArray.mask] = 1.
	flagIdx = (rollingMean > (threshold * stDev + 1.)) 
	flagIdx += (rollingMean < (1. - stDev * threshold))
	
	flagIdx[maskedArray.mask] = False
	if plot:
		plt.figure(3)
		plt.plot(pltc.mask)
		plt.figure(4)
		plt.plot(products)
		plt.figure(5)
		plt.plot(counter)
		plt.figure(6)
		plt.plot(rollingMean)	
		plt.figure(1)
		plt.plot(flagIdx)
	
	for offset in np.arange(1,M):
		flagIdx += rollPad(flagIdx, offset)
	maskedArray[flagIdx] = np.ma.masked
	if plot:
		plt.figure(2)
		plt.plot(maskedArray.mask)
		plt.show()
	
	return maskedArray.mask

def rollPad(array, shift):
	returnVar = np.roll(array, -1 * shift)
	returnVar[-1 * np.arange(shift - 1) -1] = False

	return returnVar


def routine(dataChunk, iterationsSumThreshold = 3, thresholdInit = 2., thresholdStep = 1.33, etaHoriz = 0.2, etaVert = 0.2, stdClean = 2.):
	dataChunk = np.ma.array(dataChunk, mask = np.zeros(dataChunk.shape))
	dataWork = dataChunk.copy()
	
	randomCol = np.random.randint(0, dataWork.shape[1], int(0.1 * dataWork.shape[1]), dtype = int)
	medianChannelsRaw = np.ma.median(dataWork[:, randomCol], axis = 1)
	
#	plt.figure(1)
#	plt.plot(medianChannelsRaw)
	medianChannels = medianClean(medianClean(medianChannelsRaw, stdClean), 4 * stdClean)
#	plt.figure(1)
#	plt.plot(medianChannels, c = 'g')
#	medianChannels = spys.savgol_filter(medianChannels[:-1], len(medianChannels) - 1, 5)
#	medianChannels = np.append(medianChannels, medianChannels[-1])
	medianChannels = rollingAverage(rollingAverage(medianChannels, 10), 5)	
	medianChannels[-1] = medianChannelsRaw[-1] # Rolling average causes issues
	dataWork = np.ma.divide(dataWork.T, medianChannels).T
	
#	plt.figure(1)
#	plt.plot(medianChannels)
	
#	plt.figure(3)
#	plt.plot(dataWork.T[0])
#	plt.figure(4)
#	plt.plot(dataChunk.T[0])
	plt.show()

#	dataWork = np.ma.divide(dataWork.T, rollingAverage(np.median(dataWork[:, np.random.randint(0, dataWork.shape[1], int(0.1 * dataWork.shape[1]), dtype = int)], axis = 1), 0.1 * dataWork.shape[1])).T

	thresholdPowers = np.arange(iterationsSumThreshold, 0., -1.)
	thresholdValues = np.power(thresholdStep, thresholdPowers)
	#horizMed = np.median(dataWork, axis = 0)
	#horizStd = customStd(dataWork, horizMed)
	
	# Limit the std to reasonable values to account for channel blowout
	#horizStd = np.clip(horizStd, 0., 10.)
	#print(horizStd, horizStd.shape)
	dirtyClean = spys.convolve(dataWork, np.asarray([[0,-1,0],[0,2,0], [0,-1,0]]), "same") > 3.
        dataWork[dirtyClean] = np.ma.masked

	for idx, threshold in enumerate(reversed(thresholdValues)):
		horizMed = np.median(dataWork, axis = 1)
        	horizStd = customStd(dataWork.T, horizMed)
		horizStd = np.divide(horizStd, np.ma.median(dataWork, axis = 0))
		plt.plot(horizStd,c = 'g')
		horizStd = np.clip(horizStd,  0.125 * np.exp(-1. * (idx) / 1.33), 7.)
		horizStd = rollingAverage(rollingAverage(horizStd, 15), 10)
		horizStd = np.clip(horizStd,  0.125 * np.exp(-1. * (idx) / 1.33), 7.)
		plt.plot(horizStd, c = 'r')
		plt.show()
		print(thresholdValues)
		
		for sampleSize in [1]:
			print(threshold)
			thresholdVar = threshold /  (1.5 ** (np.log2(sampleSize)))
			print((1.5 ** (np.log2(sampleSize))))
			print("##        Processing Window {0} with Threshold {1}        ##".format(sampleSize, thresholdVar * thresholdInit), end = "\r")
			stdout.flush()
			mask = []
			plotBool = False
			for idx, row in enumerate(dataWork.T):
				if (idx % 190000 == 0):
					print(idx)
					plotBool = False
				dataWork[SumThreshold(row, sampleSize, thresholdInit * thresholdVar, horizStd, plotBool), idx] = np.ma.masked
				plotBool = False
			
			plt.imshow(dataWork.mask.astype(int), aspect = "auto")
			plt.figure(2)
			plt.imshow(dataWork, aspect = "auto")
			plt.show()
	
	print("####        Aplying SIRO to Rows        ####", end = "\r")
	stdout.flush()

	dataSIRWork = dataWork.copy()
	for idx, row in enumerate(dataWork.mask):
		dataSIRWork[idx, ScaleInvariantRankOperator(row, etaHoriz)] = np.ma.masked
		if idx % 1000 == 0:
			plt.imshow(dataSIRWork.mask.astype(int), aspect = "auto")
			plt.show()
	plt.imshow(dataSIRWork.mask.astype(int), aspect = "auto")
	plt.show()
	print("####        Applying SIRO to Columns        ####", end = "\r")
	stdout.flush()
	
	plt.imshow(dataSIRWork.mask.astype(int), aspect = "auto")
	plt.show()
	for idx, col in enumerate(dataWork.T.mask):
		dataSIRWork.T[idx, ScaleInvariantRankOperator(col, etaVert)] = np.ma.masked

	return dataSIRWork.mask
#	return dataWork



def customStd(values, appMean):
	return np.sqrt(np.mean(np.square(values - appMean), axis = 0))

def rolledArr(array):
	return lambda count: np.roll(array, -1 * int(count))

def multiRoller(lambdaFunc, idx):
	return lambdaFunc(idx)

def cuargmax(row):
    __cuargmax = np.frompyfunc(
    	lambda prevVar, nextVar: 
    		nextVar if row[nextVar] > row[prevVar] else prevVar, 
    															2, 1)

    return __cuargmax.accumulate(
    			np.arange(0, len(row)), 0, dtype=np.object).astype(int)

def cuargmin(row):
    __cuargmin = np.frompyfunc(
    	lambda prevVar, nextVar:
    		nextVar if row[nextVar] < row[prevVar] else prevVar,
    															2, 1)

    return __cuargmin.accumulate(
    			np.arange(0, len(row)), 0, dtype=np.object).astype(int)


def ReferenceSumThreshold(inputRow, currentMask, M, chi):
	'''
	Direction implementation of pseudocode.

	Implemented of Offringa et al.'s SumThreshold method for RFI
		identification, reimplemented in python with intent to
		function oh H5 datasets.

	Inputs:
		inputRow: Current sample to test
		currentMask: Currently flagged data points
		M: length of the current sequence window
		chi: Who the fuck knows.

	Return:
		newMask: An updated version of currentMask
	'''

	currSum = 0.
	counter = 0
	newMask = currentMask.copy()

	for initialIdx in range(M):
		if not currentMask[initialIdx]:
			currSum += inputRow[initialIdx]
			counter += 1

	for idx in range(M, len(inputRow)):
		if abs(currSum) > chi:
			newMask[idx - M: idx] = 1

		if not currentMask[idx]:
			currSum += inputRow[idx]
			counter += 1
		if not currentMask[idx - M]:
			z -= inputRow[idx - M]
			count -= 1

	return newMask


def rollingAverage(dataSet, n):
	kernel = np.ones([int(n), 1])
	kernel /= np.sum(kernel, axis = None) # Normalise the kernel
	
	padDataset1 = np.mean(dataSet[:n]) * np.ones(n)
	padDataset2 = np.mean(dataSet[-5:]) * np.ones(int(n / 2.))
	paddedDataset = np.concatenate([padDataset1, dataSet, padDataset2])
	return spys.convolve(paddedDataset.reshape(-1, 1), kernel,  'same', 'symm').reshape(-1)[n:-int(n / 2)]

'''
def medianClean(medianChannels, stdMul):
        stdMed = np.std(medianChannels)
        medMed = np.median(medianChannels)
        flags = (abs(medianChannels - medMed) > 2. * stdMed)
        plt.figure(2)
        plt.plot(medianChannels - medMed)
        plt.plot(np.arange(400), (medMed + 2. * stdMed) * np.ones(400))
        notFirst = False
        for idx, flag in enumerate(flags):
                offset = -1
                if flag:
                        while flags[idx + offset]:
                                offset -= 1
                                if not notFirst:
                                        offset += 2
                        print(offset, idx)
                        print(medianChannels[idx + offset])
                        medianChannels[idx] = medianChannels[idx + offset]
                notFirst = True
	return medianChannels
'''

def medianClean(medianChannels, thresFac):
	#Future: Consider early channels being flagged.
	slope = np.abs(medianChannels - np.roll(medianChannels, 1))
	
	slpMean = np.median(slope)
	
	flags = (slope) > (thresFac * slpMean)

        for idx, flag in enumerate(flags):
                offset = -1
                if flag:
                        while flags[idx + offset]:
                                offset -= 1
                        medianChannels[idx] = medianChannels[idx + offset]

	return medianChannels
