import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as spys


def routine(dataChunk, thresholdValue = 0.2):
	median = np.median(dataChunk, axis = 1)
	median, flags = medianClean(median, 5.)
	median = rollingAverage(rollingAverage(median, 15), 5)	

	dataChunk[flags] = np.ma.masked

	#plt.imshow(dataChunk, aspect = "auto")
	#plt.show()
	
	dataWork = dataChunk.copy()
	dataWork = np.transpose(dataWork.T / median)

	std = np.ma.std(dataWork, axis = 1)
	plt.plot(std)
	plt.show()
	while np.mean(std) > thresholdValue:
		
		smoothStd, flagsChannel = stdClean(std, 5.)
		plt.plot(smoothStd, flagsChannel)
		smoothStd = rollingAverage(std, 5)
		plt.plot(smoothStd)
		dataWork[flagsChannel] = np.ma.masked
		flags = np.abs(dataWork.T - 1.) > 5. * std
		plt.figure(2)
		plt.imshow(flags,aspect = "auto")
		plt.show()
		dataWork[flags.T] = np.ma.masked
		median = np.ma.median(dataWork, axis = 1)
		std = customStd(dataWork.T, median)
		plt.plot(std)
		plt.show()

	plt.imshow(dataWork, aspect = "auto")
	plt.show()

	
def customStd(dataSet, median):
	return np.sqrt(np.mean(np.square(dataSet - median), axis = 0))

def rollingAverage(dataSet, n):
	kernel = np.ones([int(n), 1])
	kernel /= np.sum(kernel, axis = None) # Normalise the kernel
	
	padDataset1 = np.mean(dataSet[:n]) * np.ones(n)
	padDataset2 = np.mean(dataSet[-5:]) * np.ones(int(n / 2.))
	paddedDataset = np.concatenate([padDataset1, dataSet, padDataset2])
	return spys.convolve(paddedDataset.reshape(-1, 1), kernel,  'same', 'symm').reshape(-1)[n:-int(n / 2)]


def medianClean(medianChannels, thresFac):
	#Future: Consider early channels being flagged.
	slope = np.abs(medianChannels - np.roll(medianChannels, 1))
	
	slpMean = np.median(slope)
	
	flags = (slope) > (thresFac * slpMean)

        for idx, flag in enumerate(flags):
                offset = -1
                if flag:
                        while flags[idx + offset]:
                                offset -= 1
                        medianChannels[idx] = medianChannels[idx + offset]

	return medianChannels, flags

def stdClean(medianChannels, thresFac):
        #Future: Consider early channels being flagged.
	plt.plot(medianChannels)
	plt.plot(np.arange(len(medianChannels)), np.median(medianChannels))
	plt.show()
        slpMean = np.median(medianChannels)

        flags = (medianChannels) > (thresFac * slpMean)
	plt.plot(flags)
	plt.show()
        for idx, flag in enumerate(flags):
                offset = -1
                if flag:
                        while flags[idx + offset]:
                                offset -= 1
                        medianChannels[idx] = medianChannels[idx + offset]

        return medianChannels, flags


