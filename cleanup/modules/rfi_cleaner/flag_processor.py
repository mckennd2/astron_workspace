from __future__ import print_function
from sys import stdout

import h5py
import numpy as np

import time 

from ..tools import data_acquisition
import data_flagger as flagger

import matplotlib.pyplot as plt

# Dev mode
reload(data_acquisition)
reload(flagger)
def autoProcessRawData(originalFile, outputFile, nSamples, beamCount, chunksize, thresholdPercentile = 99.):
	with h5py.File(originalFile, 'r') as dataFile:
		mainShape = np.asarray(dataFile["DYNSPEC_000/DATA"].shape)[:2]
		dt = dataFile.attrs.items()[31][1] / 1e6

		offShape = mainShape.copy()
		mainShape = np.asarray(dataFile["DYNSPEC_000/DATA"].shape)[:2]
		offShape = mainShape.copy()
	
		if offShape[0] > nSamples * chunksize:
			offShape[0] = nSamples * chunksize
			mainShape[0] = nSamples * chunksize
	
	
		with h5py.File(outputFile, 'a') as newFid:
			mainSet = newFid.create_dataset("DYNSPEC_000/DATA", shape = mainShape, dtype=np.float32, 
				compression = "lzf")
	
			newFreq = newFid.create_dataset("DYNSPEC_000/COORDINATES/SPECTRAL", shape = [0], dtype = np.float16)
	
	
			for i in range(7): # Magic number to copy frequency array to original location, copy all metadata up to the array
				newFreq.attrs.create(dataFile["DYNSPEC_000/COORDINATES/SPECTRAL"].attrs.items()[i][0], 
					dataFile["DYNSPEC_000/COORDINATES/SPECTRAL"].attrs.items()[i][1])
	
			offSet = np.array(np.ones(beamCount - 1), dtype=object)
	
			for i in range(beamCount - 1):
				offSet[i] = newFid.create_dataset("DYNSPEC_{0:03d}/DATA".format(i+1), shape = offShape, 
					dtype=np.float32, compression = "lzf")
	
			for i in range(nSamples):
				print("                                      ", end = "\r")
				print("####			Flagging Time Segment {0}			####".format(i + 1))
	
				mainFlagged, offFlagged = processRawData(dataFile, dt, i, beamCount - 1, chunksize, thresholdPercentile)
	
				print("Writing to disk (Main Beam)                                     ", end = '\r')
				stdout.flush()
	
				if not i == (nSamples -1):
					__offBeamHandler(offSet, offFlagged, i, chunksize)
					mainSet[i * chunksize : (i+1) * chunksize, ...] = mainFlagged.transpose()
	
				else:
					mainSet[i * chunksize:, ...] = mainFlagged.transpose()
					__offBeamHandler(offSet, offFlagged, i, chunksize, True)
	
	
			newFid.close()
		dataFile.close()

def processRawData(dataFile, dt, sample, beamCount, chunksize = 60000, thresholdPercentile = 99.):
	print("		##	Acquiring Data	##		", end = '\r')
	stdout.flush()
	main, offArr = data_acquisition.dataAcq(dataFile, sample, chunksize, beamCount)

	print("		##	Flagging Main Beam	##		", end = '\r')
	stdout.flush()
	mainFlagged, mask2 = autoFlagger(main, 0.2, thresholdPercentile)
	mainFlagged, mask = autoFlagger(mainFlagged, 0.2, 99.5)

	mask += mask2
	maskClip = np.clip(mask, -1., 1.)
	assert(mainFlagged.shape == main.shape)
	offFlagged = []

	for idx, offbeam in enumerate(offArr):
		print("                                      ", end = "\r")
		print("		##	Flagging Offset beam {0}	##".format(idx + 1))
		flaggedImage, __ = autoFlagger(offbeam, 0.2, 99.5)
		
		flaggedImage *= maskClip
		
		offFlagged.append(flaggedImage)

	offFlagged = np.asarray(offFlagged)

	print(offFlagged.shape, offArr.shape)
	assert(offFlagged.shape == offArr.shape)

	return mainFlagged, offFlagged

def __offBeamHandler(fileRef, beams, i, chunksize, lastElement = False):
	if not lastElement:
		for idx, beam in enumerate(beams):
			print("Writing to disk (Offset beam {0})                                     ".format(idx +1), end = '\r')
			stdout.flush()

			fileRef[idx][i * chunksize: (i+1) * chunksize, ...] = beam.transpose()
	else:
		for idx, beam in enumerate(beams):
			print("Writing to disk (Offset beam {0})                                     ".format(idx +1), end = '\r')
			stdout.flush()
			
			fileRef[idx][i * chunksize:, ...] = beam.transpose()


def autoFlagger(inputIm, flaggerConst, thresholdPercentile):
	###
	# Given a dataset, process and heal and RFI within it.
	###

	# Make a working copy of the data
	flagsCopy = inputIm.copy()
	processCopy = inputIm.copy()

	# If the data isn't normalised or is somehow very extreme, try again
	if np.max(flagsCopy) > 10.:
		median_values = np.median(flagsCopy, axis = 0)
		flagsCopy = flagsCopy / median_values

	print("Data Normalised                                      ", end = '\r')
	stdout.flush()
	
	# Perform channel flagging on the dataset
	channelFlag = flagger.channelFlagger_Simpl(flagsCopy, flaggerConst, thresholdPercentile)

	print("Channel Flagging Performed                                      ", end = '\r')
	stdout.flush()

	# Create a simple array for converting the flagged data points to 0 through
	# 	elementwise multiplication.
	nullPoints = 1. - channelFlag
	removedNoiseImage = processCopy * nullPoints
	
	print("Flagged Data Removed                                      ", end = '\r')
	stdout.flush()

	#plt.figure(1)	
	#plt.imshow(inputIm, aspect = "auto", interpolation = 'nearest')
	#plt.figure(2)
	#plt.imshow(removedNoiseImage, aspect = "auto", interpolation = "nearest")
	#plt.show()

	# Return a large amount of debug data if needed / for comparison.
	#timeFlag = np.zeros(flagsCopy.shape) + timeFlag
	#return cleanImage, [channelFlag, timeFlag, combinedFlag, flagsCopy, removedNoiseImage, timeHealed, healedImage, healedImage2, cleanImage]

	return removedNoiseImage, nullPoints
