import numpy as np

###
# A set of functions to help determine the intensity of a given observation
#	through various sampling methods.
###


def mean(mainBeam, offsetBeam):
	offsetBeam = np.mean(offsetBeam, axis = 0)

	return __worker(mainBeam, offsetBeam)

def median(mainBeam, offsetBeam):
	print("Intensity shapes")
	print(offsetBeam.shape, mainBeam.shape)
	mainBeam = np.median(mainBeam)
	offsetBeam = np.median(offsetBeam, axis = [1,2])
	print(offsetBeam.shape, mainBeam.shape)

	return __worker(mainBeam, offsetBeam)

def min(mainBeam, offsetBeam):
	offsetBeam = np.min(offsetBeam, axis = 0)

	return __worker(mainBeam, offsetBeam)

def percentile(mainBeam, offsetBeam, percentile = 10.):
	offsetBeam = np.percentile(offsetBeam, percentile, axis = 0)

	return __worker(mainBeam, offsetBeam)

def raw(mainBeam, offsetBeam):
	offsetBeam = np.transpose(offsetBeam, axes = (1,2,0))

	return __worker(mainBeam, offsetBeam)


def __worker(mainBeam, offsetBeam):
	print(mainBeam.shape, offsetBeam.shape)
	meanIntensity = mainBeam - offsetBeam
	return meanIntensity
