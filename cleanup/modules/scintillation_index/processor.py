from __future__ import print_function
from sys import stdout

import numpy as np 
import h5py

import index_calcuator as indexCalc

import matplotlib.pyplot as plt

from ..tools import data_acquisition as dataTools

# "/media/chrx/1336ADCD09F54879/Series/L649109_3C48_CS_148to167MHz_processed.h5"


# Devtools
reload(indexCalc)
reload(dataTools)

def mass_determine_m_index(fileName, dataSetsDict, segments, chunksize, dt = 0.01048576, welchPercentile = 10,
						autoCutoff = True, intensityMethod = '', powerMethod = '', percentilePower = 0.,
						percentileIntensity = 0., dataFormat = "normalised"):
	with h5py.File(fileName, 'r') as readFile:

		mIdxArr = np.asarray([])
		for i in range(segments):
			startIdx = i * chunksize
			endIdx = (i+1) * chunksize

			mainBeam = readFile[dataSetsDict[0]][startIdx: endIdx, ...].transpose()
			print(mainBeam.shape)
			print(mainBeam[0, 0].shape)
			offsetBeams = np.zeros([mainBeam.shape[0], mainBeam.shape[1], len(dataSetsDict) - 1])
			for idx, dictString in enumerate(dataSetsDict[1:]):
				offsetBeams[..., idx] = readFile[dictString][startIdx: endIdx, ...].transpose()
			offsetBeams = np.transpose(offsetBeams, axes = (2,0,1))

			mIdx = indexCalc.auto_index_method(mainBeam, offsetBeams, dt, welchPercentile, autoCutoff, 
						intensityMethod, powerMethod, percentilePower, percentileIntensity)
			print("## Data segment {0} processed ##                        ".format(i+1))
			stdout.flush()

			mIdxArr = np.append(mIdxArr, mIdx)
		readFile.close()

	return mIdxArr


def automated_processor(fileName, beams, segments = 5,chunksize = 60000, freqArrIdx = 0, dataFormatVar = "normalised"):
	dictArr = dataTools.dict_clean_creator(fileName, freqArrIdx, beams)
	print("Processing Datasets {0}".format(dictArr))

	mIdxArr = mass_determine_m_index(fileName, dictArr, segments, chunksize, intensityMethod = 'median',
		powerMethod = 'median', dataFormat = dataFormatVar)

	return mIdxArr
