from __future__ import print_function
from sys import stdout

from scipy.signal import welch
import numpy as np

import power as powerCalc
import intensity as intensityCalc

from ..tools import index_tools as tools

import matplotlib.pyplot as plt


#Devmode 

reload(tools)
reload(powerCalc)
reload(intensityCalc)



def data_preparation(dataBeam, dataOffsetBeams, welchPercentile = 10, autoCutoff = True, 
	dt = 0.01048576, freqThreshold = 1e-3, freqHits = 25, freqAvg = 20):
	print("## Performing Main Beam Welch Operation ##                   ", end = '\r')
	stdout.flush()
	plt.figure(1)
	plt.imshow(dataBeam, aspect = "auto")
	#plt.show()
	freqM, welchM = welch(np.percentile(dataBeam,10, axis = 0), fs = 1. / dt, 
		nperseg = 4096, return_onesided=True, window = ("kaiser", 8.6))
	plt.figure(9)
	plt.loglog(freqM, welchM)
	#raw_input("?")
	if len(dataOffsetBeams.shape) != 3:
		dataOffsetBeamsFix = np.zeros([1, dataOffsetBeams.shape[0], dataOffsetBeams.shape[1]])

		dataOffsetBeamsFix[0, :, :] = dataOffsetBeams
		dataOffsetBeams = dataOffsetBeamsFix

	welchO = []

	for idx, offBeam in enumerate(dataOffsetBeams):
		print("## Performing Off Beam {0} FFT ##                       ".format(idx + 1), end = '\r')
		stdout.flush()

		freqO, welchOEle = welch(np.median(offBeam, axis = 0), fs = 1. / dt, 
			nperseg = 4096, return_onesided=True, window = ("kaiser", 8.6))

		assert((freqO == freqM).all())

		welchO.append(welchOEle)

	welchO = np.asarray(welchO)
	#plt.figure(10)
	#plt.loglog(welchO.transpose())
	#plt.show()
	print("## Determining Cutoff Frequency ##                        ", end = '\r')
	stdout.flush()
	if autoCutoff:
		cutoffFreqIdx = tools.determineCutoff(welchM, freqThreshold, freqHits, freqAvg)
	else:
		plt.loglog(welchM)
		plt.show()
		cutoffFreqIdx = int(raw_input("Cut off Idx?"))


	return welchM, welchO, freqM, cutoffFreqIdx



def default_index_method(welchM, welchO, mainBeam, offsetBeam, freqM, cutoffIdx):
	powerIntegral = powerCalc.median(welchM, welchO, freqM, cutoffIdx)
	meanIntensity = intensityCalc.median(mainBeam, offsetBeam)
	meanIntensitySq = np.square(meanIntensity)
	print("HIT")
	mValues = np.sqrt(powerIntegral / meanIntensitySq)

	print(powerIntegral, meanIntensity, meanIntensitySq)
	print(mValues)
	return mValues


def slow_index_custom(welchM, welchO, mainBeam, offsetBeam, freqM, cutoffIdx,
	intensityMethod, powerMethod, percentilePower = 0., percentileIntensity = 0.):

	powerFunc = getattr(powerCalc, powerMethod)
	intensityFunc = getattr(intensityCalc, intensityMethod)

	if powerMethod != "percentile":
		powerIntegral = powerFunc(welchM, welchO, freqM, cutoffIdx)
	else:
		powerIntegral = powerFunc(welchM, welchO, freqM, cutoffIdx, percentilePower)

	if intensityMethod != "percentile":
		meanIntensity = intensityFunc(mainBeam, offsetBeam)
	else:
		meanIntensity = intensityFunc(mainBeam, offsetBeam, percentileIntensity)

	meanIntensitySq = np.square(meanIntensity)

	mValues = powerIntegral / meanIntensitySq
	mValues = np.sqrt(mValues)
	print(mValues)

	return mValues



def auto_index_method(dataBeam, dataOffsetBeams, dt = 0.01048576, welchPercentile = 10,
	autoCutoff = True, intensityMethod = "", powerMethod = "", percentilePower = 0., 
	percentileIntensity = 0.):

	dataBeam = np.ma.masked_where(dataBeam == 0., dataBeam)
	dataOffsetBeams = np.ma.masked_where(dataOffsetBeams == 0., dataOffsetBeams)
	welchM, welchO, freqM, cutoffIdx = data_preparation(dataBeam, dataOffsetBeams, 
		welchPercentile, autoCutoff)


	#if intensityMethod == "" and powerMethod == "":
	return default_index_method(welchM, welchO, dataBeam, dataOffsetBeams, freqM, cutoffIdx)
	#else:
	#	return slow_index_custom(welchM, welchO, dataBeam, dataOffsetBeams, freqM, cutoffIdx,
	#			intensityMethod, powerMethod, percentilePower, percentileIntensity)
