import numpy as np

###
# A module to determine the power integral through the trapezoid
#	integration routine, using verious methods to weight the 
# 	influence of the combined beams on the output result.
###

def mean(mainPower, offsetPower, freqArr, cutoffIdx):
	offsetPower = np.mean(offsetPower, axis = 0)

	return __worker(mainPower, offsetPower, freqArr, cutoffIdx)

def median(mainPower, offsetPower, freqArr, cutoffIdx):
	return __worker(mainPower, offsetPower, freqArr, cutoffIdx)

def min(mainPower, offsetPower, freqArr, cutoffIdx):
	offsetPower = np.min(offsetPower, axis = 0)

	return __worker(mainPower, offsetPower, freqArr, cutoffIdx)

def percentile(mainPower, offsetPower, freqArr, cutoffIdx, percentile = 10.):
	offsetPower = np.percentile(offsetPower, percentile, axis = [1,2])

	return __worker(mainPower, offsetPower, freqArr, cutoffIdx)

def raw(mainPower, offsetPower, freqArr, cutoffIdx):
	return __worker(mainPower, offsetPower, freqArr, cutoffIdx)



def __worker(mainPower, offsetPower, freqArr, cutoffIdx):
	truePower = mainPower - offsetPower
	print(truePower.shape)

	truePower = truePower.transpose()
	print(truePower.shape)
	return np.trapz(y = truePower[:cutoffIdx], x = freqArr[:cutoffIdx], axis = 0)
