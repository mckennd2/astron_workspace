import matplotlib.pyplot as plt
import numpy as np

global mIdx
mIdx = np.load("./real_midx.npy")

mIdx = mIdx.reshape(69,6)

def demo():
	global mIdx

	mWork = mIdx.copy()
	badLoc = mWork > 0.3
	mWork[badLoc] = np.nan

	plt.figure(1)
	plt.plot(range(69), mWork)
	plt.ylim = [0, 0.3]


	plt.figure(2)
	plt.hist(mWork)

	mWork[badLoc] = 0.
	for i in range(6):
		plt.figure(i + 3)
		plt.hist(mWork[:, i][mWork[:, i] != 0.])

	std = np.std(mWork, axis = 1)
	print(std)
	plt.show()

demo()