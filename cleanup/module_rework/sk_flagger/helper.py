import numpy as np 


def S_1(data):
	# input: timesamples x channels x arbaxis
	return np.sum(np.mean(data, axis = 3), axis = 0)

def S_2(data):
	return np.sum(np.square(np.mean(data, axis = 3)), axis = 0)

def sk_est(data, d = 1.):
	dShape = data.shape 
	M = float(dShape[0])
	N = float(dShape[3]) * 1024.

	print(M, N)

	S1 = S_1(data)
	S2 = S_2(data) 

	mean = S1 / M
	meanSq = np.square(mean)
	stdDev = np.divide(np.multiply(M, S2) - np.square(S1), M * (M - 1))

	kurt_est = ((M * N * d + 1.) / (M -1.)) * ((M * S2 / np.square(S1)) -1)

	return kurt_est


def routine(data):
	N_sizes = np.arange(10)
	M_sizes = np.power(2, np.arange(7))

	for avgsize in N_sizes:
		for seqsteps in M_sizes:
