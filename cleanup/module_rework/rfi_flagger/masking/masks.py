import numpy as np
import matplotlib.pyplot as plt

# Offringa et al.
def ScaleInvariantRankOperator(Omega, eta = -0.05): # returns: Omega Prime
	'''
	Implementation of Offringa et al's defining routine behind AOFlagger, 
		reimplemented in python to function on H5 datasets for the 
		dynamic spectrum pipeline. Written with intent for cleaning
		observations to perform interplanetary scintillation analysis,
		but should be general enough to work on any datasets.

	Inputs:
		REMOVED --- N: The size of the input array
		Omega: The input flags array
		eta: An 'agressiveness' parameter for the function.

	Returns:
		OmegaPrime: the output flags array

	'''
	N = Omega.shape[0]
	idxRange = np.arange(N)

	psi = np.ones(N) * (eta - 1.)
	psi[Omega.astype(bool)] = eta

	M = np.concatenate([[0.], np.cumsum(psi)])

	P = cuargmin(M[:N])
	Q = np.flipud(N - cuargmax(np.flipud(M[1:])))

	OmegaPrime = np.zeros(N, dtype = int)
	OmegaPrime[(M[Q] - M[P]) >= 0.] = 1

	'''
	plt.plot(Omega)
	plt.plot(psi)
	plt.figure(2)
	plt.plot(M)
	plt.figure(3)
	plt.plot(P)
	plt.plot(Q)
	plt.plot(M[Q] - M[P])
	plt.figure(4)
	plt.plot(OmegaPrime)
	plt.show()
	'''

	return OmegaPrime.astype(int)


def cuargmax(row):
    __cuargmax = np.frompyfunc(
    	lambda prevVar, nextVar: 
    		nextVar if row[nextVar] > row[prevVar] else prevVar, 
    															2, 1)

    return __cuargmax.accumulate(
    			np.arange(0, len(row)), 0, dtype=np.object).astype(int)

def cuargmin(row):
    __cuargmin = np.frompyfunc(
    	lambda prevVar, nextVar:
    		nextVar if row[nextVar] < row[prevVar] else prevVar,
    															2, 1)

    return __cuargmin.accumulate(
    			np.arange(0, len(row)), 0, dtype=np.object).astype(int)