import numpy as np 
import matplotlib.pyplot as plt

def work(dataset, noiseThres, blownThres, blownDiscard, noiseDiscard):
	workCopy = np.ma.array(dataset.copy())

	thresholdMet = False
	flags = np.zeros(workCopy.shape[1])

	while not thresholdMet:
		newFlags = singlePass(workCopy, noiseThres, blownThres, blownDiscard, noiseDiscard)
		boolArr = newFlags != 0

		thresholdMet = not boolArr.any()
		print(thresholdMet)

		flags[newFlags != 0] = newFlags[newFlags != 0]

		blownThres *= 2
		noiseThres *= 2

	return flags

def singlePass(dataset, noiseThres, blownThres, blownDiscard, noiseDiscard):
	channelMed = np.median(dataset, axis = 0)
	datasetShape = dataset.shape
	maskedBool = channelMed.mask
	if maskedBool.any():
		maskLoc = np.argwhere(maskedBool)
		print("Replacing {0}".format(maskLoc))
		ranges = [np.clip(dataset.shape[1] -1, 0, np.arange(idx - 5, idx + 6)) for idx in maskLoc]
		channelMed[maskedBool] = [np.ma.median(channelMed[rangeVar]) for rangeVar in ranges]
		print(ranges, [np.ma.median(channelMed[rangeVar]) for rangeVar in ranges])
	channelGrad = abs(np.gradient(np.gradient(np.gradient(np.gradient(channelMed)))))

	'''
	plt.plot(channelMed)
	plt.gca().twinx().plot(channelGrad)
	plt.show()
	'''
	'''
	plt.imshow(dataset, aspect = "auto")
	plt.figure(2)
	plt.plot(channelGrad)
	plt.show()
	'''
	blownChan = ((channelGrad) > blownThres * np.percentile(channelGrad, 95)) # Account for negative slopes, roll as slope will be in the idx-1 slot
	blownChan += (channelGrad < -1. * blownThres * np.percentile(abs(channelGrad), 80))

	#plt.scatter(np.arange(blownChan.shape[0]), blownChan)
	blownChan += channelMed > blownThres * np.median(channelMed)

	if blownChan[-3]: # Slope sampling can only comare so far; if we are near the end and flagging, assume the remaining channels are flagged.
		blownChan[-3:] = True
	#plt.scatter(np.arange(blownChan.shape[0]), blownChan)
	blownChan[blownChan == np.ma.masked] = False

	channelNoise = np.std(dataset, axis = 0)
	noiseyChan = channelNoise > noiseThres * np.median(channelNoise)

	flags = np.zeros(dataset.shape[1])

	if blownDiscard:
		print("blown")
		dataset[:, blownChan] = np.ma.masked
		flags[blownChan > 0] = 1

	if noiseDiscard:
		print("noise)")
		dataset[:, noiseyChan] = np.ma.masked
		flags[noiseyChan > 0] = 2

	#plt.plot(flags)
	#plt.show()
	return flags
