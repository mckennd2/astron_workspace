import numpy as np 

from ..channel import classifier as chanclassifier
from ..temporal import classifier as tempclassifier

reload(chanclassifier)
reload(tempclassifier)


def main(dataset, optionsDict):
	if optionsDict['discardBlown'] or optionsDict['discardNoisey']:
		mask = channelClassifierHandler(dataset, optionsDict)

	if optionsDict['discardBlown']:
		mask2 = temporalClassifierHandler(dataset, optionsDict)
		mask[mask2 != 0] = 1

	return mask

def channelClassifierHandler(dataset, optionsDict):
	mask = np.zeros(dataset.shape)

	classified = chanclassifier.work(dataset, optionsDict['noiseThresholdChan'], optionsDict['blownThresholdChan'], optionsDict['discardBlown'], optionsDict['discardNoisey'])

	noiseyChan = np.squeeze(np.argwhere(classified == 2))
	blownChan  = np.squeeze(np.argwhere(classified == 1))

	if optionsDict['discardBlown']:
		blownChan = np.split(blownChan, np.where(np.diff(blownChan) != 1)[0] + 1)
		sizeBool = np.array([len(segment) > 3 for segment in blownChan])
		blownChan = np.concatenate([segment[1:-1] if sizeBool[idx] else segment for idx, segment in enumerate(blownChan)])
		

		mask[:, blownChan] = 2
		print(blownChan)

	if optionsDict['discardNoisey']:
		mask[:, noiseyChan] = 2.5

	return mask

def temporalClassifierHandler(dataset, optionsDict):
	mask = np.zeros(dataset.shape)

	classified = tempclassifier.work(dataset, optionsDict['blownThresholdTemp'], optionsDict['blownRampLenTemp'])

	mask[classified == True, :] = 1

	return mask