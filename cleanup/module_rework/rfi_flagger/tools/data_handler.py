"""A module to process a given dataset to attempt to flag and remove suspected RFI samples.
"""
import numpy as np
import h5py
import matplotlib.pyplot as plt

import threshold_routine as thresholding
import cleaning_routine as cleaning
from ..masking import masks

reload(thresholding)
reload(cleaning)
reload(masks)

def handle_input(dataset, optionsDict = {}):
	"""Head routine for processing a given dataset.
	
	Args:
	    fileName (string): h5 File location
	    datasetName (string): Name of the dataset inside the h5 file
	    optionsDict (dict, optional): Cleaning options, when left as none the default values (from self.defaultOptions()) will be used
	
	Returns:
	    np.ndarray: Mask array; different mask types have different value classifiers.
	    	0: Unflagged
	    	1: Temporal Blown Thresholded
	    	2: Channel Blown Thresholded (+0.5 Noisey)
	    	3: Temporal Cleaned Values
	    	4: Channel Cleaned Values
	    	5: SIR Expanded values
	"""
	if not optionsDict:
		optionsDict = defaultOptions()

	if optionsDict['solarObservation'] and option['pulsarObservation']:
		continueBool = bool((raw_input("Handling both solar observations and pulsars at the same time? This won't end well. Continue? y/[n]") == "y") or False)
		if not continueBool:
			print("Exiting")
			quit()
	dataset = np.ma.array(dataset)
	datasetClone = dataset.copy()
	dataShape = dataset.shape
	print(dataShape)
	channels = dataShape[0]

	mask = np.zeros(dataset.shape)
	mask = thresholding.main(dataset, optionsDict)
	dataset[mask != 0] = np.ma.masked

	thresholdMet = False
	idx = 2

	while not thresholdMet:
		newMask = cleaning.main(dataset, mask, optionsDict)
		mask[newMask != 0] += newMask[newMask != 0]
		dataset[mask != 0] = np.ma.masked
		if idx == 2:
			thresholdMet = True
		idx += 1
		'''
		maskCache = mask.copy()
		for idx in np.arange(dataset.shape[0]):
			masked = masks.ScaleInvariantRankOperator((mask[idx] != 0).astype(int)) * 5
			mask[idx, maskCache[idx] == 0] = masked[maskCache[idx] == 0]
			dataset[idx, masked != 0] = np.ma.masked
		plt.figure(3)
		plt.imshow(mask != maskCache, aspect = "auto")
		plt.figure(1)
		plt.imshow(np.log(datasetClone), aspect = "auto")
		plt.figure(2)
		plt.imshow(np.log(dataset), aspect = "auto")
		plt.show()
		'''

	return dataset.filled(0.)

def defaultOptions():
	"""Summary
	
	Returns:
	    TYPE: Description
	"""
	return dict([
			['outputSuffix', '_RFI_flagged'],

			['discardBlown', True,],
			['discardNoisey', False],

			['noiseThresholdChan', 250.],
			['blownThresholdChan', 15.],

			['blownThresholdTemp', 5.],
			['blownRampLenTemp', 5],

			['cleanSlopeStdFrac', 2.],

			['temporalCleanerDefaultThres', 1.5],
			['temporalCleanerInterpolate', False],
			
			['temporalKernel', np.array([
					[0,0,0],
					[-1, 2,-1],
					[0,0,0]
				])],
			['channelKernel', np.array([
					[],
					[],
					[]
				])],

			['solarObservation', False],
			['pulsarObservation', False]

			])
