import numpy as np 
import scipy.signal as spys
import scipy.interpolate as spyi
import scipy.stats as spyst

import matplotlib.pyplot as plt

from ..channel import cleaner as chanClean
from ..temporal import cleaner as tempClean

reload(chanClean)
reload(tempClean)

def main(dataset, currentMask, optionsDict):
	normalisedData = median_normalised(dataset, optionsDict)
	'''
	plt.imshow(normalisedData, aspect = "auto")
	plt.show()
	'''

	mask = tempClean.perform(normalisedData, optionsDict)
	#__, mask2 = chanclean.perform(temporalCleaned, optionsDict)

	currentMask += mask #+ mask2

	dataset[currentMask != 0] = np.ma.masked

	return currentMask

def median_normalised(dataset, optionsDict):
	normalisedData = np.ma.array(dataset.copy())
	medianVal = np.ma.median(dataset, axis = 0)

	slopes = np.convolve(medianVal, np.array([1, -1]), "same")
	slopes[0] = slopes[1]
	smoothSlope = spys.medfilt(medianVal, 7)

	replaceIdx = slopes > np.std(slopes) * optionsDict['cleanSlopeStdFrac']
	medianVal[replaceIdx] = smoothSlope[replaceIdx]

	chanShape = dataset.shape[1]
	boolArr = medianVal.mask
	if boolArr.any():
		boolArr = np.invert(boolArr)
		medianVal = spyi.interp1d(np.arange(dataset.shape[1])[boolArr], medianVal[boolArr], "cubic")(np.arange(dataset.shape[1])[boolArr])

		normalisedData[:, boolArr]  = normalisedData[:, boolArr] / medianVal
		normalisedData[:, np.invert(boolArr)] = 1.

	else:
		normalisedData /= medianVal
	plt.figure(2)
	plt.imshow(normalisedData, aspect = "auto", vmin = 0.95, vmax = 1.05)
	plt.title("Norm")
	plt.show()
	return normalisedData
