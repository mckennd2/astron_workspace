from __future__ import print_function
import numpy as np
import h5py
import os.path
import shutil
from sys import stdout
import data_handler as dh
import matplotlib.pyplot as plt
reload(dh)

def process_file(optionsDict):
	print("====      Performing Verification Steps    ====", end = '\r')
	stdout.flush()
	
	inputFileName  = optionsDict['inputFile']
	assert(os.path.isfile(inputFileName))

	outputFileName = optionsDict['outputFolder'] + optionsDict['inputFile'].split("/")[-1][:-3] + optionsDict["outputSuffix"] + ".h5"

	assert(os.path.isdir(optionsDict['outputFolder']))
	assert(not os.path.isfile(outputFileName))

	if optionsDict['cloneDatastructure']:
		print("====           Cloning Data File           ====", end = '\r')
		stdout.flush()
		shutil.copyfile(inputFileName, outputFileName)
	else:
		with h5py.File(outputFileName, 'a') as outputRef:
			outputRef.close()


	print("====    Begining Processing    ====")
	with h5py.File(inputFileName, 'r') as inputRef:
		with h5py.File(outputFileName, 'r+') as outputRef:
			processList = optionsDict['processList']
			
			if not processList:
				print(inputRef.items())
				processList = [datasetname[0] + "/DATA" for datasetname in inputRef.items() if "DYNSPEC" in datasetname[0]]
			
			allDatasets = [datasetname[0] + "/DATA" for datasetname in inputRef.items() if "DYNSPEC" in datasetname[0]]

			print(processList, allDatasets)
			for dName in allDatasets:
				if not dName in processList:
					del outputRef[dName]
				else:
					outputRef.require_dataset(dName, shape = inputRef[dName].shape, dtype = type(inputRef[dName][0,0,0]))

			segmentSize = optionsDict['segmentSize']
			cacheSize = optionsDict['cacheSize'] * segmentSize
			for dName in processList:
				inputShape = inputRef[dName].shape
				datasetPasses = int(inputShape[0] / cacheSize) + 1
				print("Starting " + dName, datasetPasses)
				for passIdx in np.arange(datasetPasses):
					remainingSamples = inputShape[0] - cacheSize * passIdx
					timeSamples = min(cacheSize, remainingSamples)
					
					cache = np.zeros([timeSamples, inputShape[1], inputShape[2]])

					if timeSamples != cacheSize:
						cacheIters = int(remainingSamples / segmentSize + 1)
					else:
						cacheIters = optionsDict['cacheSize']

					knownOffset = passIdx * cacheSize
					for cacheIdx in np.arange(cacheIters):
						print("====    Begining Pass {0} of {1} with cache of size {2}    ====".format(passIdx * optionsDict['cacheSize'] + cacheIdx, dName,timeSamples))
						cacheStartIdx = cacheIdx * segmentSize
						cacheEndIdx = (cacheIdx + 1) * (segmentSize)

						dataStartIdx = knownOffset + cacheStartIdx
						dataEndIdx = knownOffset + cacheEndIdx

						print(cacheStartIdx, cacheEndIdx, dataStartIdx, dataEndIdx)

						inputData = inputRef[dName][dataStartIdx: dataEndIdx, :, :]

						for polarisation in np.arange(inputShape[2]):
							cache[cacheStartIdx:cacheEndIdx, :, polarisation] = dh.handle_input(inputData[:, :, polarisation], optionsDict['cleaningDict'])
					plt.imshow(cache[:, :, 0], aspect = "auto")
					plt.figure(2)
					plt.imshow(inputRef[dName][knownOffset:dataEndIdx, :, 0], aspect = "auto")
					plt.show()
					print("====    Writing cache to disk    ====", end ='\r')
					stdout.flush()
					outputRef[dName][knownOffset: dataEndIdx, :, :] = cache
	return outputFileName





def defaultOptions():
	return dict([
			['inputFile', "./Dynspec.h5"],
			['processList', None], # ['processList', ["DYNSPEC_000/DATA", "DYNSPEC_001/DATA"],
			['outputFolder', "./"],
			['outputSuffix', "_rfi_processed"],

			['segmentSize', 20000],
			['segmentOverlap', 0.1],
			['segmentStart', 0],
			['segmentEnd', None],
			['cacheSize', 6], # n of segments

			['cloneDatastructure', True],

			['cleaningDict', dh.defaultOptions()]
		])
