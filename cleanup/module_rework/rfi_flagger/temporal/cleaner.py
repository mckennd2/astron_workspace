import numpy as np 
import scipy.signal as spys
import scipy.interpolate as spyi

import matplotlib.pyplot as plt

def perform(dataset, optionsDict):
	dataclone = dataset.copy()
	maskCache2 = SumThreshold(dataset.T, 1, 6., optionsDict)
#	cleanedDataset = np.multiply(dataset, (1 - maskCache2.T))
#	cleanedDataset[maskCache2.T] = np.ma.masked


	#mask = edgeDetection(dataset, optionsDict)
	maskCache = SumThreshold(dataset, 1, 6., optionsDict)

#	cleanedDataset = np.multiply(dataset, (1 - maskCache))
#	cleanedDataset[maskCache] = np.ma.masked

	


	#cleanedDataset /= np.ma.median(cleanedDataset, axis = 0)
	#mask = SumThreshold(cleanedDataset, 1, 6., optionsDict)
	#cleanedDataset = np.multiply(cleanedDataset, (1 - mask))
	#maskCache += mask
	#cleanedDataset[mask] = np.ma.masked
	#mask = SumThreshold(cleanedDataset, 1, 6., optionsDict)
	#cleanedDataset = np.multiply(cleanedDataset, (1 - mask))
	#maskCache += mask
	#cleanedDataset[mask] = np.ma.masked
	#mask = SumThreshold(cleanedDataset, 2, 6., optionsDict)
	#cleanedDataset = np.multiply(cleanedDataset, (1 - mask))
	#maskCache += mask
	#cleanedDataset[mask] = np.ma.masked
	#cleanedDataset = np.multiply(cleanedDataset, (1. - mask))

	#mask2 = edgeDetection(cleanedDataset, optionsDict)

	#cleanedDataset = np.multiply(cleanedDataset, (1. - mask2))

	'''
	plt.imshow(cleanedDataset, aspect = "auto", vmax = 0.9)
	plt.show()
	'''

#	maskCache2 = np.zeros([1])

	mask = np.clip(1,0, maskCache + maskCache2.T)
	cleanedDataset = dataset.copy()

	cleanedDataset[maskCache] = np.ma.masked
	'''
	plt.imshow(cleanedDataset, aspect = "auto")
	plt.figure(2)
	plt.imshow(dataclone, aspect = "auto", vmax = 1.2, vmin = 0.8)
	plt.show()
	'''
	return mask

def edgeDetection(datasetV, optionsDict):
	dataset = datasetV.copy()
	print(dataset[0])
	print(np.ma.sum(dataset == np.ma.masked))
	dataset[dataset == np.ma.masked] = 1.
	localKernel = optionsDict['temporalKernel'].T
	shape0 = int(localKernel.shape[0] / 2.)
	shape1 = int(localKernel.shape[1] / 2.)


	if optionsDict['temporalCleanerInterpolate']: ### TODO: FIX, using broken array referencing.
		interpolatorStart = spyi.interp1d(np.arange(shape0 + 1), dataset[:shape0 + 1, :], "cubic")
		interpolatorEnd = spyi.interp1d(np.arange(shape0 + 1), dataset[:-shape0:-1, :], "cubic")

		paddedDataset = np.concatenate([interpolatorStart(np.arange(-shape0, 0)), dataset, interpolatorEnd(np.arange(-shape0, 0))])

		interpolatorStart = spyi.interp1d(np.arange(shape1 + 1), paddedDataset[:, :shape1 + 1], "cubic")
		interpolatorEnd = spyi.interp1d(np.arange(shape1 + 1), paddedDataset[:, :-shape1:-1], "cubic")

		paddedDataset = np.concatenate([interpolatorStart(np.arange(-shape1, 0), paddedDataset, interpolatorEnd(np.arange(-shape1, 0)))], axis = 1)

	else:
		paddedDataset = np.concatenate([dataset[shape0 + 1:1:-1, :], dataset, dataset[-1:-shape0 - 1:-1, :]])
		return paddedDataset
		print(paddedDataset[:, shape1 + 2:1:-1].shape, paddedDataset.shape, paddedDataset[:, -1:-shape1 - 2:-1].shape)
		paddedDataset = np.concatenate([paddedDataset[:, shape1 + 1:1:-1], paddedDataset, paddedDataset[:, -1:-shape1 - 1:-1]], axis = 1)

	print(paddedDataset.shape, localKernel.shape)
	convolvedDatabase = spys.convolve2d(paddedDataset, localKernel, "valid")

	mask = convolvedDatabase > optionsDict['temporalCleanerDefaultThres']

	'''
	plt.imshow(convolvedDatabase, aspect = "auto", vmin = -1, vmax = 1)
	plt.figure(2)
	plt.imshow(mask, aspect = "auto")
	plt.show()
	'''

	return mask

def SumThreshold(maskedArray, M, threshold, optionsDict, stDev = [], plot = False):
	'''
	Implementation of Offringa et al.'s SumThreshold method for RFI
		identification, reimplemented in python with intent to
		function oh H5 datasets.

	Inputs:
		inputRow: Current sample to test
		currentMask: Currently flagged data points
		M: length of the current sequence window
		chi: Who the fuck knows.

	Return:
		newMask: An updated version of currentMask
	'''
	localKernel = optionsDict['temporalKernel']
	cache = maskedArray.copy()
	maskedArray[maskedArray.mask] = 0.
	shape0 = int(localKernel.shape[0] / 2.)
	shape1 = int(localKernel.shape[1] / 2.)

	#pltc = maskedArray.copy()
	#maskedArray = np.ma.concatenate([maskedArray, np.ones(M) * np.mean(maskedArray[-M * 2 + 1:])])	
	'''
	maskRoll = rolledArr(currentMaskPad)
	dataRoll = rolledArr(dataPad)
	segmentRoll = np.vectorize(multiRoller, otypes = [object])
	
	
	kernels = np.vstack(segmentRoll(maskRoll, np.arange(M)))[:, :-1 * M]
	segments = np.vstack(segmentRoll(dataRoll, np.arange(M)))[:, :-1 * M]

	products = np.sum((kernels * segments), axis = 0)
	counter = np.sum(kernels, axis = 0)
	'''
	paddedDataset = np.concatenate([maskedArray[shape0 + 1:1:-1, :], maskedArray, maskedArray[-1:-shape0 - 1:-1, :]])
	paddedDataset = np.concatenate([paddedDataset[:, shape1 + 1:1:-1], paddedDataset, paddedDataset[:, -1:-shape1 - 1:-1]], axis = 1)

	products = spys.convolve2d(paddedDataset, localKernel, "valid")

	counter = spys.convolve2d(1 - (paddedDataset == 0), localKernel> 0, "valid")
	
	counter[counter == 0.] = np.ma.masked
	rollingMean = np.ma.divide(products, counter)

	medianProd = np.ma.median(products)
	stDev = customStd(products, medianProd)
	
	rollingMean[maskedArray.mask] = 1.
	flagIdx = (rollingMean > (threshold * stDev + medianProd)) 
	#flagIdx += (rollingMean < (medianProd - stDev * threshold))
	
	flagIdx[maskedArray.mask] = False
	if plot:
		#plt.figure(3)
		#plt.imshow(pltc, aspect = "auto")
		plt.figure(4)
		plt.imshow(products, aspect = "auto")
		plt.figure(6)
		plt.imshow(rollingMean, aspect = "auto", vmax = 1, vmin = -1)	
		plt.figure(1)
		plt.imshow(flagIdx, aspect = "auto")
	
	#for offset in np.arange(1,M):
	#	flagIdx += rollPad(flagIdx, offset)
	
	maskedArray[flagIdx] = 1.
	maskedArray[flagIdx] = np.ma.masked

	if plot:
		plt.figure(2)
		plt.imshow(np.log(maskedArray), aspect = "auto")
		plt.figure(3)
		plt.imshow(np.log(cache), aspect = "auto")
		plt.show()
	
	return maskedArray.mask

def rollPad(array, shift):
	returnVar = np.roll(array, -1 * shift)
	returnVar[-1 * np.arange(shift - 1) -1] = False

	return returnVar

def customStd(values, appMean):
	return np.sqrt(np.ma.median(np.square(values - appMean), axis = 0))

def channelAnalyser(dataset, optionsDict):
	return
