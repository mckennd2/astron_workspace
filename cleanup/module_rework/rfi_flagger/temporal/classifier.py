import numpy as np 
import scipy.ndimage as spyi

def work(dataset, blownThresh, rampMinLength):
	medianSample = np.median(dataset, axis = 1)
	refMed = np.median(medianSample)

	blownTime = (medianSample > blownThresh * np.percentile(medianSample, 33)).astype(int)
	
	if rampMinLength:
		checkList = np.where(blownTime[1:] - blownTime[:-1])[0] +1
		if blownTime[0]:
			checkList = np.concatenate([[0], checkList])

		checkList = checkList.reshape(-1, 2)
		cleanList = np.where((checkList[:, 1] - checkList[:, 0]) >= rampMinLength)[0]

		print(cleanList)
		if cleanList:
			slices = np.concatenate([np.arange(element[0], element[1]) if (not abs(blownTime[element[0] + 1] - blownTime[element[0]]) > 0.1 * refMed) else "WUT" for element in checkList[cleanList]])
			blownTime[slices] = False

	return blownTime.astype(int)