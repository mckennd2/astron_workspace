from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

data = None

if rank == 0:
	data = np.arange(25)
	newData = np.zeros(25)
	if len(data) > size:
		nIter = int((len(data) - len(data) % size) / size + 1)
		for iter in range(nIter):
			recbuffer = np.zeros(size)
			print(data[iter * size: (iter+1) * size])
			recbuffer = comm.Scatter(data[iter * size: (iter+1) * size], recbuffer, root = 0)
			newData[iter * size: (iter + 1) * size] = recbuffer

		print(newData)
else:
	recv = None
	data = comm.scatter(data, root = 0)
	data += 1
	comm.Send(data, dest = 0)
