import numpy as np
import scipy.signal as spys
import time
import matplotlib.pyplot as plt

def b1(size):
	timeStart = time.clock()
	kernel = np.ones(5)
	data = np.ones(size** 2).reshape([size,size])

	array = []
	for row in data:
		array.append(np.convolve(row, kernel, "same"))

	#print(array)

	return time.clock() - timeStart

def b2(size):
	timeStart = time.clock()
	kernel = np.ones(5)
	data = np.ones(size ** 2).reshape([size,size])

	array = []
	for row in data:
		array.append(spys.convolve(row,kernel, "same"))

	#print(array)

	return time.clock() - timeStart

def b3(size):
	timeStart = time.clock()
	kernel = np.asarray([np.ones(5), np.zeros(5)])
	data = np.ones(size ** 2).reshape([size,size])

	array = (spys.convolve(data,kernel, "same"))

	#print(array)

	return time.clock() - timeStart

def run(n, size):
	b1a = []
	b2a = []
	b3a = []
	for i in range(n):
		b1a.append(b1(size))
		b2a.append(b2(size))
		b3a.append(b3(size))

	b1a = np.sum(b1a)
	b1a /= n

	b2a = np.sum(b2a)
	b2a /= n

	b3a = np.sum(b3a)
	b3a /= n

	print([b1a, b2a, b3a])
	return [b1a, b2a, b3a]

logs = []
samples = [10, 25, 50, 100, 300, 500, 1000, 3000, 5000]#, 10000]
for size in samples:
	if size < 700:
		logs.append(run(10, size))
	else:
		logs.append(run(2, size))
logs = np.asarray(logs)

print(logs)
print(samples, logs[:,0])
plt.loglog(samples, logs[:, 0], label = "Numpy")
plt.loglog(samples, logs[:, 1], label = "Scipy for loop")
plt.loglog(samples, logs[:, 2], label = "Scipy 2D")
plt.legend()
plt.show()