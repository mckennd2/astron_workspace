import numpy as np 

import scipy.signal as spys

def rollingAverage(dataSet, n):
	####
	# For a given numpy array, convert each value into a rolling average of the n nearby values on
	# 	each side of the data point. Constructed to be used for large data sets with lossy information 
	#	at the start and end.
	####

	# Setup a 0 array to store the average values
	newSet = np.zeros(dataSet.shape)

	# Iterate over the given dataset (skipping the first n+1 and last n+1 pieces of data)
	
	# This could be improved by providing the local values instead of a 0, but np.copy() is 
	# 	significantly slower than getting a zero'd array.
	for i in range(len(dataSet) - n -1)[n+1:]:
		# Store the local value (faster than np.copy())
		newSet[i] = dataSet[i]

		# Add the n nearby values on each side of the current point
		for i1 in range(n)[1:]:
			newSet[i] += dataSet[i + i1]
			newSet[i] += dataSet[i - i1]

	# Determine the average of the values
	newSet /= (2 * n + 1)

	# Return the averaged data set.
	return newSet

def rollingAverageConv(dataSet, n):

	kernel = np.ones(int(n) * np.ones(len(dataSet.shape), dtype = int))
	kernel /= np.sum(kernel, axis = None) # Normalise the kernel

	print(kernel)
	return spys.convolve(dataSet, kernel,  'same', 'auto')