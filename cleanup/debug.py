import h5py
import numpy as np
import modules.rfi_cleaner.new_flagger as aof 
import matplotlib.pyplot as plt

fileRef = h5py.File("../Dynspec_rebinned_L656422_SAP000.h5", 'r')
# 114, 250, 350

#extremae = fileRef["DYNSPEC_000/DATA"][60000:80000, 250, 0] > np.median(fileRef["DYNSPEC_000/DATA"][60000:80000, 250, 0]) * 1.1
#plotVar = fileRef["DYNSPEC_000/DATA"][60000:80000, 350, 0] / np.median(fileRef["DYNSPEC_000/DATA"][60000:80000, 250, 0])
#print(np.std(plotVar))
#plt.hist(plotVar)
#plt.show()

dataChunk = fileRef["DYNSPEC_000/DATA"][60000:80000, :, 0]

mask = aof.routine(dataChunk.T)

np.save("mask-mask.npy", mask.mask)
np.save("mask-array.npy", np.asarray(mask))
