from __future__ import print_function # Only Python 2.x
import subprocess
import sys
from sys import stdout
from subprocess import PIPE
import shlex
import time
import datetime

import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation

from astropy.io import fits

from os import remove, listdir
import re

plt.rcParams['animation.ffmpeg_path'] = u'/home/mckenna/.imageio/ffmpeg/ffmpeg-linux64-v3.3.1'
FFMpegWriter = manimation.writers['ffmpeg']

from argparse import ArgumentParser

parser = ArgumentParser("Process a MS observation through WSClean then create a film with \"create_fits_cube.py\".")
parser.add_argument('-i', help="Input MS Folder", default = "./", dest = 'input_ms')
parser.add_argument('-o_fits', help = "Default WSCLEAN .fits prefix", default = "./wsclean", dest = 'wsc_prefix')
parser.add_argument('-o_cube', help = "Default Image Cube name", default = "./output_movie.fits", dest = 'cube_name')
parser.add_argument('-od_cube', help = "Cube output directory", default = "./", dest = "cube_od")
parser.add_argument('-res', help = "WSClean scale parameter", default = "40as", dest = 'res')
parser.add_argument('-im_size', type = int, help = "Size of the output image in pixels", default = 100, dest = 'im_res')
parser.add_argument('-uvmax', help = "WSClean maxuv-l parameter", default = "300", dest = 'uvmax')
parser.add_argument('-wsc_param', help = "Other parameters to pass to WSClean command", default = "-j 24 -mem 30 -no-update-model-required -weight briggs 0 -pol I -data-column DATA -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -make-psf -multiscale", dest = 'wsc_param')
parser.add_argument('-int_start', type = int, help = "WSClean starting interval", default = 0, dest = 'int_start')
parser.add_argument('-int_end', type = int, help = "WSClean end interval", default = 100, dest = 'int_end')
parser.add_argument('-int_out', type = int, help = "WSClean output interval count", default = 100, dest = 'int_out')
parser.add_argument('-sl', help = "Cubes script location", default = "./", dest = 'cubesLoc')
parser.add_argument('-film', type = bool, help = "Convert image cube to a mp4 movie via matplotlib? (REQUIRES FFMPEG)", default = True, dest = 'film')
parser.add_argument('-sigma', type = float, help = "Standard deviations above the median value used to set the minimum floor for the output movie", default = 7., dest = 'cube_std')
parser.add_argument('-wsc_extras_cleanup', type = bool, help = "Remove non-image WSClean generated .FITS (psf, dirty, etc.) after video creation?", default = True, dest = 'wsc_clean_lim')
parser.add_argument('-wsc_all_cleanup', type = bool, help = "Remove all WSClean generated .FITS files after video creation?", default = False, dest = 'wsc_clean_all')
parser.add_argument('-cube_cleanup', type = bool, help = "Remove the image cube .FITS after creation of the video?", default = False, dest = 'cube_cleanup')

def main(argv):
	args = parser.parse_args(argv)

	wsclean_cmd = "wsclean -maxuv-l {0} -scale {1} -interval {2} {3} -intervals-out {4} -name {5} {6} -size {7} {7} {8}".format( args.uvmax, args.res, args.int_start, args.int_end, args.int_out, args.wsc_prefix, args.wsc_param, args.im_res, args.input_ms)
	cubesCommand = "python2 {0}create_fits_cube.py -i {1}-t*-image.fits -d {2} -o {3}".format(args.cubesLoc, args.wsc_prefix, args.cube_od, args.cube_name)

	#wsclean = shlex.split(wsclean_cmd)
	#cubes = shlex.split(cubesCommand)
	wsclean = wsclean_cmd
	cubes = cubesCommand
	print("Begining WSClean command: {0}".format(wsclean_cmd))
	wsproc = execute(wsclean)
	
	handle(wsproc, "=== IMAGING TABLE ===", int(args.int_out))
	
	while wsproc.poll() == None:
		continue
	print("WSClean exit by signal {0}".format(wsproc.poll()))

	print("Begining Image Cube Command: {0}".format(cubesCommand))
	icproc = execute(cubes)
	handle(icproc, "Processing File", int(args.int_out))
	
	while icproc.poll() == None:
		continue
	print("Image Cube exit by signal {0}".format(icproc.poll()))
	if args.film:
		print("Converting Image Cube to Movie: {0}.mp4, {1} frames taking minimum of {2} std above the median value.".format(args.cube_name, args.int_out, args.cube_std))
		makeVideo(args.cube_od + args.cube_name, args.cube_std, args.cube_od + args.cube_name[:-5])

	if args.wsc_clean_lim or args.wsc_clean_all or args.cube_cleanup:
		print("Begining Cleanup proces...")

		if args.wsc_clean_lim or arg.wsc_clean_all:
			imagingDir = args.wsc_prefix[:(len(args.wsc_prefix.split("/")[:-1]))]
			print(imagingDir)
			if imagingDir == "":
				imagingDir = "./"
			filesList = listdir(imagingDir)
			if args.wsc_clean_all:
				removeList = fileFlagger(imagingDir, "{0}-t.*\.fits".format(args.wsc_prefix))

				print("Removing all WSClean generated .FITS files: {0}".format(removeList))

				[remove(file) for file in removeList] 
			else:
				removeList = fileFlagger(imagingDir, "{0}-t.*(?<!image)\.fits".format(args.wsc_prefix))
				
				print("Removing extra WSClean generated .FITS files: {0}".format(removeList))

				[remove(file) for file in removeList]

		if args.cube_cleanup:
			removeLoc = args.cube_od + args.cube_name
			print("Removing image cube .FITS file at {0}".format(removeLoc))

			remove(removeLoc)

		print("Cleanup Finished")
	print("Exiting")

def fileFlagger(dir, regexCheck):
	check = re.compile(regexCheck)
	filesInDir = listdir(dir)
	list = filter(check.match, filesInDir)
	removeList = map((lambda name: dir + name), list)

	return removeList 

def execute(args):
	proc = subprocess.Popen(args, stdout=PIPE, shell = True, bufsize = 1)
	return proc

def handle(proc, trigger, totalOp):
	with proc.stdout:
		count = -1
		startTime = time.time()
		currentPixelIdx = 0
		for line in iter(proc.stdout.readline, ''):
			if trigger in line:
				count += 1
				progress = count / float(totalOp)
				width = int(subprocess.check_output(['stty', 'size']).split()[1])
				currentPixelIdx = handleBar(startTime, progress, width, currentPixelIdx)
	proc.wait()

def timeRemaining(progress, timeElapsed):
	if progress < 0.01:
		return "--:--:--"

	estRemain = int(timeElapsed / progress - timeElapsed)
	timeString = str(datetime.timedelta(seconds=estRemain))

	return timeString

global wipPixels
wipPixels = ["-", "\\", "|", "/", "-", "\\", "|", "/"]

def progressbar(length, progress, currentPixelIdx):
	length -= 5
	progressPixels = int(length * progress)
	remainingPixels = length - progressPixels

	progressPixels  *= "="
	remainingPixels *= "."
	if currentPixelIdx == 8:
		currentPixelIdx = 0
	workingPixel = wipPixels[currentPixelIdx]

	construction = " [{0}{1}{2}] ".format(progressPixels, workingPixel, remainingPixels)

	return construction, currentPixelIdx + 1

def handleBar(startTime, progress, columns, currentPixelIdx):
	timeRemainingVar = timeRemaining(progress, time.time() - startTime)
	progressStr = int(len("{:.2%}".format(progress)))
	progressBarVar, currentPixelIdx = progressbar(columns - len(timeRemainingVar) -progressStr, progress, currentPixelIdx)

	print("{0}{1}{2:.2%}".format(timeRemainingVar, progressBarVar, progress), end = "\r")

	return currentPixelIdx

def makeVideo(imageCubeLoc, sigma, outputName, fpsV = 12.5, dpi = 240, title = "Scintillation", author = "ASTRON", comment = "Scintillating."):
	imageCube = fits.open(imageCubeLoc)[0].data
	metadataV = dict(title = title, artist = author, comment = comment)
	writer = FFMpegWriter(fps = fpsV, metadata = metadataV)

	fig = plt.figure()
	
	dataMed = np.median(imageCube)
	dataStd = np.std(imageCube)
	dataMax = np.max(imageCube)
	frames = imageCube[0].shape[0]
	currentPixelIdx = 0
	startTime = time.time()
	with writer.saving(fig, outputName + ".mp4", dpi):
		for idx, frame in enumerate(imageCube[0]):
			plt.title("{0}, frame {1}".format(title, idx))
			plt.imshow(frame, interpolation = 'nearest',  vmax = dataMax, vmin = dataMed + sigma * dataStd, cmap = 'gist_gray')
			fig.tight_layout()

			plt.colorbar()
			writer.grab_frame()
			plt.clf()

			width = int(subprocess.check_output(['stty', 'size']).split()[1])
			currentPixelIdx = handleBar(startTime, (idx + 1.) / frames, width, currentPixelIdx)

if __name__ == '__main__':
        main(sys.argv[1:])

