#!/bin/python

import module_rework.imaging.processing.imaging_automation as ia 

step_size = 200
size = 472200
for i in range(int(size / step_size)):
	stepSize = step_size
	endStep = (i + 1) * stepSize
	startStep = i * stepSize

	print("Starting Observation 3C147 segment {0} to {1} (of {2})".format(startStep, endStep, size))
	locs = ia.main(['-target', '3C147', '-int_out', str(stepSize), '-sigma', '3', '-int_end', str(endStep), '-int_start', str(startStep), '-uvmax', '350', '-res', '30arcsec', '-wsc_param', '-j 24 -mem 50 -no-update-model-required -weight briggs 0 -pol I -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -multiscale', '-i', '../imaging/3C147-28thjune/observation_avg.MS', '-col', 'CORRECTED_DATA'])

