"""Generate the scintillation indicies based on the processed data
"""
import numpy as np
import h5py
import astropy.time
from astropy.coordinates import get_sun, SkyCoord
from astropy.wcs import WCS
from ..tools import generic_tools as tools

import matplotlib.pyplot as plt

def mapper(dataArr, unpackArr, mergeSources = True, lambdaMeters = 1.9, plotVar = False):
	"""Combines all the processed data
	
	Args:
	    dataArr (TimeSeries): Raw dataset
	    unpackArr (List(np.ndarray)): Results from generate_arrays.py
	    mergeSources (bool, optional): Whether or not to merge scintillating pixels
	    lambdaMeters (float, optional): Length of the sampled photon in meters
	    plotVar (bool, optional): Plot results / debug information or not
	
	Returns:
	    TODO: TODO
	"""
	print("Unpacking Data")
	deltaS, sourceError = unpackArr[0:2]
	stdVal = unpackArr[-1]

	print("Dividing things")
	dataMean = np.mean(dataArr, axis = (0,1))

	if mergeSources:
		dSSummed, boolArray, sources = tools.source_merger(deltaS)
		errSummed = np.sqrt(tools.sum_elements(boolArray, sourceError))
		limIdx = np.max(sources)
		print(limIdx)

		if limIdx == 0:
			return np.array([np.nan]), np.array([np.nan]), 1
		
		dMean = tools.sum_elements(boolArray, dataMean)
		print(dMean, dSSummed, errSummed)
		indexMapResults = np.divide(dSSummed, dMean)

		stdValMod = stdVal * boolArray
		dataMeanMod = dataMean * boolArray

		print(errSummed, dSSummed, stdValMod[boolArray], dataMeanMod[boolArray])
		errSummed = np.divide(np.sqrt(np.sum(np.square(np.divide(errSummed, dSSummed)) + np.sum(np.square(np.divide(stdValMod[boolArray], dataMeanMod[boolArray]))))), np.sum(boolArray))
		indexMapErr = np.multiply(errSummed, indexMapResults)

		sourcePixels = np.round([np.mean(np.argwhere(sources == currVal), axis = 0) for currVal in range(1, limIdx +1)]).astype(int) # Why does an int rounding function not return ints and not allow for the dtype keyword?
		print(type(sourcePixels[0]))
		if type(sourcePixels[0]) == np.int64:
			sourcePixels = [sourcePixels]
		print(sourcePixels)
		print("Hit", indexMapResults.shape, indexMapErr.shape, len(sourcePixels), limIdx)
	else:
		sources = deltaS != 0.
		sourcePixels = np.argwhere(sources)
		print(dataArr.shape)
		indexMapResults = np.divide(deltaS[sources], dataMean[sources])
		indexMapErr = np.multiply(np.sqrt(np.square(np.divide(sourceError[sources], deltaS[sources])) + np.square(np.divide(np.mean(stdVal[1 - sources]), dataMean[sources]))), indexMapResults)	
#		indexMapErr = np.multiply((np.divide(sourceError[sources], deltaS[sources])), indexMapResults)

	fileLoc = dataArr.sourceFile
	with h5py.File(fileLoc) as refFile:
		if dataArr.baseVersion < 0.1:
			print(dataArr.groupName)
			headerDict = dict(refFile[dataArr.groupName + "/raw_headers/0"].attrs.items())
		else:
			headerDict = dict(refFile[dataArr.groupName + "/raw_headers/" + str(dataArr.splitSegments[2])].attrs.items())
		# Work around dimensionality bug VVVV
		if "CDELT3" in headerDict:
			headerDict["CDELT3"] = 1. 
		wVar = WCS(headerDict)
		obsTime = astropy.time.Time(headerDict["DATE-OBS"])
		featureCoords = [(SkyCoord.from_pixel(sourcePixel[-2], sourcePixel[-1], wVar)) for sourcePixel in sourcePixels]
		solarCoord = get_sun(obsTime)
		
		elongations = [coord.position_angle(solarCoord) for coord in featureCoords]

	elongations = [float(angle.to_string()[:-3]) for angle in elongations]
	
	pVar = abs(np.sin(np.array(elongations)))

	predictIndex = 0.06 * lambdaMeters * (np.power(pVar, -1.6))
	predictIndex = np.maximum([1.], predictIndex)
	print(predictIndex, indexMapResults, indexMapErr)

	if plotVar:
		plt.errorbar(pVar, indexMapResults, yerr = indexMapErr, fmt = 'ro')
		expline(-1.6, 0.06 * 1.9, 20)
		plt.xlabel("Solar Elongation (AU)")
		plt.ylabel("Scintillation Index")
		plt.gca().set_xscale('log')
		plt.gca().set_yscale('log')

		plt.figure(2)
		plt.errorbar(indexMapResults, predictIndex, xerr = indexMapErr, fmt = 'ro')
		line(1.,0.)
		plt.xlabel("Observed Scintillation Index")
		plt.ylabel("Predicted Scintillation Index (Solar Elongation)")

		plt.figure(3)
		plt.hist(indexMapResults)
		plt.xlabel("Scintillation Indicies")
		plt.ylabel("Counts")
		plt.show()

	return indexMapResults, indexMapErr, indexMapResults.size

def line(slope, intercept):
    """Plot a line from slope and intercept
    
    Args:
        slope (float): Slope of the line
        intercept (float): Intercept of the line
    """
    xVar = np.array(plt.gca().get_xlim())
    yVar = intercept + slope * xVar
    plt.plot(xVar, yVar, '')

def expline(power, constant, steps):
    """Plot a line that will be straight in log-log space with a given slope
    
    Args:
        power (float): Exponential slope
        constant (float): Exponential constant multiple
        steps (int): Samples count
    """
    xVar = np.linspace(start = plt.gca().get_xlim()[0], stop = plt.gca().get_xlim()[1],num = steps)
    yVar = np.power(xVar, power) * constant
    plt.plot(xVar, yVar, '--')

def errSum(arr):
	"""Error calculator for sums of values
	
	Args:
	    arr (np.ndarray): Values to sum/square
	
	Returns:
	    float: Error value
	"""
	return np.sqrt(np.sum(np.square(arr)))
