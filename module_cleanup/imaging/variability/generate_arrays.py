"""Process a raw observation of imaging data


Heavily based off the work of Morgan et al. MNRAS 473, 2965-2983.
"""
import numpy as np 
import scipy.stats as spystat
from ..tools import filters as filterFunc

import matplotlib.pyplot as plt

def generate_images(dataArr, n_ips = 1.5, stdMul = 7., filterSettings = [None]):
	"""Generate the arrays needed for generate_maps
	
	Args:
	    dataArr (TimeSeries): Input data set
	    n_ips (float, optional): The estimated time scale of scintillation (provided by Morgan et al.)
	    stdMul (float, optional): Requred std to flag as scintillating
	    filterSettings (list, optional): Filters for the Butterworth filter
	
	Returns:
	    List(np.ndarray): Output results
	"""
	dataArrRef = dataArr.copy()

	filterSettings = filterFunc.filter_settings(dataArrRef.timeStep, *filterSettings)
	nSamples = (dataArr.shape[0] * dataArr.shape[1])

	n_ips *= (1. / filterSettings[0]) # Convert from seconds to sample counts
	skewArr, kurtosisArr, filteredArr, __ = higher_order_moment_maps(dataArrRef, filterSettings)
	
	varIm = std(filteredArr, axisVar = (0,1))
	meanVal = np.mean(varIm)

	print(meanVal)

	flagMap, stdVal = flag_values(varIm, meanVal, stdMul)
	deltaS = scintillating_flux_density_map(varIm, flagMap, meanVal)
	errSys, errScint = error_maps(varIm, nSamples, meanVal, stdVal, deltaS, n_ips)

	return deltaS, errScint, errSys, flagMap, skewArr, kurtosisArr, filteredArr, varIm





def std(dataArr, axisVar = None):	
	"""Custom standard deviation (used to be more interesting)
	
	Args:
	    dataArr (TimeSeries): Input dataset
	    axisVar (int, optional): Axis to use for calculation
	
	Returns:
	    np.ndarray: Standard deviation
	"""
	#return np.sqrt(np.sum(np.square(dataArr), axis = axisRef) / np.prod(shapeArr[axisVar]))
	return np.std(dataArr, axis = axisVar)

def higher_order_moment_maps(dataArr, filterSettings):
	"""Apply filters and determine the mean, skew, kurtotis of the data
	
	Args:
	    dataArr (TimeSeries): Input dataset
	    filterSettings (List): Butterworth filter
	
	Returns:
	    List(np.ndarray): Arrays / values
	"""
	sampleFreq, pixelLoc, highF, lowF, order, axis = filterSettings
	meanVal = np.mean(dataArr)

	highPassVar = filterFunc.butterworth_filter(lowF, None, sampleFreq, "highpass", order)
	highPassArr = filterFunc.apply_filter(highPassVar, dataArr, 0)

	skewArr = spystat.skew(highPassArr, axis = 0)
	kurtosisArr = spystat.kurtosis(highPassArr, axis = 0)

	lowPassVar = filterFunc.butterworth_filter(None, highF, sampleFreq, "lowpass", order)
	filteredArr = filterFunc.apply_filter(lowPassVar, highPassArr)

	return skewArr, kurtosisArr, filteredArr, meanVal

def flag_values(dataArr, meanVal, stdMul = 5.):
	"""Determine the scintillating pixels
	
	Args:
	    dataArr (TimeSeries): Processed data input
	    meanVal (float): Mean value for the observation
	    stdMul (float, optional): Standard deviation to flag scintillating pixels
	
	Returns:
	    List(np.ndarray): Flags array, standard deviation of the data
	"""
	stdVal = std(dataArr)
	flags = ( dataArr - meanVal ) > ( stdMul * stdVal )
	
	return flags, stdVal

def scintillating_flux_density_map(dataArr, flags, meanVal):
	"""Deterine the scintillating flux intensities
	
	Args:
	    dataArr (TimeSeries): Input dataset
	    flags (np.ndarray): Flagged pixels to analyse
	    meanVal (np.ndarray): Reference mean value
	
	Returns:
	    np.ndarray: Scintillating flux density
	"""
	deltaS = np.zeros(dataArr.shape)
	deltaS[flags] = np.sqrt(np.square(dataArr[flags]) - np.square(meanVal))
	
	
	return deltaS

def error_maps(dataArr, nSamples, meanVal, stdVal, deltaS_scint, n_ips = 1.5):
	"""Determine the errors in our calcuations
	
	Args:
	    dataArr (TimeSeries): Input data set
	    nSamples (float): Sample count 
	    meanVal (np.ndarray): Mean value of the flux
	    stdVal (np.ndarray): Standard deviation of the flux
	    deltaS_scint (np.ndarray): scintillating flux density
	    n_ips (float, optional): Time scale of the scintillation
	
	Returns:
	    TYPE: Description
	"""
	errSys = np.sqrt(abs(np.square(dataArr + stdVal) - np.square(meanVal))) - deltaS_scint
	errScint = np.sqrt(np.square(errSys) + np.divide(np.square(deltaS_scint), (nSamples / n_ips)))

	return errSys, errScint
