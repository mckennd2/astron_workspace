"""Automated setup for the imaging of radio MS observations with WSClean.

Can be run through CLI or as a module import as import.main(['-flag', 'value', ...]), through a manual process if no flags are provided, or have a dictionary returned if called as import.main([], getVals = True)

Attributes:
    parser (ArgumentParser): Description

Deleted Attributes:
    wipPixels (list): Description
"""
from __future__ import print_function # Only Python 2.x

from matplotlib import use as pltUse
pltUse("Agg")

from sys import stdout # print \r only works if this is specifically imported? What?
import sys
import subprocess
import time
import re
import h5py
import os.path
import md5
import casacore.tables
import numpy as np

import astropy.time

from os import remove, listdir
from argparse import ArgumentParser

import matplotlib.animation as manimation
import matplotlib.pyplot as plt


from ..tools import generic_tools as toolSet
from ..tools import h5_data_writer as h5Writer

reload(toolSet)
reload(h5Writer)

####TODO: Refine outputFileLoc = args.sky_target + obsStartTime +  ".h5"
parser = ArgumentParser("Process a MS observation through WSClean then create a film with \"create_fits_cube.py\".")
parser.add_argument('-i', help="Input MS Folder", default = "./", dest = 'input_ms')
parser.add_argument('-outputs_prefix', help = "Default WSCLEAN .fits prefix", default = "./wsclean", dest = 'wsc_prefix')
parser.add_argument('-od_cube', help = "Cube output directory", default = "./", dest = "cube_od")
parser.add_argument('-res', help = "WSClean scale parameter", default = "40as", dest = 'res')
parser.add_argument('-im_size', type = int, help = "Size of the output image in pixels", default = 100, dest = 'im_res')
parser.add_argument('-uvmax', help = "WSClean maxuv-l parameter", default = "300", dest = 'uvmax')
parser.add_argument('-wsc_param', help = "Other parameters to pass to WSClean command", default = "-j 24 -mem 30 -no-update-model-required -weight briggs 0 -pol I -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -make-psf -multiscale", dest = 'wsc_param')
parser.add_argument('-int_start', type = int, help = "WSClean starting interval", default = 0, dest = 'int_start')
parser.add_argument('-int_end', type = int, help = "WSClean end interval", default = 100, dest = 'int_end')
parser.add_argument('-int_out', type = float, help = "WSClean output interval count (fraction of segments)", default = 1., dest = 'int_out')
parser.add_argument('-segment_size', type = int, help = "Size of jobs supplied to WSClean in timesteps.", default = None, dest = 'segSize')
parser.add_argument('-film', type = bool, help = "Convert image cube to a mp4 movie via matplotlib? (REQUIRES FFMPEG)", default = False, dest = 'film')
parser.add_argument('-sigma', type = float, help = "Standard deviations above the median value used to set the minimum floor for the output movie", default = 7., dest = 'cube_std')
parser.add_argument('-wsc_extras_cleanup', type = bool, help = "Remove non-image WSClean generated .FITS (psf, dirty, etc.) after video creation?", default = True, dest = 'wsc_clean_lim')
parser.add_argument('-wsc_all_cleanup', type = bool, help = "Remove all WSClean generated .FITS files after video creation?", default = True, dest = 'wsc_clean_all')
parser.add_argument('-ffmpeg_loc', type = str, help = "Location of your FFMPeg executable if not accessible in your path and you wish to make a movie", default = u'/home/mckenna/.imageio/ffmpeg/ffmpeg-linux64-v3.3.1', dest = 'ffmpegLoc')
parser.add_argument('-ts', type = float, help = "Time step between image files", default = 0.083886, dest = 'time_step')
parser.add_argument('-target', type = str, help = "Name of target (will check if output file exists for target)", default = "undefined_target", dest = 'sky_target')
parser.add_argument('-col', type = str, help = "Column to read from in MS set when imaging", default = "DATA", dest = 'col')
def main(argv, getVals = False):
	"""A monster used for everything. To be abstracted.
	
	Args:
	    argv (List): Input list of flags / values. CLI or module-like (through stringed flags/values) supported.
	    getVals (bool, optional): if set, return the dictionary of flags and default parameters.
	
	Returns:
	    string: Output file location
	    string: Dataset name within output file.
	
	Raises:
	    Exception_err: Raised if the ffmpegWriter is not defined for film creation.
	    RuntimeError: 
	"""
	args = parser.parse_args(argv)

	if getVals:
		return args.__dict__
	
	if not argv:
		print("You didnt pass any arguments? Here are my default values.")
		print(args.__dict__)
		inputVar = raw_input("Manually set arguments? y/[n]") or 'n'
		if inputVar.lower() == 'y':
			for key in args.__dict__:
				value = args.__dict__[key]
				args.__dict__[key] = type(value)(raw_input("{0}: {1}, {2}".format(key, value, type(value))) or value)
		else: 
			return None

	if args.film:
		try:
			ffmpegWriter = manimation.writers['ffmpeg']
		except (RuntimeError, KeyError):
			try:
				print("FFMPeg not found in path, trying -ffmpeg_loc flag, {0}".format(args.ffmpegLoc))
				plt.rcParams['animation.ffmpeg_path'] = args.ffmpegLoc
				ffmpegWriter = manimation.writers['ffmpeg']
			except Exception as Exception_err:
				raise Exception_err


	cleanPrefix = args.wsc_prefix

	casatable = casacore.tables.table(args.input_ms)
	time_step = casacore.tables.tablecolumn(casatable, "INTERVAL").getcol(0, 1)[0]
	startTime, endTime = casacore.tables.tablecolumn(casatable, "OBSERVATION").TIME_RANGE.getcol() / (60. * 60. * 24.) # Why does nobody except LOFAR use Modified Julian Date in seconds???
	
	#casatableDataLength = casatable.DATA.getdminfo()['SPEC']['CubeShape'][2] / 
	obsStartTime = astropy.time.Time(startTime, format = 'mjd')
	obsStartTime.format = 'isot'
	obsStartTime = str(obsStartTime)

	obsEndTime = astropy.time.Time(endTime, format = 'mjd')
	obsEndTime.format = 'isot'
	obsEndTime = str(obsEndTime)
	print(time_step, 'end')


	imagingDir = args.wsc_prefix[:(len("/".join(args.wsc_prefix.split("/")[:-1]))) + 1]

	if imagingDir == '':
		imagingDir = "./"
		filePrefix = args.wsc_prefix
	else:
		filePrefix = args.wsc_prefix.split("/")[-1]

	currFolder = toolSet.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))
	offsetNum = 1
	idxVar = None
	while currFolder:
		print("We suspect there may be imaging files with the same prefix in your current path; we are adding a '-{0}' suffix to your given name.".format(offsetNum))
		args.wsc_prefix = args.wsc_prefix[:idxVar] + '-' + str(offsetNum)
		filePrefix = filePrefix[:idxVar] + '-' + str(offsetNum)
		idxVar = (len(str(offsetNum)) + 1) * -1
		offsetNum += 1
		currFolder = toolSet.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))

	if offsetNum > 10:
		print("\033[93m\033[1mHow lazy have you gotten? Delete your imaging files already.\033[0m")

	initHashReference = "wsclean -maxuv-l {0} -scale {1} -interval {2} {3} -intervals-out {4} -name {5} {6} -size {7} {7} -data-column {8} {9}".format( args.uvmax, args.res, args.int_start, args.int_end, args.int_out, cleanPrefix, args.wsc_param, args.im_res, args.col, args.input_ms)
	headCmdHash = md5.new(initHashReference).hexdigest()[:7]

	segmentSize = args.segSize
	segments = (args.int_end - args.int_start) / segmentSize + 1

	for segmentIdx in range(segments):
		startIdx = args.int_start + segmentIdx * segmentSize
		endIdx = args.int_start + (segmentIdx + 1) * segmentSize

		print(startIdx, endIdx)
		wscleanCmd = "wsclean -maxuv-l {0} -scale {1} -interval {2} {3} -intervals-out {4} -name {5} {6} -size {7} {7} -data-column {8} {9}".format( args.uvmax, args.res, startIdx, endIdx, int(args.int_out * segmentSize), args.wsc_prefix, args.wsc_param, args.im_res, args.col, args.input_ms)

		print("Testing WSClean command...")
		skipClean = False
		if os.path.isfile(args.sky_target + ".h5"):
			with h5py.File(args.sky_target + ".h5") as testFile:
				culledWSCC = initHashReference.replace(' ', '')
				for key, val in testFile["command_reference"].attrs.items():
					if culledWSCC in val.replace(' ', ''):
						skipClean = True
						hashVal = key
				if skipClean:
					for key in testFile["observations"].keys():
						if hashVal in key:
							datasetLoc = "observations/{0}/image_data".format(key)

		wsclean = wscleanCmd

		print(args.wsc_prefix, imagingDir)
		if imagingDir == '':
			imagingDir = "./"
			filePrefix = args.wsc_prefix
		else:
			filePrefix = args.wsc_prefix.split("/")[-1]


		if not skipClean:
			print("Begining WSClean command: {0}".format(wscleanCmd))
			wsproc = toolSet.execute(wsclean)
	
			toolSet.handle(wsproc, "=== IMAGING TABLE ===", int(args.int_out * segmentSize))
	
			while wsproc.poll() is None:
				continue

			exitCode = wsproc.poll()

			print("WSClean exit by signal {0}".format(exitCode))
			if exitCode == 127:
				raise RuntimeError("You didn't load WSClean.")

			if exitCode == 255:
				raise RuntimeError("There was an issue with one of your passed flags.")

			print(filePrefix)
			cubeFiles = toolSet.fileFlagger(imagingDir, "{0}-t.*image\.fits".format(filePrefix))
			cubeFiles.sort(key=lambda f: int(filter(str.isdigit, f)))

			if segmentIdx == 0:
				attrsKeys = ['segmentSize', 'dataCount', 'timeStep', 'init_time', 'end_time']
				attrsVals = [segmentSize, args.int_end - args.int_start, time_step, obsStartTime, obsEndTime]
				attrs = dict(zip(attrsKeys, attrsVals))
				datasetLoc = h5Writer.initialiseObservation(args.sky_target, cubeFiles[0], headCmdHash, attrs)

			h5Writer.processChunk(args.sky_target + obsStartTime +  ".h5", cubeFiles, datasetLoc, wsclean, [startIdx, endIdx])
			
			currFolder = toolSet.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))
			idxVar = None
			while currFolder and not args.wsc_clean_all:
				args.wsc_prefix = args.wsc_prefix[:idxVar] + '-' + str(offsetNum)
				filePrefix = filePrefix[:idxVar] + '-' + str(offsetNum)
				idxVar = (len(str(offsetNum)) + 1) * -1
				offsetNum += 1
				currFolder = toolSet.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))
				print("As you chose to keep some of the fits files, we are adding a '-{0}' suffix to your given name for the {1} segment.".format(offsetNum, segmentIdx))


		else:
			print("Command detected in command history, attempting to skip cleaning process. If an error occurs, remove the command reference and processed data sets from your h5 file.")



	if args.film:
		outputName = args.cube_od + args.sky_target + "-" + datasetLoc.split("/")[1]
		print("Converting Image Cube to Movie: {0}.mp4, {1} frames taking minimum of {2} std above the median value.".format(outputName, args.int_out, args.cube_std))
		makeVideo(ffmpegWriter, args.cube_od + args.sky_target + ".h5", datasetLoc, args.cube_std, outputName, fpsV = 1. / time_step, startIdx = args.int_start, title = "{0}, $\sigma_{{min}}={1}$, {2}, {3}".format(args.sky_target, args.cube_std, args.res, "-".join(datasetLoc.split("-")[1:]).split("/")[-2]))

	if args.wsc_clean_lim or args.wsc_clean_all or args.cube_cleanup:
		print("Begining Cleanup proces...")

		if args.wsc_clean_lim or args.wsc_clean_all:
			print(imagingDir)
			if imagingDir == "":
				imagingDir = "./"
			if args.wsc_clean_all:
				removeList = toolSet.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))

				print("Removing all WSClean generated .FITS files: {0}".format(removeList))

				__ = [remove(fileVar) for fileVar in removeList] 
			else:
				removeList = toolSet.fileFlagger(imagingDir, "{0}-t.*(?<!image)\.fits".format(filePrefix))
				
				print("Removing extra WSClean generated .FITS files: {0}".format(removeList))

				__ = [remove(fileVar) for fileVar in removeList]

		print("Cleanup Finished")
	print("Exiting")
	return outputFileLoc, datasetLoc

def makeVideo(ffmpegWriter, imageCubeLoc, datasetName, sigma, outputName, spectralChannel = 0, fpsV = 12.5, dpi = 240, title = "Scintillation", author = "ASTRON", comment = "Scintillating.", startIdx = 0):
	"""Generate a video based on the data within a h5 dataset.
	
	Args:
	    ffmpegWriter (FFMPeGWriter): Pre-generated matplotlib ffmpeg output writer.
	    imageCubeLoc (string): h5 File location
	    datasetName (string): h5 Dataset location in file
	    sigma (float): Threshold for noise floor when imaging.
	    outputName (string): Output video name
	    spectralChannel (int, optional): Spectral channel in dataset to analyse.
	    fpsV (float, optional): Output framerate (if below 5fps this will be modified to allow for playback, most video players cannot handle extremely low framerates)
	    dpi (int, optional): DPI of the output video.
	    title (str, optional): Plot title
	    author (str, optional): Video 'autor' for metadata
	    comment (str, optional): Video 'comment' for metadata
	    startIdx (int, optional): Timestep to begin rendering video at.
	"""
	with h5py.File(imageCubeLoc, 'r') as refFile:
		imageCube = refFile[datasetName]
		metadataV = dict(title = title, artist = author, comment = comment)
		writer = ffmpegWriter(fps = fpsV, metadata = metadataV)
		fig = plt.figure()
		
		dataMed = np.median(imageCube)
		dataStd = np.std(imageCube)
		dataMax = np.max(imageCube)

		frames = imageCube[0].shape[0]
		currentPixelIdx = 0
		startTime = time.time()

		with writer.saving(fig, outputName + ".mp4", dpi):
			for idx, frame in enumerate(imageCube[spectralChannel, :]):
				plt.title("{0}\nframe {1}".format(title, idx + startIdx))
				if idx == 0:
					imVar = plt.imshow(frame, interpolation = 'nearest',  vmax = dataMax, vmin = dataMed + sigma * dataStd, cmap = 'gist_gray')
					plt.colorbar()
				else:
					imVar.set_data(frame)
				writer.grab_frame()
			
				width = int(subprocess.check_output(['stty', 'size']).split()[1])
				currentPixelIdx = toolSet.handleBar(startTime, float(idx) / float(frames), width, currentPixelIdx)


# If called via CLI, pass along args.
if __name__ == '__main__':
		main(sys.argv[1:])
