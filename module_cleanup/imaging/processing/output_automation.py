"""Automated analysis of scintillation indicies for imaging data.

TODO: Rework, after TimeSeries issues are resolved.

Attributes:
    TimeSeries (TimeSeries): Object references -- weird bug fix for custom np.ndarray
"""
import numpy as np

from ..direct import power_analysis as powerFunc
from ..variability import generate_arrays as mapData
from ..variability import generate_maps as mapMaker
#from ..tools.timeSeries import timeSeries
from ..tools import timeSeries
from ..tools import generic_tools as tools

import matplotlib.pyplot as plt

reload(powerFunc)
reload(mapData)
reload(mapMaker)
reload(timeSeries)
reload(tools)

TimeSeries = timeSeries.TimeSeries

def process_image_cube_power(fileLoc, groupName, timeStep = 0.083886, segmentSize = 5000, segmentOverlap = 0.2):
	"""Process an image cube via the power spectrum method
	
	Args:
	    fileLoc (string): File location
	    groupName (string): Dataset name within file
	    timeStep (float, optional): Time step between steps in image cube for analysis
	    TODO: return the parameter pixelLoc (list, optional): Pixel to sample
	    segmentSize (int, optional): Segmentation size per step TODO: integrate in timeSeries rework.
	    segmentOverlap (float, optional): Overlap between segments TODO: integrate in timeSeries rework.
	
	Returns:
	    resultIdx (np.ndarray): Scintillation Indicies
	    sampledPixels (List): location of sampled pixels
	
	Raises:
	    RuntimeError: Stop predicting my fuckups.
	"""
	middleSample = TimeSeries(fileLoc, groupName, segment_info = [True, None, 10000, 20000])

	print(middleSample.shape)

	meanSamples = np.max(middleSample, axis = 1)[0, :, :]

	approxMax = max(np.percentile(meanSamples, 97), 0.33 * np.max(meanSamples))
	meanSampleMap = meanSamples > approxMax

	boolArr, arr = tools.source_merger(meanSampleMap, False)

	maxSources = np.max(arr)
	if maxSources != 1:
		raise RuntimeError("Unexpected amount of sources found (detected {0} blobs)".format(maxSources))


	resultIdx = np.array([])
	samplePixels = np.argwhere(meanSampleMap)
	print(samplePixels)

	for pixel in samplePixels:
		print(pixel)
		mainPixel = TimeSeries(fileLoc, groupName, pixel, segment_splitter = [segmentSize, segmentOverlap, None])[0]
		print(mainPixel.shape)

		pixelIdx = []
		for segment in mainPixel.segmentIter():
			segIdx, channelIdx, segmentData = segment

			scintIdx = powerFunc.powerIntegral(segmentData, [True, [False, 0, 5, 20], 20])	

			print(scintIdx)
			pixelIdx.append(scintIdx)
		print(len(pixelIdx))
		if resultIdx.shape == (0,):
			resultIdx = np.array(pixelIdx)
		else:
			resultIdx = np.vstack([resultIdx, pixelIdx])

	'''
	largeSeries = TimeSeries(fileLoc, groupName, segment_splitter = [segmentSize, segmentOverlap, None])

	largeSeries = largeSeries[:, :, samplePixels[:, 0], samplePixels[:, 1]]
	largeSeries = largeSeries.mean(axis = 2)[:, :, np.newaxis, np.newaxis]

	resultIdx = []
	for segment in largeSeries.segmentIter():
		segIdx, channelIdx, segmentData = segment

		scintIdx = powerFunc.powerIntegral(segmentData, [True, [False, 0, 5, 20], 20])	

		print(scintIdx)
		resultIdx.append(scintIdx)
	'''
	resultIdx = np.array(resultIdx)
	return resultIdx, samplePixels

def process_image_cube_variability(fileLoc, groupName,  mergeSources = True, segmentSize = 5000, segmentOverlap = 0.2):
	"""Determine scintillation index through the variability method.

	reference usage:
	returnarr = oa.process_image_cube_variability("./3C147-segment.h5", "observations/2018-06-22-merged/image_data")
	
	Args:
	    fileLoc (string): h5 File location
	    groupName (string): h5 dataset name
	    mergeSources (bool, optional): Whether or not to merge connected scintillating pixels into a single scintillation index
	    segmentSize (int, optional): Segment size TODO: integrate in timeSeries rework.
	    segmentOverlap (float, optional): Segment overlap TODO: integrate in timeSeries rework.
	
	Returns:
	    TYPE: Description
	"""
	print("Extracting Time series")
	cubeExtract = TimeSeries(fileLoc, groupName, segment_splitter = [segmentSize, segmentOverlap, None])

	print(cubeExtract.shape)

	resultIdx = np.array([[],[],[],[]])
	for segment in cubeExtract.segmentIter():
		segIdx, channelIdx, segmentData = segment
		print("Generating Data")
		unpackArr = mapData.generate_images(segmentData)
		print("Passing to Mapper")
		scintIdx, scintIdxErr, sources = mapMaker.mapper(segmentData, unpackArr, mergeSources = mergeSources)

		resultIdx = np.hstack([resultIdx, [scintIdx, scintIdxErr, np.ones(sources) * segIdx, np.ones(sources) * channelIdx]])
	
	return resultIdx
