"""TImeSeries object for handling imaging data

TODO: Rewrite from scratch
"""
import numpy as np
import h5py


class TimeSeries(np.ndarray):
	"""A object for handling data related to pixel time series
	
	Attributes:
	    baseVersion (float): Processed version
	    elongation (float): Solar enlongation (in AU)
	    filteredState (): ?????
	    groupName (TYPE): Name of observation in the data set
	    nPixels (int): Number of sampled pixels
	    pixels (list): Pixels sampled
	    segment (list): Segment of observation dataset
	    sourceFile (string): Observation h5 file location
	    sourceShape (array-like, np.ndarray): Shape of the observation dataset
	    splitSegments (List): Describe how self.segmentIter() splits the data
	    timeStep (float): Interval between time samples
	"""

	def __new__(cls, source_file, group_name, pixelLoc = [None, None], time_step = 0.083886, segment_info = [False, None, None, None], segment_splitter = [5000, 0.2, None]):
		"""Numpy sublass initialisation
		
		Args:
		    source_file (string): Source file location
		    group_name (string): Group name of the observation
		    pixelLoc (list, optional): Pixel locations
		    time_step (float, optional): Time step for older observations
		    segment_info (list, optional): Describe the location of the array to extract
		    segment_splitter (list, optional): Describe how to split the array for self.segmentIter()
		
		Returns:
		    TimeSeries: The TimeSeries Object
		"""
		with h5py.File(source_file, 'r') as readFile: # Indent everything or numpy will assume we gave up and initialise the array afterwards.

			if segment_info[0]:
				channel = segment_info[1]

				if not channel:
					channel = slice(0, readFile[group_name + "/image_data"].shape[0])

				startTime = segment_info[2]
				endTime = segment_info[3]
			else:
				channel = slice(0, readFile[group_name + "/image_data"].shape[0])
				startTime = 0
				endTime = readFile[group_name + "/image_data"].shape[1]

			if not segment_info[0] and (pixelLoc == np.asarray([None, None])).all():
				data = readFile.get(group_name + "/image_data")
			else:
				if (pixelLoc == np.asarray([None, None])).any():
					pixelLoc[0] = pixelLoc[0] or slice(0, readFile[group_name + "/image_data"].shape[-2]) # H5py Work around for broken design 
					pixelLoc[1] = pixelLoc[1] or slice(0, readFile[group_name + "/image_data"].shape[-1])
				print(type(pixelLoc))
				if not isinstance(pixelLoc, list) and not isinstance(pixelLoc, np.ndarray):
					print("Adapting {0} to list form.".format(pixelLoc))
					pixelLoc = pixelLoc.tolist()[0]

				print(channel, startTime, endTime, pixelLoc)
				data = readFile[group_name + "/image_data"][channel,startTime:endTime, pixelLoc[0], pixelLoc[1]]

			self = data.view(cls)
			self.sourceShape = readFile[group_name + "/image_data"].shape
			self.nPixels = 1
			self.filteredState = False
			self.segment = segment_info
			self.sourceFile = source_file
			self.groupName = group_name
			self.pixels = pixelLoc
			self.splitSegments = segment_splitter
			self.baseVersion = readFile.attrs['version']

			if self.baseVersion >= 0.1:
				self.timeStep = readFile[group_name].attrs['timeStep']
				self.elongation = readFile[group_name]['location_ref'][str(np.round(startTime, -3))].attrs["solar_elongation"]			
			else:
				self.timeStep = time_step
				self.elongation = readFile[group_name].attrs["solar_elongation"]
			readFile.close()
			return self

	def __array_finalize__(self, obj):
		"""Numpy subclass finalisation
		
		Args:
		    obj (object): Object with attributes of the TimeSeries init
		"""
		if obj is None: return
	
		self.timeStep = getattr(obj, 'timeStep', None)
		self.nPixels = getattr(obj, 'nPixels', None)
		self.filteredState = getattr(obj, 'filteredState', None)
		self.segment = getattr(obj, 'segment', None)
		self.sourceFile = getattr(obj, 'source_file', None)
		self.pixels = getattr(obj, 'pixels', None)
		self.sourceShape = getattr(obj, 'sourceShape', None)
		self.groupName = getattr(obj, 'groupName', None)
		self.elongation = getattr(obj, 'elongation', None)
		self.splitSegments = getattr(obj, 'splitSegments', None)
		self.baseVersion = getattr(obj, 'baseVersion', None)

		self.__dict__.update(getattr(obj, "__dict__", {}))


	def segmentIter(self):
		"""Iterate over chunks of the dataset
		
		Yields:
		    [index, channel, TimeSeries]: A culled segment of the time series with extra segment information.
		"""
		segSize, segOverlap, __ = self.splitSegments

		print(self.shape, 'pre')
		if len(self.shape) == 1:
			self = self[np.newaxis, :, np.newaxis, np.newaxis]
			print(self.shape, 'pass')

		segments = int(self.sourceShape[1] / segSize)

		startIdx = 0
		endIdx = int(segSize * (1. + segOverlap))
		self.splitSegments[2] = int(((startIdx + endIdx) / 2.))
		for idx in range(segments - 1):
			for channel in range(self.shape[0]):
				print(channel, startIdx, endIdx)
				yield [idx, channel, self[channel, startIdx:endIdx, ...][np.newaxis, ...]]

			startIdx = (idx + 1) * segSize - int(segSize * segOverlap / 2.)
			endIdx = (idx + 2) * segSize + int(segSize * segOverlap / 2.)
			self.splitSegments[2] = int(((startIdx + endIdx) / 2.))

		for channel in range(self.shape[0]):
			print(channel, startIdx, -1, self[channel, startIdx:-1, ...][np.newaxis, ...].shape)
			yield [idx + 1, channel, self[channel, startIdx:-1, ...][np.newaxis, ...]]

		self.splitSegments[2] = None