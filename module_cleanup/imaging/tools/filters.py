"""File dedicated to the creation and definition of analogue-style filters.
"""
import numpy as np 
import scipy.signal as spys


def apply_butterworth_filter(dataArr, axisVar = -1, lowF = .1, highF = 10., btypeVar = "bandpass", order = 2):
	"""Given a statset and parameters, apply a butterworth filter to the data.
	
	Args:
	    dataArr (np.ndarray): Input dataset (normally kept 1d)
	    axisVar (int, optional): Axis to apply the filter to.
	    lowF (float, optional): High pass filter frequency
	    highF (float, optional): Low pass filter frequency
	    btypeVar (str, optional): Type of filter to be applied ('highpass', 'lowpass', 'bandpass')
	    order (int, optional): Order of the filter
	
	Returns:
	    filteredData (np.ndarray): Dataset after being passed through the filter.
	"""
	sampleRate = 1. / dataArr.timeStep

	filterVar = butterworth_filter(lowF, highF, sampleRate, btypeVar, order)
	filteredData = apply_filter(filterVar, dataArr, axisVar)

	return filteredData



def apply_filter(varArr, dataArr, axis = -1):
	"""Apply a generated analog filter
	
	Args:
	    varArr (List): Analog filter parameters
	    dataArr (np.ndarray): Data to be filtered
	    axis (int, optional): Axis to apply the filter along
	
	Returns:
	    np.ndarray: Filtered dataset
	"""
	return spys.lfilter(varArr[0], varArr[1], dataArr, axis)



def butterworth_filter(lowF, highF, sampleF, btypeVar = "bandpass", order = 2):
	"""Filter parameters generator
	
	Args:
	    lowF (float): High pass filter frequency
	    highF (float): Low pass filter frequency
	    sampleF (float): Sampling frequency
	    btypeVar (str, optional): Filter type ('highpass', 'lowpass', 'bandpass')
	    order (int, optional): Filter order
	
	Returns:
	    List: Filter parameters
	"""
	freqRange = __filterPrep(lowF, highF, sampleF, btypeVar)

	filterNumer, filterDenom = spys.butter(order, freqRange, btype = btypeVar)

	return [filterNumer, filterDenom]

def __filterPrep(lowF, highF, sampleF, btypeVar):
	"""Filter helper
	
	Args:
	    lowF (float): High pass filter frequency
	    highF (float): Low pass filter frequency
	    sampleF (float): Sampling frequency
	    btypeVar (str, optional): Filter type ('highpass', 'lowpass', 'bandpass')
	
	Returns:
	    freqRange (array-like): Frequencies as a multiple of the Nyquist frequency
	"""
	nyquist = sampleF / 2.

	highF = np.min([highF, nyquist]) # Filter limit 

	if btypeVar == "highpass":
		freqRange = lowF / nyquist
	elif btypeVar == "lowpass":
		freqRange = highF / nyquist
	elif btypeVar == "bandpass":
		freqRange = np.asarray([lowF, highF]) / nyquist

	else:
		print("#!#!#! Unknown filter, defaulting to bandpass")
		freqRange = np.asarray([lowF, highF]) / nyquist

	return freqRange





def filter_settings(sampleFreq, pixelLoc = None, highF = 10., lowF = 0.1, order = 2, axis = -1):
	"""Summary
	
	Args:
	    sampleFreq (TYPE): Description
	    pixelLoc (None, optional): Description
	    highF (float, optional): Description
	    lowF (float, optional): Description
	    order (int, optional): Description
	    axis (TYPE, optional): Description
	
	Returns:
	    TYPE: Description
	"""
	return [1. / sampleFreq, pixelLoc, highF, lowF, order, axis]
