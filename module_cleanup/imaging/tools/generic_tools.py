"""Generic tools useful for imagin gobservations

Attributes:
    estTime (culled List): Loading bar estimated time remaining, length limited array
    wipPixels (list): List of pixel symbols for the 'currenly cycling' pixel

"""
from __future__ import print_function
from scipy.ndimage import label as ndLabel
from os import listdir
from subprocess import PIPE
import re
import time
import datetime
from collections import deque

import numpy as np
import subprocess

import matplotlib.pyplot as plt

def source_merger(dataArr, sumElements = True):
	"""Given a boolean dataset, find grouped sources
	
	Args:
	    dataArr (np.ndarray (bool-like)): Input dataset, any non-0 value will be merged
	    sumElements (bool, optional): Whether or not to sum the contributions of dataset elements
	
	Returns:
		summedElements (np.ndarray, OPTIONAL): Sum of merged sources
	    boolArray (np.ndarray): List of sources and the labels
	    array (np.ndarray): Labels applied to orignal dataset
	"""
	array, sources = ndLabel(dataArr, np.ones(9, dtype = int).reshape(3,3))
	boolArray = label_identifier(array, sources)

	if sumElements:
		summedElements = sum_elements(boolArray, dataArr)

		return summedElements, boolArray, array

	return boolArray, array


def label_identifier(dataLabelArr, sources):
	"""Generate an n-dimensional array of each source
	
	Args:
	    dataLabelArr (np.ndarray): Array of labelled sources
	    sources (int): Output source count
	
	Returns:
	    boolArray (np.ndarray): An (n, m, sources)-dimensional array containing each of the sources on it's own axis.
	"""
	lim = np.max(sources)
	indicies = np.arange(1, lim + 1)
	
	boolArray = np.equal(indicies, dataLabelArr.reshape(dataLabelArr.shape[0], dataLabelArr.shape[1], 1))
	boolArray = np.transpose(boolArray, axes = (2, 0, 1))

	return boolArray

def sum_elements(boolArray, dataArr):
	"""Sum up all the elements in a given grouping.
	
	Args:
	    boolArray (np.ndarray): Output of label_identifier
	    dataArr (np.ndarray): Input dataset
	
	Returns:
	   np.ndarray: Summed elements as defined by the boolean-logic of boolArray
	"""
	nulledArray = boolArray * dataArr
	print(nulledArray.shape, boolArray.shape, dataArr.shape)
	return np.sum(nulledArray, axis = (1,2))





def fileFlagger(dirVar, regexCheck):
	"""Find files that match a regex check
	
	Args:
	    dirVar (string): Folder to search
	    regexCheck (string): Uncompiled Regex
	
	Returns:
	    removeList (List): Flagged Files
	"""
	print(regexCheck)
	check = re.compile(regexCheck)
	filesInDir = listdir(dirVar)
	listVar = filter(check.match, filesInDir)
	removeList = [dirVar + name for name in listVar]
	return removeList 

def execute(args):
	"""Execute a shell command as a subprocess, pipe out output, pass on the reference
	
	Args:
	    args (string): Command arguments
	
	Returns:
	    subprocess: Subprocess reference
	"""
	proc = subprocess.Popen(args, stdout=PIPE, shell = True, bufsize = 1)
	return proc

def handle(proc, trigger, totalOp):
	"""Handler for progress bar, listens for progress updates
	
	Args:
	    proc (subprocess (?): Subprocess to list to the output of.
	    trigger (string): String to update count on
	    totalOp (int): Expected operations
	"""
	with proc.stdout:
		count = -1
		startTime = time.time()
		currentPixelIdx = 0
		for line in iter(proc.stdout.readline, ''):
			if trigger in line:
				count += 1
				progress = count / float(totalOp)
				width = int(subprocess.check_output(['stty', 'size']).split()[1])
				currentPixelIdx = handleBar(startTime, progress, width, currentPixelIdx)
	proc.wait()

def timeRemaining(progress, timeElapsed):
	"""Remaining time estimate, will update the estTime deque (length limited) List
	
	Args:
	    progress (float): Current progress (0-1)
	    timeElapsed (Time): Current progress
	
	Returns:
	    string: Time remaining (human-readable)
	"""
	if progress < 0.01:
		return "--:--:--"

	estRemain = int(timeElapsed / progress - timeElapsed)
	estTime.append(estRemain)

	estRemain = int(np.mean(estTime))

	timeString = str(datetime.timedelta(seconds=estRemain))

	return timeString

global wipPixels
wipPixels = ["-", "\\", "|", "/", "-", "\\", "|", "/"]


global estTime
estTime = deque(maxlen = 10)

def progressbar(length, progress, currentPixelIdx):
	"""Construct the progress bar
	
	Args:
	    length (int): Width of the bar
	    progress (float): Current progress (0-1)
	    currentPixelIdx (int): Current 'working' pixel index
	
	Returns:
	    List: Progress bar string
	    int: currentPixelIdx + 1
	"""
	length -= 5
	progressPixels = int(length * progress)
	remainingPixels = length - progressPixels

	progressPixels  *= "="
	remainingPixels *= "."
	if currentPixelIdx == len(wipPixels):
		currentPixelIdx = 0
	workingPixel = wipPixels[currentPixelIdx]

	construction = " [{0}{1}{2}] ".format(progressPixels, workingPixel, remainingPixels)

	return construction, currentPixelIdx + 1

def handleBar(startTime, progress, columns = None, currentPixelIdx = None):
	"""Progress bar creator
	
	Args:
	    startTime (Time): Initial time to reference for remaining time
	    progress (float): Current progress (0-1)
	    columns (int): Width of the console in symbols
	    currentPixelIdx (int): Current 'working' pixel index
	
	Returns:
	    currentPixelIdx (int): currentPixelIdx + 1
	"""

	if columns == None:
		columns = int(subprocess.check_output(['stty', 'size']).split()[1])
		

	if currentPixelIdx == None:
		currentPixelIdx = 0
	timeRemainingVar = timeRemaining(progress, time.time() - startTime)
	progressStr = int(len("{:.2%}".format(progress)))
	progressBarVar, currentPixelIdx = progressbar(columns - len(timeRemainingVar) -progressStr, progress, currentPixelIdx)

	print("{0}{1}{2:.2%}".format(timeRemainingVar, progressBarVar, progress), end = "\r")

	return currentPixelIdx
