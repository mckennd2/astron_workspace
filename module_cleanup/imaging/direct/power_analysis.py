"""Heavy WIP modification of ../../dynspec/scintillation_index/index_calculator.py, to be merged.
"""
import numpy as np
import scipy.signal as spys
from ..tools.timeSeries import TimeSeries
import matplotlib.pyplot as plt

import scipy.optimize as spyo

def determine_index(mainPixel, numOffset = 16):
    	"""Taking the power series of a source pixel, determine the scintillation index
        	using the power spectrum, subtracting the given off source pixel time series
    	
    	Args:
        	mainPixel (TYPE): Description
	        offPixel (None, optional): Description
    	
    	Returns:
        	TYPE: Description
    
   	"""
	print(mainPixel.timeStep)
	freqOutput, fftOutput = spys.welch(np.mean(mainPixel, axis = 0), fs = 1. / mainPixel.timeStep,
									window = ("kaiser", 8.4), nperseg = 512,
									return_onesided = True)


	offPixel = np.random.randint(0, min(mainPixel.sourceShape[-2:]), (numOffset, 1, 2))

	fftOutputOff = np.zeros(fftOutput.shape)
	offPixelSeries = np.zeros(mainPixel.shape)

	for pixelLoc in offPixel:
		if (mainPixel in pixelLoc):
			pixelLoc += 1
		offTimeSeries = TimeSeries(mainPixel.sourceFile, mainPixel.groupName, pixelLoc = pixelLoc, time_step = mainPixel.timeStep)

		tempFreq, fftOutputOffTemp = spys.welch(np.mean(offTimeSeries, axis = 0), fs = 1. / offTimeSeries.timeStep,
									window = ("kaiser", 8.4), nperseg = 512,
									return_onesided = True)

		print(mainPixel.shape, fftOutput.shape, offTimeSeries.shape, fftOutputOffTemp.shape)

		offPixelSeries += offTimeSeries
		fftOutputOff += fftOutputOffTemp


		assert((tempFreq == freqOutput).all())

	fftOutputOff /= numOffset
	offPixelSeries /= numOffset

	plt.loglog(fftOutput)
	plt.figure(2)
	plt.plot(mainPixel)
	plt.show()
	#TODO: Determine cutoffIfx programatically
	cutoffIdx = 450
	integral = np.trapz(fftOutput[:cutoffIdx] - fftOutputOff[:cutoffIdx], freqOutput[:cutoffIdx])
	correction = np.square(np.mean(mainPixel - offPixelSeries))

	print(integral, correction)
	return np.sqrt(integral / correction)


def powerIntegral(beam, optionsArr = None):
	#print(beam.shape)

	if not optionsArr:
		optionsArr = defaultOptions()

	plotVar, resampleOpt, __ = optionsArr

#	if resampleOpt[0]:
#		beam.timeStep = float(resampleOpt[2]) * beam.timeStep
#	print("Timestep: {0}".format(beam.timeStep))

	print(beam.shape, 'hii')
	compressedBeam = beam[0, :, 0, 0].copy()
	print(compressedBeam.shape, 'hi')
	print(beam.timeStep, 'tsdb')
#	compressedBeam = beam.compress(35, 0, squeeze = True, resample = resampleOpt)

	freqOutput, fftOutput = spys.welch(compressedBeam, fs = 1. / beam.timeStep, window = ("kaiser", 8.6), nperseg = 512, return_onesided=True)

	startIdx, endIdx, midSlope = sigmoidFit(np.log10(freqOutput), np.log10(fftOutput))

	correction = np.square(np.mean(compressedBeam))
	netPower = fftOutput[startIdx:endIdx]

	integral = np.trapz(netPower, freqOutput[startIdx:endIdx])

	if plotVar:
		plt.figure(1)
		plt.plot(compressedBeam)
		plt.figure(3)
		plt.loglog(freqOutput, fftOutput)
		plt.figure(4)
		plt.loglog(fftOutput)
		plt.show()


	return np.sqrt(integral / correction)

def defaultOptions():
	return [False, [True, 0, 5, 20], 30.]


def calculate_offsources(beam, offsource):
	return

def invSigmoid(datafloor, a, c):
	x, floor = datafloor
	return np.maximum(np.divide(1., 1. + np.exp(a * (x - c))), floor)

def invInvSigmoid(y, a, c):
	return np.divide(np.log(np.divide(1., y) - 1.), a) + c

def invSigmoidSlope(x, a, c):
	exponential = np.exp(a * (x - c))
	return -1. * np.divide(a * exponential, np.square(exponential + 1))

def sigmoidFit(freq, dataset):
	print(dataset.shape, freq.shape)
	dataset = dataset[1:]
	freq = freq[1:] # Remove f = 0 to remove infinite element

	print(np.argwhere(np.isnan(dataset)), np.argwhere(np.isinf(dataset)))
	print(np.argwhere(np.isnan(freq)), np.argwhere(np.isinf(freq)))

	earlyMax = np.mean(dataset[:5])
	lateMean = np.mean(dataset[-1 * int(len(dataset) * 0.2):])

	normData = (dataset - lateMean) / (earlyMax - lateMean)

	lateMean = np.percentile(normData[-1 * int(len(normData) * 0.2)], 95)
	normData = np.maximum(normData, lateMean)
	print(np.argwhere(np.isnan(normData)), np.argwhere(np.isinf(normData)))

	param = spyo.curve_fit(invSigmoid, [freq, lateMean], normData, maxfev = 100000)

	print(param[0])

	lateMean = np.percentile(normData[-1 * int(len(normData) * 0.2)], 95)

	sigmoidUpper = 0.97
	sigmoidLower = max(max(invSigmoid([freq[-1], lateMean], param[0][0], param[0][1]), lateMean), 0.03)
	
	startVal = invInvSigmoid(sigmoidUpper, param[0][0], param[0][1])
	endVal = invInvSigmoid(sigmoidLower, param[0][0], param[0][1])

	#plt.figure(3)
	#plt.plot(dataset)
	#plt.figure(4)
	#plt.plot(freq, normData)
	#plt.plot(freq, invSigmoid([freq, lateMean], param[0][0], param[0][1]))
	#plt.show()

	idxes = np.argwhere(np.logical_and(freq > startVal, freq < endVal))
	print(startVal, endVal)

	if idxes.size != 0:
		startIdx = idxes[0][0]
		endIdx = idxes[-1][0]
	else:
		startIdx = 0
		endIdx = np.argwhere(normData == lateMean)[0][0] + 100
	print(startIdx, endIdx)
	midSlope = param[0][1] # f(c) = 0.5 for a sigmoid
	slope = invSigmoidSlope(midSlope, param[0][0], param[0][1])
	
	lateMeanPreFix = np.mean(dataset[freq > 0.6])
	debug = np.power(10., lateMeanPreFix)
	return startIdx + 1, endIdx + 1, [midSlope, param[0][0], param[0][1], debug]
