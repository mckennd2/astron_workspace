"""The file dedicated to the different types of beams and spectra we will handle when inspecting
		inteplanetary scintillations. 

Attributes:
    def_segment_info (list): Description

Deleted Attributes:
    default_segment (list): Description
"""
from __future__ import print_function
from sys import stdout

import os.path
import shutil

import numpy as np
import h5py

import data_tools as dataTools 
from  ..processing import index_automation as indexAuto
from  ..processing import flag_automation as flagAuto

reload(dataTools)
reload(indexAuto)
reload(flagAuto)

global def_segment_info
def_segment_info = [False, True, 60000, 0.1]

class Beam(np.ndarray):

	"""A custom np.array with extra attributes to make handling beams easier.
	
	Attributes:
	    beamType (string): Describe the beam state: on source (main) or off-source (off)
	    freqArr (np.array): The frequencies attributed to the channels of the data
	    segment (list): Describing the segmentation of the beam (and current segment location in overall data)
	    sourceFile (string): Location of the source file for the spectrum
	    timeStep (float): The time resolution associated with the dataset.
	"""
	
	def __new__(cls, beam, spectrumObj, segment_info, type_var):
		"""Initiatisation of Beam object
		
		Args:
		    beam (np.array): The dataset containing the beam in question
		    spectrumObj (TYPE): The spectrum object the beam is beging segmented from
		    segment_info (TYPE): Properties of the segment
		    type_var (TYPE): Describe the beam state: on source (main) or off-source (off)
		
		Returns:
		    Beam(np.array): Augmented numpy array
		"""
		self = np.asarray(beam).view(cls)
		self.freqArr = spectrumObj.freqArr
		self.timeStep = spectrumObj.timeStep
		self.segment = segment_info #Currently segmenting, to segment, segment size, segment overlap, start parent index, end parent index
		self.sourceFile = spectrumObj.sourceFile
		self.beamType = type_var
		
		return self

	def __array_finalize__(self, obj):
		"""Numpy classes have a unique process for sublcassing them; here we confirm our attributes
		
		Args:
		    obj (?): ?
		
		Returns:
		    TYPE: Description
		"""
		if obj is None: 
			return self
		
		self.freqArr = getattr(obj, 'freqArr', None)
		self.timeStep = getattr(obj, 'timeStep', None)
		self.segment = getattr(obj, 'segment_info', None)
		self.sourceFile = getattr(obj, 'sourceFile', None)
		self.beamType = getattr(obj, 'type_var', None)
		
		self.__dict__.update(getattr(obj, "__dict__", {}))


	def compress(self, percentile, axisVar = 1, squeeze = True, resample = [False, 0, 5, 33]):
		"""Resample a beam to lower the noise at the cost of accuracy
		
		Args:
		    percentile (int): Resampling percentile.
		    axisVar (int, optional): Axis to resample
		    squeeze (bool, optional): Boolean to remove superferlous axis dimensions (we may reduce axis to 1 datapoint, at which point it can be removed)
		    resample (list, optional): Perform extra resampling on a given axis by sampling over n data points as well. (TODO: reabstract)
		
		Returns:
		    Beam (Beam(np.ndarray): Resampled Beam object
		"""
		if resample[0]:
			tmp = self.copy()
			tmpReshape = np.array(tmp.shape)
			tmpReshape[resample[1]] = -1
			tmpReshape = np.insert(tmpReshape, resample[1] + 1, resample[2])
			
			modCheck = tmp.shape[resample[1]] % resample[2]
			
			print(tmpReshape, tmp.shape, modCheck)

			if modCheck != 0:
				tmp = np.delete(tmp, tmp.shape[resample[1]] + np.arange(-1 * modCheck, 0), resample[1])
			if resample[3] != 'mean':
				tmp = np.percentile(tmp.reshape(tmpReshape), resample[3], axis = resample[1] + 1)
			else:
				tmp = np.mean(tmp.reshape(tmpReshape), axis = resample[1] + 1)
			tmp = np.percentile(tmp, percentile, axis = axisVar)


		else:
			tmp = np.percentile(self, percentile, axis = axisVar)
		
		if squeeze:
			tmp = np.squeeze(tmp)
		return Beam(tmp, self, self.segment, self.beamType)
		


class Spectrum(object):

	"""The base spectrum class for handling different sets of observations, containing all the main functions.
	
	Attributes:
	    beams (h5py.Dataset): h5py references to the datasets containing beam data
	    dict (list): List of database names to reference to access the data
	    freqArr (np.array): The frequencies attributed to the channels of the data
	    iterN (int): Number of beams associated with the spectrum
	    offsetBeam (bool): Determine if beams other than DYNSPEC_000 are on-source or off-source
	    segment (list): Properties describing segments (and whether or not to segment) during processing
	    sourceFile (string): Original spectrum source file location
	    timeStep (float): Time resolution for the datasets.
	"""
	
	def __init__(self, fileLoc, offset_beams = None, segment_info = def_segment_info):
		"""Initialisation for spectrum objects.
		
		Args:
		    fileLoc (string): File location
		    offset_beams (bool, optional): Determine the preserne of off source beams. If not provided, we will define it based on the sampled stations (internations stations will cause this to become False)
		    segment_info (List, optional): A list of options with regard to sampling in the format: [Currently segmenting, to segment, segment size, segment overlap fraction]
		
		Returns:
		    Sectrum (Spectrum): The spectrum object, ready for usage.
		"""
		fileRef = h5py.File(fileLoc, 'r')
		self.dict = dictgen(fileRef)
		self.beams = [fileRef[dictEle] for dictEle in self.dict]


		confirm_spectrum_integrity(self.beams)


		self.iterN = len(self.beams)
		self.timeStep = fileRef.attrs["SAMPLING_TIME"] / 1e6
		self.freqArr = fileRef["DYNSPEC_000/COORDINATES/SPECTRAL"].attrs["AXIS_VALUE_WORLD"]

		if offset_beams == None:
			self.offsetBeam = determineBeamType(fileRef)
		else:
			self.offsetBeam = offset_beams

		self.segment = segment_info
		self.sourceFile = fileLoc


		return self


	def beam_iter(self, beamIdx, offsetIdx = 0):
		"""Iterate over beam elements. is this depreciated? Don't recall using it.
		
		Args:
		    beamIdx (TYPE): Description
		    offsetIdx (int, optional): Description
		
		Yields:
		    TYPE: Description
		"""
		beamArr = self.beams[beamIdx]

		if self.segment[1]:
			self.segment[0] = True

			segmentSize = self.segment[2]
			rangeLimit = int(beamArr.shape[0] / segmentSize) + 1

			endIdx = int(segmentSize * 2 * self.segment[3])
			startIdx = 0

			for idx in range(offsetIdx, rangeLimit):
				print(idx, rangeLimit)
				endIdx += segmentSize
				yield Beam(beamArr[startIdx:endIdx, ...], self, [True, True, 60000, 0.1, startIdx, endIdx], self.offsetBeam)
				startIdx += segmentSize

			self.segment[0] = False
		else:
			yield Beam(beamArr, self, [False, False, self.segment[2], self.segment[3], 0, self.segment[2]])


	def multi_beam_iter(self):
		"""Iterate over several beams, returning the results in a np.ndarray
		
		Yields:
		    Beam (Beam(np.ndarray): Beam object contaning all beams in a np.ndarray for a given samplnig step (defined by Spectrum.segment)
		"""
		if self.segment[1]:
			self.segment[0] = True

			segmentSize = self.segment[2]
			rangeLimit = self.beams[0].shape[0] % segmentSize

			endIdx = segmentSize + int(segmentSize * 2 * self.segment[3])
			endIdx = min(endIdx, self.beams[0].shape[0])

			startIdx = 0
			print([self.iterN, endIdx],[list(self.beams[0].shape[1:])])
			yieldArray = np.zeros(np.concatenate([[self.iterN, endIdx],list(self.beams[0].shape[1:])]))

			for idx in range(rangeLimit):

				for beamN in range(self.iterN):
					yieldArray[beamN, ...] = self.beams[beamN][startIdx:endIdx, ...]

				yield Beam(yieldArray, self, [True, True, 60000, 0.1, startIdx, endIdx], self.offsetBeam)
				
				startIdx += segmentSize
				endIdx += segmentSize

			self.segment[0] = False
		else:
			if raw_input("Are you sure you want to load all {0} data points from {1} beams into memory at once? If so, please state 'Yes', otherwise use any input.".format(np.prod(self.beams[0].shape), self.iterN)) == 'Yes':
				yieldArray = np.zeros(np.concatenate([[self.iterN], self.beams[0].shape]))
			for beamN in range(self.iterN):
				yieldArray[beamN, ...] = self.beams[beamIdx]
			yield Beam(beamArr, self, [False, False, self.segment[2], self.segment[3], 0, self.segment[2]])

	def beam_iter_offset(self, beamIdx, num_offset = None, offsetIdx = 0):
		"""Iterate over beams to give an array containing the main beam and an offset beam in a stacked manner. (timestep, channel, 2)
		
		Args:
		    beamIdx (int): Index of main beam, always returned first.
		    num_offset (int, optional): Iterate over the first n beams, will iterate over all if not defined.
		    offsetIdx (int, optional): Offset for the above iteration case. TODO: merge?
		
		Yields:
		    Beam (Beam(np.ndarray): Beam object for given 
		"""
		if not self.offsetBeam:
			print("This object was determined not to have offset beams, attempting to use this function will result in pain. Change the self.offsetBeam variable to True if you want to attempt it anyway.")
			yield None

		beamArr = self.beams[beamIdx]

		if not num_offset:
			num_offset = len(self.beams)

		offset_idx = np.arange(0, num_offset, dtype = int)
		offset_idx = offset_idx[offset_idx != beamIdx]

		if self.segment[1]:
			self.segment[0] = True

			segmentSize = self.segment[2]
			rangeLimit = int(beamArr.shape[0] / segmentSize) + 1

			endIdx = int(segmentSize * 2 * self.segment[3])
			startIdx = 0

			for idx in range(offsetIdx, rangeLimit):
				print(idx, rangeLimit)
				endIdx += segmentSize
				for offIdx in offset_idx:
					print(offIdx)
					yield Beam(np.dstack([beamArr[startIdx:endIdx, ...], self.beams[offIdx][startIdx:endIdx, ...]]), self, [True, True, 60000, 0.1, startIdx, endIdx], self.offsetBeam)
				startIdx += segmentSize


			self.segment[0] = False
		else:
			print("You just tried to load {0} data elemenets. To save you from your insanity, we stopped the action.".format(np.product([beam.shape for beam in self.beams])))
			yield None




class RawSpectrum(Spectrum):
	"""Helper object, inheriting Spectrum, for cleaning Dynamic Spectra through RFI flagging. Mostly placeholder.
	
	Attributes:
	    cleanOptions (List): Cleaning options for a simple RFI cleaner: [output file location, threshold percentile for upper sample flagging, offset from median-normalised to flag]
	"""
	
	def __init__(self, clean_options = None, *args, **kwargs):
		"""Initialisation, passed to Spectrum
		
		Args:
		    clean_options (List, optional): See Attributes
		    *args: used to pass on spectrum properties
		    **kwargs: used to pass on spectrum properties
		"""
		super(RawSpectrum, self).__init__(*args, **kwargs)

		if clean_options == None:
			self.cleanOptions = cleaning_options(self, self.sourceFile)
		else:
			self.cleanOptions = clean_options


	def clean(self):
		"""Automated cleaning through rawSpectrum.clean()
		
		Returns:
		    CleanSpectrum (CleanSpectrum): CleanSpectrum object for the cleaned dataset.
		"""
		outputFile = self.cleanOptions[0]

		if not os.path.exists(outputFile):
			shutil.copy(self.sourceFile, outputFile)
				
		with h5py.File(outputFile, 'r+') as writeFile:
			if self.offsetBeam:
				__ = flagAuto.offset_clean(self, writeFile)
			else:
				__ = flagAuto.clean(self, writeFile)
			writeFile.close()

		return CleanSpectrum(outputFile, self.timeStep, self.sourceFile, self.offsetBeams, self.segment)

	def cleanSpectrumTwin(self, fileRef = None, fileLoc = None):
		"""Return the 'twin' of this dataset if it has been previously cleaned with default options. Close the existing fileRef for this object if provided.
		
		Args:
		    fileRef (h5py.File, optional): File reference to check and close if needed
		    fileLoc (string, optional): If defined, pass the current properties to this object.
		
		Returns:
		    CleanSpectrum (CleanSpectrum): CleanSpectrum twin for this RawSpectrum.
		"""
		if (fileRef != None): fileRef.close()

		if fileLoc:
			newFileLoc = fileLoc
		else:
			newFileLoc = self.cleanOptions[0]

		if not os.path.exists(options[0]): raise IOError("Twin does not yet exist. Have you run a cleaning method yet or provided the right location?")

		return CleanSpectrum(fileLoc = newFileLoc, offset_beams = self.offsetBeam, segment_info = self.segment)

class CleanSpectrum(Spectrum):
	"""A spectrum object with helpers for analysing the scintillation index.
	
	Attributes:
		TODO: Have attributes saved to / generated from cleaned dataset h5 attributes.
	    pastCleaning (List): Describes the cleaning pass made on the dataset if the object is generated via a RawSpectrum call
	    pastSegments (List): Describes the segmentation used on the dataset if the object is generated via a RawSpectrum call
	"""
	
	def __init__(self, segment_options_past = None, clean_options_past = None, *args, **kwargs):
		"""Initialisation
		
		Args:
		    segment_options_past (List, optional): See Attributes
		    clean_options_past (List, optional): See Attributes
		    *args: used to pass on spectrum properties
		    **kwargs: used to pass on spectrum properties
		"""
		super(CleanSpectrum, self).__init__(*args, **kwargs)
		self.pastSegments = segment_options_past
		self.pastCleaning = clean_options_past

	def determine_index(self, optionsArr = None):
		"""Automated index calcation
		
		Args:
			optionsArr (List, optional): Options for determining the scintillation index. TODO: Cleanup
		Returns:
		    TODO: Todo
		"""
		return indexAuto.process(self, optionsArr)


def cleaning_options(self, sourceFile, thresholdPercentile = 99., flaggerConst = 0.2):
	"""Default RFI Cleaning options
	
	Args:
	    sourceFile (string): File location (to generate output location)
	    thresholdPercentile (float, optional): Upper penctile to flag
	    flaggerConst (float, optional): Offset from median-normalised data to flag.
	
	Returns:
	    TYPE: Description
	"""
	outputFile = "./" + self.sourceFile[:-3].split("/")[-1] + "_rfi_flagged.h5"

	return [outputFile, thresholdPercentile, flaggerConst]

def confirm_spectrum_integrity(fileRef):
	"""Determine if the sequential beams conform to expected shapes / input files. 
	
	Args:
	    fileRef (h5py.File): Input h5py File reference
	"""
	for beamRef in fileRef[:-1]:
			assert(beamRef.file == fileRef[-1].file) # It's literally reading froma single file, why?
			assert(beamRef.shape == fileRef[-1].shape)

def dictgen(fileRef):
	"""Generate a dataset dictionary -- duplicates data_tools.py?
	
	Args:
	    fileRef (h5py.File): File referencr to analyse
	
	Returns:
	    returnList (List): List of dyanmic spectra data locations in the given file
	"""
	returnList = [group[0] + "/DATA" for group in fileRef.items() if ("DYNSPEC" in group[0])]
	returnList.sort(key=lambda f: int(filter(str.isdigit, f.encode("ASCII"))))
		
	return returnList


def determineBeamType(fileRef):
	"""Analyse the stations used for observations to determine if we have off source beams available.
	
	Args:
	    fileRef (h5py.File): File reference to analyse
	
	Returns:
	    bool: Boolean of off source beam presence
	"""
	stations = fileRef.attrs["OBSERVATION_STATIONS_LIST"].tolist()

	if all([('CS' in stringVal) or ('RS' in stringVal) for stringVal in stations]):
		return True
	else:
		return False

