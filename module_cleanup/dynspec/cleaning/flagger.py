"""Simplified RFI Flagger. Mostly Abandoned. Kept as a skeletal function for now.
"""
from __future__ import print_function
from sys import stdout

import numpy as np
import matplotlib.pyplot as plt




def autoFlagger(inputIm, flaggerConst, thresholdPercentile):
	"""Header function for processing RFI in structured datasets: (timesteps, frequency steps)
	
	Args:
	    inputIm (np.ndarray): Input dynamic spectrum data
	    flaggerConst (float): Threshold for RFI flagging
	    thresholdPercentile (int): 0-100 Percentile for data flagging of upper extremae.
	
	Returns:
	    removedNoiseImage (np.ndarray): Returned array with flagged RFI samples as 0. data points
	    nullPoints (np.ndarray): Location of flags in a boolean-style array (False for flagged) for easy removal of offset beams
	"""
	print("Input image {0}".format(inputIm.shape))

	# Make a working copy of the data
	flagsCopy = inputIm.copy()
	processCopy = inputIm.copy()

	# If the data isn't normalised or is somehow very extreme, try again
	if np.max(flagsCopy) > 10.:
		median_values = np.median(flagsCopy, axis = 0)
		flagsCopy = flagsCopy / median_values

	print("Data Normalised                                      ", end = '\r')
	stdout.flush()
	
	# Perform channel flagging on the dataset
	channelFlag = channelFlagger(flagsCopy, flaggerConst, thresholdPercentile)

	print("Channel Flagging Performed                                      ", end = '\r')
	stdout.flush()

	# Create a simple array for converting the flagged data points to 0 through
	# 	elementwise multiplication.
	nullPoints = 1. - channelFlag
	removedNoiseImage = processCopy * nullPoints
	
	print("Flagged Data Removed                                      ", end = '\r')
	stdout.flush()

	return removedNoiseImage, nullPoints


def channelFlagger(data, triggerChannel, triggerThreshold):
	"""Perform channel-based flagging of data based on given thresholds.
	
	Args:
	    data (np.nparray): Input dynamic spectrum dataset
	    triggerChannel (float): Offset from nearby channels to cause extremae flagging
	    triggerThreshold (int): Percentile to flag upper extremae by (useful for multiple load channels, such as DAB)
	
	Returns:
	    triggeredArr (np.ndarray): Boolean-style array of locations of flags (True where flagged)
	"""
	# KNOWN ISSUE: Cannot handle RFI source over more than 2 consequetive channels

	###
	# Given a dataset and a threshold between channels, flag data for suspected RFI
	###

	print("Channel Flagger {0}".format(data.shape))
	# Generate arrays of the data shifted one up and one down
	shiftUp = np.roll(data, -1, axis = 0)
	shiftDown = np.roll(data,1, axis = 0)

	# Check if either the channel above or below each point is above the threshold
	triggeredArr = ((data > shiftUp + triggerChannel).astype(int) 
		+ (data > shiftDown + triggerChannel).astype(int))

	triggeredArr += (data > np.percentile(data, triggerThreshold)).astype(int)

	# Limit return values to be true (1.) or false (0.), sequential channels may have 
	#	generated values of 2 we need to account for.
	triggeredArr = np.clip(triggeredArr, -1., 1.)
	
	testArr = np.sum(triggeredArr, axis = (1,2))
	triggeredArr[testArr > data.shape[1] * 0.75,...] = 1.

	# Return array of flags
	return triggeredArr.astype(int)




### Unused, need a better method.
def temporalFlagger(data, percentile = 99.7):
	"""Summary
	
	Args:
	    data (TYPE): Description
	    percentile (float, optional): Description
	
	Returns:
	    TYPE: Description
	"""
	###
	# Given a data set and a percentile, see if a time sample's median is above
	#	the nth sample of an expect normal distrobution for the data. Flag if true.
	###

	# Get the median values for each time sample
	medianVal = np.median(data, axis = 1)

	# Determine the value corresponding to the percentile given in the normal distrobution
	extremae = np.percentile(medianVal, percentile, axis = 1)

	# Generate an array of true (1.) or false (0.) flags for if the data is beyond the
	#	given percentile.
	flaggedTime = medianVal > extremae

	# Return the array of flags
	return flaggedTime.astype(int)
