"""Generate spectrum object and perform actions on them in an automated manner.

This is separate from flag_automated due to a historical circular import. To be merged.
"""
from ..tools import dynamicSpectrum as dS

reload(dS)

def generate_raw_spectrum(file_loc, offset_beams = None, segment_info = dS.def_segment_info, clean_options = None):
	"""Return a RawSpectrum object for a given h5 observation.
	
	Args:
	    file_loc (string): Location of the beamformed data.
	    offset_beams (bool, optional): Describe whether non-000 datasets are observing on-source (True) or off-source (False), defaults to None where we will programatically decide based on observing stations.
	    segment_info (list, optional): Describe the segmentation properties for the dataset. See dynamicSpectrum.py for further information
	    clean_options (dict, optional): Description
	
	Returns:
	    RawSpectrum: The raw spectrum object, ready to be cleaned by .clean()
	"""
	spectrumObj = dS.RawSpectrum(fileLoc = file_loc, offset_beams = offset_beams, segment_info = segment_info, clean_options = clean_options or dS.cleaning_options(file_loc))

	return spectrumObj


def generate_and_clean(file_loc, offset_beams = None, segment_info = dS.def_segment_info, clean_options = None):
	"""Summary
	
	Args:
	    file_loc (string): Location of the beamformed data.
	    offset_beams (bool, optional): Describe whether non-000 datasets are observing on-source (True) or off-source (False), defaults to None where we will programatically decide based on observing stations.
	    segment_info (list, optional): Describe the segmentation properties for the dataset. See dynamicSpectrum.py for further information
	    clean_options (list, optional): Desired cleaning options for the spectrum. Defaults to None to gain defaults from the dynamicSpectrum.py defaults
	
	Returns:
	    CleanSpectrum: The cleaned spectrum object, ready to be analysed by .determine_index()

	"""
	rawSpec = generate_spectrum(fileLoc = file_loc, offset_beams = offset_beams, segment_info = segment_info, clean_options = clean_options or dS.cleaning_options(file_loc))
	cleanSpec = rawSpec.clean()

	return cleanSpec

def generate_and_clean_and_index(file_loc, offset_beams = None, segment_info_clean = dS.def_segment_info, segment_info_indexing = [False, True, 60000, 0.1], clean_options = None):
	"""Summary
	
	Args:
	    file_loc (string): Location of the beamformed data.
	    offset_beams (bool, optional): Describe whether non-000 datasets are observing on-source (True) or off-source (False), defaults to None where we will programatically decide based on observing stations.
	    segment_info_clean (list, optional): Describe the segmentation properties for the dataset during the cleaning process. See dynamicSpectrum.py for further information
	    segment_info_indexing (list, optional): Describe the segmentation properties for the dataset during the scintillation index process. See dynamicSpectrum.py for further information
	    clean_options (list, optional): Desired cleaning options for the spectrum. Defaults to None to gain defaults from the dynamicSpectrum.py defaults
	
	Returns:
	    np.ndarray: The array of scintillation indicies.
		CleanSpectrum: The clean spectrum object for further manipulation or information.
	"""
	cleanSpec = generate_and_clean(file_loc, offset_beams, segment_info_clean, clean_options)
	idx, ___ = cleanSpec.clean()

	return idx