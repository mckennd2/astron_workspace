"""A helper file for managing differemt methods for calculating the scintillation index of a dynamic spectrum

"""
import numpy as np 
import matplotlib.pyplot as plt

from ..scintillation_index import index_calculator as indexCalc

reload(indexCalc)


def process(cleanSpectrum, optionsArr = None):
	"""Taking a cleanSpectrum object and a set of options (defaults in CleanSpectrum),
		this function will generate your scintillation indicies based on your given options.
	
	Args:
	    cleanSpectrum (CleanSpectrum): Object holding all of our requeired data / references
	    optionsArr (List, optional): A list holding cleaning options: 
	    	[Scintillation index method,
	    	Plot debug information
	    		[Resample data bool,
	    		Resample axis,
	    		Time steps to sample for 1 new point
	    		Percentile to take from samples],
	    	Width (in time samples) for running mean in std method]
	
	Returns:
	    IndexArray(np.ndarray): List containing the scintillation indicies
	    calc(List): Debug information
	"""
	idx = []

	if cleanSpectrum.offsetBeam:
		beamIter = cleanSpectrum.beam_iter_offset(0)
		print("Processing off-source beams")
	else:
		beamIter = cleanSpectrum.beam_iter(0)
		print("Processing on-source beam exclusively")

	if optionsArr:
		if optionsArr[0] == "std":
			iterFunc = indexCalc.stdMethod
		else:
			iterFunc = indexCalc.powerIntegral
	else:
		iterFunc = indexCalc.powerIntegral
		optionsArr = [None] + indexCalc.defaultOptions()

	for idxv, beam in enumerate(beamIter):
		calc = iterFunc(beam, optionsArr[1:])
		print("Calculated Index: {0}".format(calc[0]))

		idx.append(calc[0])

	beamCount = len(cleanSpectrum.beams) - 1
	idx = np.array(idx).reshape(-1, beamCount)

	return idx, calc





# Scratch / baxkup code
'''

def irrmerg():
segment = 430450
for i in range(1):
	dsv = writer.require_dataset("observations/2018-06-22-merged-{0}/image_data".format(i), shape = (1, segment, 100, 100), dtype = np.float32)
	header = writer.require_group("observations/2018-06-22-merged-{0}/raw_headers/0".format(i))
	for key,val in reader["observations"].items()[0][1].items()[1][1]["0"].attrs.items():
		header.attrs.create(key, val)

	startIdx = 0
	endIdx = 200
	for idx, obsTime in enumerate(reader["observations"].items()[i * segment:(i + 1) * segment]):
		obs = obsTime[1]
		print(obs["image_data"], dsv)
		dataSet = obs["image_data"]
		startIdx = endIdx
		endIdx = startIdx + dataSet.shape[1]
		print(startIdx, endIdx)
		dsv[:, startIdx:endIdx, ...] = dataSet

		for key,val in obs.attrs.items():
			dsv.attrs.create(key, val)
			print(key, val)

 plt.scatter(test3x, test3[0], s = 0.05, label = 'Individual Pixels'); 
     ...: 
     ...: plt.errorbar(test2x, test2[0], yerr = 3. * test2[1], fmt = 'ro', alpha
     ...:  = 0.3, label = 'Averaged Pixels (3$\sigma$)');
     ...: plt.errorbar(test2x, test2[0], yerr = test2[1], fmt = 'go', label = 'A
     ...: veraged Pixels (1$\sigma$)'); 
     ...: plt.xlabel("Time On 2018-04-18 (Hours)")
     ...: plt.ylabel("Scintillation Index")
     ...: plt.title("3C48 Scintillation Indicies Derived from Imaging Data")
     ...: plt.legend()
     ...: plt.figure(2)
     ...: plt.xlabel("Time On 2018-04-18 (Hours)")
     ...: plt.ylabel("Scintillation Index")    
     ...: plt.title("3C48 Scintillation Indicies Derived from Dynamic Spectra")
     ...: plt.plot(idxdebugx, np.median(idxdebug, axis = 1), label = "Median Ind
     ...: ex (Off-Source Beams Sampled = 6)")
     ...: plt.plot(idxdebugx, idxdebug, alpha = 0.2)
     ...: plt.legend()
     ...: 
     ...: plt.show()

In [136]: savearr = [test3x, test3, test2x, test2, idxdebugx, idxdebug]

In [137]: np.save("./working_idx.npy", savearr)





In [116]: np.save("./3C147_imagingidx-merged.npy", c147_test)

In [117]: np.save("./3C147_imagingidx-single.npy", c147_test_single_sources)

'''