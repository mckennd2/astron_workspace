"""A routine for automated flagging of spectrum objects.


Needs work to be truely useful, in most cases this step is currently bypassed.
"""
import numpy as np 
import h5py


from ..cleaning import flagger

reload(flagger)

def clean(rawSpectrum, outputRef):
	"""Given a raw spectrum and an output h5 file reference, process the data as all being on-source beams
	
	Args:
	    rawSpectrum (RawSpectrum): A RawSpectrum object, generated for the dataset of interest
	    outputRef (h5py.File): An output h5py file reference to write cleaned data to.
	
	Returns:
	    CleanSpectrum: An object containing all of the information needed to process the clean spectrum to determine scintillation indicies.
	"""
	if rawSpectrum.offsetBeam:
		print("Wrong cleaning method, calling offset_clean()")
		return offset_clean(rawSpectrum, outputRef)

	__, thresholdPercentile, flaggerConst = self.cleanOptions


	for segment in rawSpectrum.multi_beam_iter():
		for idx, beam in enumerate(segment):
			flaggedIm, __ = flagger.autoFlagger(beam, flaggerConst, thresholdPercentile)
			print(rawSpectrum.dict[idx], beam.segment[4], beam.segment[5])
			outputRef[rawSpectrum.dict[idx]][beam.segment[4]:beam.segment[5], ...] = flaggedIm

	return rawSpectrum.cleanSpectrumTwin(outputRef)


def offset_clean(rawSpectrum, outputRef):
	"""Given a raw spectrum and an output h5 file reference, process the data as the first being an on-source and others being off-source
	
	Args:
	    rawSpectrum (RawSpectrum): A RawSpectrum object, generated for the dataset of interest
	    outputRef (h5py.File): An output h5py file reference to write cleaned data to.
	
	Returns:
	    CleanSpectrum: An object containing all of the information needed to process the clean spectrum to determine scintillation indicies.
	"""
	if not rawSpectrum.offsetBeam:
		print("Wrong cleaning method, calling clean()")
		return clean(rawSpectrum, outputRef)

	__, thresholdPercentileTrue, flaggerConst = rawSpectrum.cleanOptions
	thresholdPercentile = thresholdPercentileTrue

	for segment in rawSpectrum.multi_beam_iter():
		for idx, beam in enumerate(segment):
			if idx != 0:
				segment *= flaggedArray
			else:
				thresholdPercentile = thresholdPercentileTrue
				beam, mask = flagger.autoFlagger(beam, flaggerConst, thresholdPercentile)
				thresholdPercentile = 99.97

			flaggedIm, mask2 = flagger.autoFlagger(beam, flaggerConst, thresholdPercentile)

			if idx == 0:
				flaggedArray = mask + mask2

			print(rawSpectrum.dict[idx], segment[4], segment[5])
			outputRef[rawSpectrum.dict[idx]][segment[4]:segment[5], ...] = flaggedIm

	return rawSpectrum.cleanSpectrumTwin(outputRef)
