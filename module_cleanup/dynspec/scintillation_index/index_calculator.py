"""A file dedicated to extracting scintillation indicied from dynamic spectra.

To be merged.
"""
import numpy as np 
import scipy.signal as spys

from ..tools import filters
#from ..tools import dynamicSpectrum as dS

import scipy.signal as spys
import scipy.optimize as spyo


reload(filters)
import matplotlib.pyplot as plt
def powerIntegral(beam, optionsArr = None):
	"""Using the power integral method, determine the scintillation index for a given source.
	
	Args:
	    beam (Beam): Beam-type object for describing the dataset
	    optionsArr (dict, optional): Options dict for determining the settings used for the operation. Generated fromd defaults when undefined.
	
	Returns:
	    TODO: Todo cleanup
	"""
	#print(beam.shape)

	if not optionsArr:
		optionsArr = powerIntegralOptions()

	plotVar, resampleOpt, __ = optionsArr

	if resampleOpt[0]:
		beam.timeStep = float(resampleOpt[2]) * beam.timeStep
		print("Timestep: {0}".format(beam.timeStep))
	compressedBeam = beam.compress(35, 1, beam.beamType, resample = resampleOpt)
	compressedBeam = dirtyclean(compressedBeam)

	meanOffset = spys.convolve2d(compressedBeam[..., 1].reshape(-1, 1), np.ones(10). reshape(10, 1) / 10., "same", "symm").reshape(-1)

	freqOutput, fftOutput = spys.welch(compressedBeam[..., 0] - meanOffset, fs = 1. / beam.timeStep, window = ("kaiser", 8.6), nperseg = 512, return_onesided=True)

	#plt.loglog(freqOutput, fftOutput)
	#plt.title("Power Spectrum of 3C147 over 612 seconds starting 22-06-2018T08:00:00")
	#plt.xlabel("Frequency (Hz)")
	#plt.ylabel("Power (arb. unit)")
	#plt.grid()
	#plt.show()

	startIdx, endIdx, midSlope = sigmoidFit(np.log10(freqOutput), np.log10(fftOutput))

	if beam.beamType:
		__, offsetFFt = spys.welch(compressedBeam[..., 1] - meanOffset, fs = 1. / beam.timeStep, window = ("kaiser", 8.6), nperseg = 512, return_onesided=True)

		correction = np.square(np.mean(compressedBeam[..., 0] - compressedBeam[..., 1]))
		netPower = fftOutput[startIdx:endIdx] - offsetFFt[startIdx:endIdx]
		#netPower = fftOutput[startIdx:endIdx]
	else:
		correction = np.square(np.mean(compressedBeam[..., 0]))
		netPower = fftOutput[startIdx:endIdx]

	integral = np.trapz(netPower, freqOutput[startIdx:endIdx])
	plotVar = False
	if plotVar:
		plt.figure(1)
		plt.plot(compressedBeam[..., 0] - meanOffset)
		plt.plot(compressedBeam[..., 1] - meanOffset)
		plt.figure(3)
		plt.loglog(freqOutput, fftOutput)
		if beam.beamType:
			plt.loglog(freqOutput, offsetFFt)
		plt.gca().twinx().semilogx(freqOutput, invSigmoid([freqOutput, -1e5], midSlope[1], midSlope[2]))
		plt.figure(4)
		plt.loglog(fftOutput)
		if beam.beamType:
			plt.loglog(offsetFFt)
		plt.gca().twinx().semilogx(invSigmoid([freqOutput, -1e5], midSlope[1], midSlope[2]))

		plt.show()
	return np.sqrt(integral / correction), np.mean(compressedBeam, axis = 0), [integral, correction], [midSlope, midSlope[-1], np.sqrt(np.trapz(np.ones(freqOutput.shape[0]) * midSlope[-1], freqOutput)), np.sqrt(np.trapz(fftOutput, freqOutput)), np.sqrt(np.trapz(offsetFFt, freqOutput))]


def stdMethod(beam, optionsArr = None):
	"""Using the variability method, calculate the scintillation index for a given source,
	
	Args:
	    beam (Beam): Beam-type object for describing the dataset.
	    optionsArr (dict, optional): Options dict for determining the settings used for the operation. Generated fromd defaults when undefined.
	
	Returns:
	    TODO: Todo
	"""
	if not optionsArr:
		optionsArr = defaultOptions()

	plotVar, resampleOpt, avgTime = optionsArr

	if resampleOpt[0]:
		beam.timeStep = float(resampleOpt[2]) * beam.timeStep
		print("Timestep: {0}".format(beam.timeStep))
	compressedBeam = beam.compress(35, 1, beam.beamType, resample = resampleOpt)
	compressedBeam = dirtyclean(compressedBeam)


	#compressedBeam = filters.apply_butterworth_filter(compressedBeam, axisVar = -1, lowF = None, highF = 10., btypeVar = "lowpass", order = 2)

	kernelSize = int(avgTime * 1. / beam.timeStep)
	print(kernelSize)
	#gaussKernel = np.exp(-1. * np.square(np.linspace(-1, 1, kernelSize)) / 2.) / np.sqrt(2. * np.pi)
	#gaussKernel /= np.sum(gaussKernel)
	#print(gaussKernel)

	meanOffset = spys.convolve2d(compressedBeam[..., 1].reshape(-1, 1), np.ones(kernelSize)[:, np.newaxis] / kernelSize, "same", "symm")
	#meanOffset2 = spys.convolve2d(compressedBeam[..., 1].reshape(-1, 1), np.ones(kernelSize)[:, np.newaxis] / kernelSize, "same", "symm")

	#plt.plot(meanOffset)
	#plt.figure(2)
	#plt.plot(meanOffset2)
	#plt.show()
	
	sourceIntensity = np.mean(compressedBeam[..., 0] - meanOffset)
	stdDev = np.std(compressedBeam[..., 0])


	offsetStd = np.std(compressedBeam[..., 1])
	dS = np.sqrt((np.square(stdDev) - np.square(offsetStd))/ np.square(sourceIntensity))

	syserr = np.sqrt(np.divide(np.square(stdDev) - np.square(offsetStd), np.square(sourceIntensity))) - dS
	print(syserr)
	err = np.sqrt(np.square(syserr) + np.divide(np.square(dS), compressedBeam.shape[0] / (1.5 / beam.timeStep)))
	print(np.median(err))
	return [dS, err], []

def defaultOptions():
	"""Variability Default options. TODO
	
	Returns:
	    List: TODO
	"""
	return [False, [True, 0, 5, 20], 30.]

'''
def dirtyclean(data):
	"""Summary
	
	Args:
	    data (TYPE): Description
	
	Returns:
	    TYPE: Description
	"""
	median1 = np.median(data[..., 0], axis = 1)
	#medmed1 = 6.12e12
	medmed1 = np.median(median1)
	flag = np.argwhere(np.bitwise_or(median1 > 1.33 * medmed1, median1 < 0.66 * medmed1))
	#flag = np.argwhere(np.bitwise_or(median1 > medmed1, median1 < 0.66 * medmed1))
	data[flag, :, 0] = data[flag - 200, :, 0]
	data[flag, :, 1] = data[flag - 200, :, 1]
	print(flag)

	if data.shape[2] != 1:
		median2 = np.median(data[..., 1], axis = 1)
		#medmed2 = 6.12e12
		medmed2 = np.median(median2)
		flag2 = np.argwhere(np.bitwise_or(median2 > 1.33 * medmed2, median2 < 0.66 * medmed2))
		#flag2 = np.argwhere(np.bitwise_or(median2 > medmed2, median2 < 0.66 * medmed2))
		data[flag2, :, 1] = data[flag2 - 15, :, 1]
		print(flag2)


	return data
'''

def dirtyclean(data):
	"""Rough cleanup for extremely dirty datasets,
	
	Args:
	    data (Beam(np.ndarray)): (timesteps, channels) shape array to be processed
	
	Returns:
	    data (Beam(np.ndarray)): Flagged dataset
	"""
	median1 = np.median(data[..., 0], axis = 0)
	#medmed1 = 6.12e12
	medmed1 = np.median(median1)
	flag = np.argwhere(np.bitwise_or(median1 > 1.33 * medmed1, median1 < 0.66 * medmed1))
	#flag = np.argwhere(np.bitwise_or(data[:, 0] > medmed1, data[:, 0] < 0.66 * medmed1))
	data[flag, 0] = data[flag - 200, 0]
	data[flag, 1] = data[flag - 200, 1]
	print(flag)

	if data.shape[1] != 1:
		median2 = np.median(data[..., 1], axis = 0)
		#medmed2 = 6.12e12
		medmed2 = np.median(median2)
		flag2 = np.argwhere(np.bitwise_or(median2 > 1.33 * medmed2, median2 < 0.66 * medmed2))
		#flag2 = np.argwhere(np.bitwise_or(data[:, 1] > medmed2, data[:, 1] < 0.66 * medmed2))
		data[flag2, 1] = data[flag2 - 15,  1]
		print(flag2)


	return data

def calculate_offsources(beam, offsource):
	"""Abandoned due to issues estimating the off source beam from scratch.
	
	Args:
	    beam (TYPE): Description
	    offsource (TYPE): Description
	
	Returns:
	    TYPE: Description
	"""
	return

def invSigmoid(datafloor, a, c):
	"""A function describing the reversed sigmoid function, f(x) = 1 / (1 + e^(a(x - c)))
	
	Args:
	    datafloor (List): List containing the dataset and a minimum value for the floor. Useful for constrained optomisation.
	    a (float): Exponential parameter for steepness of sigmoid
	    c (float): Exponential parameter for offset of the turning point from 0.
	
	Returns:
	    (float, array-like): Return for the sampled x values
	"""
	x, floor = datafloor
	return np.maximum(np.divide(1., 1. + np.exp(a * (x - c))), floor)

def invInvSigmoid(y, a, c):
	"""Inverse of the sigmoid function to estimate the value of x for a given y value
	
	Args:
	    y (float, array-like): Output of the sigmoid (on range [0,1])
	    a (float): Exponential parameter for steepness of sigmoid
	    c (float): Exponential parameter for offset of the turning point from 0.
	
	Returns:
	   	(float, array-like): Solutions to given input
	"""
	return np.divide(np.log(np.divide(1., y) - 1.), a) + c

def invSigmoidSlope(x, a, c):
	"""Return the float of the sigmoid at a given point f'(x) = -a * e^(a(x - c)) / (1 + ^(a(x - c)))^2
	
	Args:
	    x (float, array-like): Sample x locations
	    a (float): Exponential parameter for steepness of sigmoid
	    c (float): Exponential parameter for offset of the turning point from 0.
	
	Returns:
	    TYPE: Description
	"""
	exponential = np.exp(a * (x - c))
	return -1. * np.divide(a * exponential, np.square(exponential + 1))

def sigmoidFit(freq, dataset):
	"""Automated process for fitting a reversed sigmoid to a given function.
	
	Args:
	    freq (np.ndarray): Sampling x-array
	    dataset (np.ndarray): Sampling y-array
	
	Returns:
	    startIdx (int): Suggested integration start point for power spectrum
	    endIdx (int): Suggested integration end point for power spectrum
	    debug (TODO): Debug information, including slope.
	"""
	dataset = dataset[1:]
	freq = freq[1:] # Remove f = 0 to remove infinite element

	print(np.argwhere(np.isnan(dataset)), np.argwhere(np.isinf(dataset)))
	print(np.argwhere(np.isnan(freq)), np.argwhere(np.isinf(freq)))

	earlyMax = np.mean(dataset[:5])
	lateMean = np.mean(dataset[-1 * int(len(dataset) * 0.2):])

	normData = (dataset - lateMean) / (earlyMax - lateMean)

	lateMean = np.percentile(normData[-1 * int(len(normData) * 0.2)], 95)
	normData = np.maximum(normData, lateMean)
	print(np.argwhere(np.isnan(normData)), np.argwhere(np.isinf(normData)))

	param = spyo.curve_fit(invSigmoid, [freq, lateMean], normData, maxfev = 100000)

	print(param[0])

	lateMean = np.percentile(normData[-1 * int(len(normData) * 0.2)], 95)

	#sigmoidUpper = 0.95 + 0.05 * invSigmoid([-1. * param[0][0], 0.], 1., 3.) # Use a sigmoid to generate a good pper limit. The sharper the fit, the closer to the noise roof we integrate to.
	#print(sigmoidUpper)
	
	sigmoidUpper = 0.97
	sigmoidLower = max(max(invSigmoid([freq[-1], lateMean], param[0][0], param[0][1]), lateMean), 0.03)
	startVal = invInvSigmoid(sigmoidUpper, param[0][0], param[0][1])
	endVal = invInvSigmoid(sigmoidLower, param[0][0], param[0][1])

	#plt.figure(3)
	#plt.plot(dataset)
	#plt.figure(4)
	#plt.plot(freq, normData, label = "Raw Data")
	#plt.plot(freq, invSigmoid([freq, lateMean], param[0][0], param[0][1]), label = "Fitted Sigmoid")
	#plt.plot(plt.xlim(), np.ones([2]) * 0.03, c = 'r')
	#plt.title("Power Spectrum of 3C147 over 612 seconds starting 22-06-2018T08:00:00")
	#plt.xlabel("Frequency (Hz)")
	#plt.ylabel("Power (arb. unit)")
	#plt.grid()
	#plt.show()
	#plt.show()

	idxes = np.argwhere(np.logical_and(freq > -0.7, freq < endVal))
	print(startVal, endVal)

	if idxes.size != 0:
		startIdx = idxes[0][0]
		endIdx = idxes[-1][0]
	else:
		startIdx = 0
		endIdx = np.argwhere(normData == lateMean)[0][0] + 100
	print(startIdx, endIdx)
	midSlope = param[0][1] # f(c) = 0.5 for a sigmoid
	slope = invSigmoidSlope(midSlope, param[0][0], param[0][1])
	
	lateMeanPreFix = np.mean(dataset[freq > 0.6])
	debug = np.power(10., lateMeanPreFix)
	return startIdx + 1, endIdx + 1, [midSlope, param[0][0], param[0][1], debug]
