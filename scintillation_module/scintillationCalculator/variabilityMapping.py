"""Process a given data input through the variability method

Heavily based off the work of Morgan et al. MNRAS 473, 2965-2983
"""
import numpy as np
import scipy.stats as spystat

from ..tools import filterTools

def variabilityResults(dataChunk, optionsArr, workVar = None):
	"""Summary
	
	Args:
	    dataChunk (ObservationChunk): Input dataset object with attributes
	    optionsArr (dict): Variability processing options
	    workVar (np.ndarray, optional): Description
	
	Returns:
	    list: Various results of the processing
	"""

	#Extract options
	__, highF, lowF, filterOrder, n_ips, stdMul = optionsArr
	nSamples = dataChunk.shape[0]

	# Get mean values
	meanVal = np.mean(dataChunk)

	# Use a bandpass filter to limit variability outside of designated frequency components
	filteredChunk = filterTools.applyButterworthFilter(dataChunk, axisVar = 0, lowF = lowF, highF = highF, btypeVar =  "bandpass", order = filterOrder)

	# Handle imaging data and beamformed data separately. We can generate system noise statistics from non-flagged pixels in the imaging process
	# while we need to process an off-source beam to determine the system noise for the beamformed data. Beamformed off-source data is passed in through
	# the workVar variable
	if dataChunk.typeVar == 'imageCube':
		varChunk = std(filteredChunk, axisVar = 0)
		meanVar = np.mean(varChunk)
		stdVar = std(varChunk)
		flaggedPixels = (varChunk - meanVar) > (stdMul * stdVar)
		stdVar = std(varChunk[~flaggedPixels])
	else: 
		varChunk = std(filteredChunk, axisVar = 0)
		offFilter = filterTools.applyButterworthFilter(workVar, axisVar = 0, lowF = lowF, highF = highF, btypeVar =  "bandpass", order = filterOrder)
		stdVar = std(offFilter)
		meanVar = np.mean(offFilter)
		flaggedPixels = [True]

	# Replication of Morgan et al.'s equations'
	scintFluxDensity = np.sqrt(np.square(varChunk) - np.square(meanVar))

	sysErr = np.sqrt(abs(np.square(varChunk + stdVar) - np.square(meanVar))) - scintFluxDensity
	scintErr = np.sqrt(np.square(sysErr) + np.divide(np.square(scintFluxDensity), (nSamples / n_ips)))
	
	return scintFluxDensity, scintErr, flaggedPixels, varChunk, meanVal, stdVar

def std(dataArr, axisVar = None):	
	"""Custom standard deviation based on the interhexile range.
	
	Args:
	    dataArr (TimeSeries): Input dataset
	    axisVar (int, optional): Axis to use for calculation
	
	Returns:
	    np.ndarray: Standard deviation
	"""
	percentile16, percentile83 = np.percentile(dataArr, (100./6., 500./6.), axis = axisVar)

	return (percentile83 - percentile16) / 2.
