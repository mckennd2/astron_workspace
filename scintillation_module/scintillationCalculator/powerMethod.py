"""Process data through the varaibility method, prepare datasets for analysis

Inspired by the Power method introduced by Manoharan 1993SoPh..148..153M
"""
import multiprocessing as mp
import numpy as np
import scipy.signal as spys
import powerSpectrum

import logging
logger = mp.log_to_stderr(logging.INFO)

def errorsHead(obsObj, indexMethod):
	samples = obsObj.processingOptions['powerSpectrum']['errorBarSamples']

	print("Begining to process observations of {0} begining {1}".format(obsObj.sourceName, obsObj.obsTime[0]))
	indices = indexMethod(obsObj)

	timeSeg = obsObj.segmentInfo['segmentByTime']
	print(timeSeg, 'timeseg')
	if timeSeg:
		obsObj.segmentInfo['segmentByTimeSeconds'] /= samples
	else:
		obsObj.segmentInfo['segmentSize'] /= samples
	obsObj.checkSegmentTime()
	if obsObj.segmentInfo['segmentSize'] < 512:
		print("The sampling length is too short to estimate the error by chosen {0} point sampling, generating array of 0s for errorbars".format(samples))
		indicesErr = np.repeat(np.zeros_like(indices), samples, axis = 0)
	else:
		print("Begining error sampling for observation of {0} begining {1}".format(obsObj.sourceName, obsObj.obsTime[0]))
		indicesErr = indexMethod(obsObj)

	if timeSeg:
		obsObj.segmentInfo['segmentByTimeSeconds'] *= samples
	else:
		obsObj.segmentInfo['segmentSize'] *= samples


	print(indicesErr.shape, indices.shape)
	errArr = np.zeros_like(indices)
	errSamples = indicesErr.shape[0]
	for idx in range(len(indices)):
		errSample = indicesErr[max(idx * samples - (samples / 2), 0): min(errSamples, idx * samples + (samples / 2)), :].transpose((1,2,0))
		print(errSample.shape)

		errArr[idx] = np.sqrt(np.nansum(np.square(errSample - indices[idx][..., np.newaxis]), axis = -1) / max(errSample.shape[0] - 1., 1.))
		print(errArr[idx].shape)

		nanValues = np.isnan(indices[idx])
		if nanValues.all():
			indices[idx][...] = 0.
		elif nanValues.any():
			nanResample = np.argwhere(nanValues)
			#print(nanValues, nanResample, indices[idx].shape, errSample.shape)
			#print(nanValues[0], nanValues[0][0], nanValues[0][1])
			for nanVal in nanResample:
				print(nanVal)
				print(indices[idx][nanVal[0], nanVal[1]])
				print(errSample[nanVal[0], nanVal[1], :])
				indices[idx][nanVal[0], nanVal[1]] = np.nanmedian(errSample[nanVal[0], nanVal[1], :]) # There has to be a better way to do this...


	indicesArr = np.zeros(np.concatenate([indices.shape, [2]]))
	indicesArr[..., 0] = indices
	indicesArr[..., 1] = errArr

	return indicesArr


def dynamicSpectrumHandler(cleanSpectrum):
	"""DS Handler
	
	Args:
	    cleanSpectrum (PreparedSpectrum): Processed dynamic spectrum for index calculation
	
	Returns:
	    np.ndarray-like: Indices array
	"""

	indicesArray = []
	beamCount = len(cleanSpectrum.beamList) - 1

	if cleanSpectrum.ios:
		beamCount = 1

	startEndVal = cleanSpectrum.processingOptions['powerSpectrum']['startEndVal']
	meanLengthSec = cleanSpectrum.processingOptions['rollingMeanLengthSeconds']
	performMultiproc = cleanSpectrum.processingOptions['multiprocess']

	# Dictionary lookups can be heavy. Cache our sampling values.
	samplingOpt = cleanSpectrum.resampleOptions
	optionsArr = [samplingOpt['resampleAxis'], samplingOpt['resamplePercentile'], samplingOpt['resampleSteps'], samplingOpt['resampleSqueeze']]

	if performMultiproc:
		cleanSpectrum.checkSegmentTime()
		multiProcFrac = cleanSpectrum.processingOptions['multiprocessCoreFrac']
		processes = int(min(mp.cpu_count() * multiProcFrac, (cleanSpectrum.sourceShape[0] / cleanSpectrum.segmentInfo['segmentSize']) + 1))
		#print(processes)
		mpPool = mp.Pool(processes = processes)
		retList = [mpPool.apply_async(__dSpecWorker, args = ([idx, beamSet, meanLengthSec, optionsArr, beamCount, startEndVal])) for idx, beamSet in cleanSpectrum.segmenter(includeIndex = True)]

		indicesArray = [[]] * len(retList)
		mpPool.close()
		mpPool.join()
		for asyncResult in retList:
			resultIdx, index, __ = asyncResult.get()
			#print(resultIdx, np.array(index).shape)
			indicesArray[resultIdx] = index

		indicesArray = np.array(indicesArray)
		print(type(indicesArray), type(indicesArray[0]), indicesArray.shape)
		indicesArray = indicesArray.astype(float)
		print(type(indicesArray), type(indicesArray[0]), type(indicesArray[0][0]), type(indicesArray[0][0][0]), indicesArray.shape)

	else:
		paramsPast = None
		for idx, beamSet in enumerate(cleanSpectrum.segmenter()):
			__, index, paramsPast =  __dSpecWorker(idx, beamSet, meanLengthSec, optionsArr, beamCount, startEndVal, paramsPast)
			indicesArray.append(index)

		indicesArray = np.array(indicesArray)

	return indicesArray

def __dSpecWorker(prefix, beamSet, meanLengthSec, optionsArr, beamCount, startEndVal, paramsPast = None):
	print("Starting worker {0} on beamSet of shape {1}".format(prefix, beamSet.shape))
	
	indexArr = []

	onSource = beamSet[..., 0].applySampling(optionsArr) #N_times x N_channel_samples
	print(onSource.shape)
	meanLengthTemp = int(meanLengthSec / onSource.timeStep)

	onPowerArr = np.zeros([onSource.shape[1]])
	offPowerArr = np.zeros([onSource.shape[1]])
	for offSourceIdx in range(1, beamCount + 1):
		#Move MP operations to me!
		offSource = beamSet[..., offSourceIdx].applySampling(optionsArr) #N_times x N_channel_samples

		offRollingMean = spys.convolve2d(offSource.reshape(offSource.shape[0], offSource.shape[1]), np.ones([meanLengthTemp, 1]) / meanLengthTemp, "same", "symm") # N_times x N_channel_samples

		for freqIdx in range(onSource.shape[1]):
			onPower, startIdx, endIdx, paramsPast = powerSpectrum.powerIntegral((onSource - offRollingMean)[:, freqIdx], startEndVal = startEndVal, paramsPast = paramsPast)
			offPower, __, __, __ = powerSpectrum.powerIntegral((offSource - offRollingMean)[:, freqIdx], [startIdx, endIdx], startEndVal = startEndVal, paramsPast = paramsPast)
			onPowerArr[freqIdx] = onPower
			offPowerArr[freqIdx] = offPower


		netPower = onPowerArr - offPowerArr
		correction = np.square(np.mean(onSource - offSource, axis = 0))

		index = np.sqrt(netPower / correction)
		indexArr.append(index)
#		print(index, beamSet.chunkDescription, beamSet.segmentInfo, onSource.timeStep, beamSet.processingOpt)

	indexArr = np.vstack(indexArr)
	logger.info("Finishing processing segment {0}. {1}".format(prefix, indexArr))
	
	return [prefix, indexArr, paramsPast]


def imageCubeHandler(imageCube):
	"""IC Handler
	
	Args:
	    imageCube (PreparedImageCube): Processed image cube for index calculation
	
	Returns:
	    list-like: Indices array
	"""
	indicesArray = []

	pixelCount = len(imageCube.pixels)
	print("Pixel count", pixelCount)
	startEndVal = imageCube.processingOptions['powerSpectrum']['startEndVal']

	# Dictionary lookups can be heavy. Cache options.
	samplingOpt = imageCube.resampleOptions
	optionsArr = [samplingOpt['resampleAxis'], samplingOpt['resamplePercentile'], samplingOpt['resampleSteps'], samplingOpt['resampleSqueeze']]
	paramsPast = None
	for segment in imageCube.segmenter():
		segment = segment.applySampling(optionsArr)
		print(segment.shape, 'segShape')
		sourcePower, __, __, paramsPast = powerSpectrum.powerIntegral(segment, paramsPast = paramsPast, startEndVal = startEndVal)

		correction = np.square(np.mean(segment))
		index = np.sqrt(sourcePower / correction)

		print(index, segment.chunkDescription)
		indicesArray.append(index)

	indicesArray = np.asarray(indicesArray).reshape(-1, pixelCount)[..., np.newaxis] # Newaxis to match shape of channel sampled beamformed data
	# Possible future change: don't only sample a single channel, match the approach of beamformed for several, also get a better way of iterating over pixels.

	return indicesArray
