"""Process data through the varaibility method, prepare datasets for analysis

Heavily based off the work of Morgan et al. MNRAS 473, 2965-2983
"""
import multiprocessing as mp
import variabilityMapping as varMap
import numpy as np
import scipy.signal as spys

from ..tools import dataTools

import matplotlib.pyplot as plt
import logging
logger = mp.log_to_stderr(logging.INFO)

def dynamicSpectrumHandler(cleanSpectrum):
	"""DS Handler
	
	Args:
	    cleanSpectrum (PreparedSpectrum): Processed dynamic spectrum for index calculation
	
	Returns:
	    np.ndarray-like: Indices array
	"""
	indicesArray = []
	beamCount = len(cleanSpectrum.beamList) - 1

	varDict = cleanSpectrum.processingOptions['variability']

	optionsArr = extractOptions(cleanSpectrum, varDict)

	samplingOpt = cleanSpectrum.resampleOptions
	resampleOptionsArr = [samplingOpt['resampleAxis'], samplingOpt['resamplePercentile'], samplingOpt['resampleSteps'], samplingOpt['resampleSqueeze']]


	meanLengthSec = cleanSpectrum.processingOptions['rollingMeanLengthSeconds']
	performMultiproc = cleanSpectrum.processingOptions['multiprocess']

	if performMultiproc:
		cleanSpectrum.checkSegmentTime()
		multiProcFrac = cleanSpectrum.processingOptions['multiprocessCoreFrac']
		processes = int(min(mp.cpu_count() * multiProcFrac, (cleanSpectrum.sourceShape[0] / cleanSpectrum.segmentInfo['segmentSize']) + 1))

		mpPool = mp.Pool(processes = processes)
		retList = [mpPool.apply_async(__dSpecWorker, args = ([idx, beamSet, meanLengthSec, optionsArr, resampleOptionsArr, beamCount])) for idx, beamSet in cleanSpectrum.segmenter(includeIndex = True)]
		
		indicesArray = [[]] * len(retList)
		
		mpPool.close()
		mpPool.join()

		for asyncResult in retList:
			resultIdx, index= asyncResult.get()
			indicesArray[resultIdx] = index

		indicesArray = np.array(indicesArray)
		indicesArray = indicesArray.astype(float)
	else:
		indicesArray = []
		for idx, beamSet in enumerate(cleanSpectrum.segmenter()):
			__, index = __dSpecWorker(idx, beamSet, meanLengthSec, optionsArr, resampleOptionsArr, beamCount)
			indicesArray.append(index)
		
		indicesArray = np.array(indicesArray)

	return indicesArray

def __dSpecWorker(prefix, beamSet, meanLengthSec, optionsArr, resampleOptionsArr, beamCount):
	"""Handle the DyanmicSpectrum processing; abstracted to allow for paralellisation.
	"""
	print("Starting worker {0}".format(prefix))
	indexArr = []
	indexErrArr = []

	onSource = beamSet[..., 0].applySampling(resampleOptionsArr)
	meanLengthTemp = int(meanLengthSec / onSource.timeStep)

	# Empty arrays for storing processed values
	scintFluxArr = np.zeros([onSource.shape[1]])
	scintErrArr = np.zeros([onSource.shape[1]])
	meanValArr = np.zeros([onSource.shape[1]])
	stdValArr = np.zeros([onSource.shape[1]])


	#Iterate on a per-beam level
	for offSourceIdx in range(1, beamCount + 1):
		#Look into the option of moving the threading to a per-beam level.
		# May be useful for the series object, as they can be quite slow to process
		offSource = beamSet[..., offSourceIdx].applySampling(resampleOptionsArr)

		# Get a smooth version of the rollnig mean to approximate the background noise
		offRollingMean = spys.convolve2d(offSource.reshape(offSource.shape[0], offSource.shape[1]), np.ones([meanLengthTemp, 1]) / meanLengthTemp, "same", "symm")


		# For every channel set being samples, pass the data to variabilityMapping
		# Subtract the smooth backgroundm pass the background variability as workVar to approximate off-source pixels
		for freqIdx in range(onSource.shape[1]):
			scintFlux, scintErr, __, __, meanVal, stdVal = varMap.variabilityResults((onSource - offRollingMean)[:, freqIdx], optionsArr, workVar = (offSource - offRollingMean)[:, freqIdx])
			scintFluxArr[freqIdx] = scintFlux
			scintErrArr[freqIdx] = scintErr
			meanValArr[freqIdx] = meanVal
			stdValArr[freqIdx] = stdVal

		# Determine error through stsndard propogation of errors.
		index = np.divide(scintFluxArr, meanValArr)
		fracErr = np.sqrt(np.square(np.divide(scintErrArr, scintFluxArr)) + np.square(np.divide(stdValArr, meanValArr)))
		indexError = np.multiply(fracErr, index)
		indexArr.append(index)
		indexErrArr.append(indexError)

	indexArr = np.vstack(indexArr)
	indexErrArr = np.vstack(indexErrArr)
	indexArr = np.stack([indexArr, indexErrArr], axis = -1)

	logger.info("Finishing processing segment {0}. {1}".format(prefix, indexArr))
	return [prefix, indexArr]

def imageCubeHandler(imageCube):
	"""IC Handler
	
	Args:
	    imageCube (PreparedImageCube): Processed image cube for index calculation
	
	Returns:
	    list-like: Indices array
	"""

	# It was found that multiprocessing this method slowed down the operations below time samples of ~ 100k and below due to the time needed for I/O and thread exchanges

	indicesArray = []

	varDict = imageCube.processingOptions['variability']

	optionsArr = extractOptions(imageCube, varDict)

	# On a per-frame level, process the images
	for segment in imageCube.segmenter(force_all = True):
		segment = segment.applySampling()

		scintFlux, scintErr, flaggedPixels, varChunk, __, __ = varMap.variabilityResults(segment, optionsArr)

		flaggedPixels[...] = False
		flaggedPixels[imageCube.pixels[:, 0], imageCube.pixels[:, 1]] = True

		scintFlux *= flaggedPixels
		scintErr *= flaggedPixels

		pixelMean = np.mean(segment, axis = 0)

		# Idx = sigma / I
		index = np.divide(scintFlux[flaggedPixels], pixelMean[flaggedPixels])


		# Error from considering propogation of errors, where the index error is from scintillating pixels and the system noise is generated from non-flagged pixels
		indexErr = np.multiply(np.sqrt(
			np.square(np.divide(scintErr[flaggedPixels], scintFlux[flaggedPixels])) + 
			np.square(np.divide(np.mean(varChunk[~flaggedPixels]), pixelMean[flaggedPixels]))),
			index)
		print(index, indexErr, segment.timeStep, segment.chunkDescription)
		indicesArray.append(np.vstack([index, indexErr]).T[:, np.newaxis, :])

	return np.array(indicesArray)

def extractOptions(obj, varDict):
	"""Options parser for the BW filter
	
	Args:
	    obj (ScintillatingObject): Input parent object
	    varDict (dict): FIltering options
	
	Returns:
	    list: Arranged options
	"""
	samplingF = 1. / obj.timeStep
	n_ips = varDict['n_ips'] * samplingF
	lowF = varDict['highPassFreq']
	highF = varDict['lowPassFreq']
	filterOrder = varDict['filterOrder']
	stdMul = varDict['stdMul']
	return [samplingF, highF, lowF, filterOrder, n_ips, stdMul]

def customStd(dataSet):
	"""Inter-hexile range standard deviation

	Args:
		dataSrt (np.ndarray): Imput data set to sample

	Returns:
		np.floatxx: Standard deviation derived from the interhexile range
	"""
	percentile16, percentile83 = np.percentile(dataSet, (100./6., 500./6.))

	return (percentile83 - percentile16) / 2.
