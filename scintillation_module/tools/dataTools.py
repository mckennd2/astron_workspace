"""Data processing functions that may be used in multiple locations.
"""
import numpy as np
from scipy.ndimage import label as ndLabel

def sourceMerger(dataArr, useSumElements = True):
	"""Given a boolean dataset, find grouped sources
	
	Args:
	    dataArr (np.ndarray (bool-like)): Input dataset, any non-0 value will be merged
	    sumElements (bool, optional): Whether or not to sum the contributions of dataset elements
	
	Returns:
		summedElements (np.ndarray, OPTIONAL): Sum of merged sources
	    boolArray (np.ndarray): List of sources and the labels
	    array (np.ndarray): Labels applied to orignal dataset
	"""
	array, sources = ndLabel(dataArr, np.ones(9, dtype = int).reshape(3,3))
	boolArray = labelIdentifier(array, sources)

	if useSumElements:
		summedElements = sumElements(boolArray, dataArr)

		return summedElements, boolArray, array

	return boolArray, array


def labelIdentifier(dataLabelArr, sources):
	"""Generate an n-dimensional array of each source
	
	Args:
	    dataLabelArr (np.ndarray): Array of labelled sources
	    sources (int): Output source count
	
	Returns:
	    boolArray (np.ndarray): An (n, m, sources)-dimensional array containing each of the sources on it's own axis.
	"""
	lim = np.max(sources)
	indices = np.arange(1, lim + 1)
	
	boolArray = np.equal(indices, dataLabelArr.reshape(dataLabelArr.shape[0], dataLabelArr.shape[1], 1))
	boolArray = np.transpose(boolArray, axes = (2, 0, 1))

	return boolArray

def sumElements(boolArray, dataArr):
	"""Sum up all the elements in a given grouping.
	
	Args:
	    boolArray (np.ndarray): Output of label_identifier
	    dataArr (np.ndarray): Input dataset
	
	Returns:
	   np.ndarray: Summed elements as defined by the boolean-logic of boolArray
	"""
	nulledArray = boolArray * dataArr
	print(nulledArray.shape, boolArray.shape, dataArr.shape)
	return np.sum(nulledArray, axis = (1,2))
