"""Helpful file for getting data locations of observations on CEP3

Attributes:
    obs3C147 (TYPE): Description
    obs3C48 (TYPE): Description
"""
### Temp file to act as a loc/group variable passer.

global obs3C48 
obs3C48 = {
	'april': {'imaging': {'fileLoc': "/data/scratch/mckenna/astron_workspace/3C48.h5", 'groupName': "/observations/2018-04-18T05:30:00.3-29a035faa2c34df6a67c04ec9d779b8e"}, 'beamformed': {'fileLoc': "/data/scratch/mckenna/workspace_2/astron_workspace/L649109_3C48_CS_148to167MHz_rfi_flagged.h5"}},
	'march': {'imaging': {'fileLoc': "/data/scratch/mckenna/astron_workspace/3C48.h5", 'groupName': "/observations/2018-03-22T07:10:00.3-3919f82"}, 'beamformed': {'fileLoc': "/data/scratch/mckenna/workspace_2/astron_workspace/L645445_3C48_CS_148to167MHz_rfi_flagged.h5"}}
}

global obs3C147
obs3C147 = {
	'june04': {'imaging': {'fileLoc': None}, 'beamformed': {'fileLoc': "/data/scratch/mckenna/workspace_2/astron_workspace/Dynspec_rebinned_L656426_SAP000_rfi_flagged.h5"}},
	'june22': {'imaging': {'fileLoc': "/data/scratch/mckenna/astron_workspace/3C147-segmentfull.h5", 'groupName': "/observations/2018-06-22-merged-0"}, 'beamformed': {'fileLoc': "/data/scratch/mckenna/workspace_2/astron_workspace/Dynspec_rebinned_L658652_SAP000_rfi_flagged.h5"}},
	'june28': {'imaging': {'fileLoc': "/data/scratch/mckenna/astron_workspace/3C147.h5", 'groupName': "/observations/2018-06-28T07:00:00.0-ffde751-trim/"}, 'beamformed': {'fileLoc': "/data/scratch/mckenna/workspace_2/astron_workspace/Dynspec_rebinned_L658658_SAP000_rfi_flagged.h5"}}
}


def returnVals(obs, key):
	"""Summary
	
	Args:
	    obs (TYPE): Description
	    key (TYPE): Description
	
	Returns:
	    TYPE: Description
	"""
	if obs == '3C48':
		dictVar = obs3C48
	elif obs == '3C147':
		dictVar = obs3C147
	else:
		print("Unknown Observation")
		return None, None, None

	if dictVar[key]['imaging']['fileLoc']:
		return dictVar[key]['imaging']['fileLoc'], dictVar[key]['imaging']['groupName'], dictVar[key]['beamformed']['fileLoc']

	return dictVar[key]['beamformed']['fileLoc']
