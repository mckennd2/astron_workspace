"""Default dictionaries for various processes carried out by the module.
"""

def dSpecCleaningOptions(sourceFile = "./unknownSource", channels = None):
	"""Options used in the RFI flagging / normalisation routine
	
	Args:
	    sourceFile (str, optional): Input file location
	    channels (None, optional): Description
	
	Returns:
	    dict: Default cleaning dictionary.
	"""

	outputLoc = "./" + sourceFile.split("/")[-1]
	outputLoc = outputLoc[:-3] + "_rfi_flagged.h5"
	channels = channels
	customDict = {
						'inputFile':  sourceFile,
						'outputFile': outputLoc, 
						'cloneStructure': True,
						'askIfExists': True,
						'overwriteByDefault': False,
						'mainBeamName': "DYNSPEC_000",

						'curvingSampleFrac': 0.05,
						'curvingSampleMax': 60000,
						'curvingSamples': 5,
						'curvingWindowLengthFrac': 0.1,

						'outputChannels': channels,

						'initPercentileSample': 66,
						'initPercentileMul': 1.1,
						'stdThresholdMul': 5.,
						'kernelSize': [4, 10],
						'kernelOrder': 3,
						'resamplePercentile': 33,
						'resamplePasses': 3,

						'debugPlot': False
					}

	cleaningDict = {
						'method': 'customFlagger',
						'customFlagger': customDict
					}

	return cleaningDict

def imagingScintillatingOptions():
	"""Options used to identify scintillating pixels in a given image cube.
	
	Returns:
	    dict: Default identification dictionary.
	"""
	scintillatingDict = {
						'sampleFrac': 0.2,
						'sampleMaxLen': 1000,
						'sampleLocationFrac': 0.4,
						'fracOfMax': 0.6
					}

	return scintillatingDict

def dSpecResampleOptions():
	"""Options used to resample dynamic spectra during processing.
	
	Returns:
	    dict: Default resampling dictionary for Dynamic Spectra
	"""
	resampleDict = {
						# ResampleAxis note: squeeze operations will remove axis, the fiirst squeeze option will ikely remove the polarisation axis (axis = 2) along with any other resampled axis that have been reduced to 1.
						'resampleAxis': [0, 1, 2],
						'resamplePercentile': [None, 33, None],
						'resampleSteps': [1, 10, 1], # To resample an entire dataset, set steps to None or a value less than 2.
						'resampleSqueeze': [False, False, True], 
						'timeStepAxis': 0
					}

	return resampleDict


def imagingResampleOptions():
	"""Options used to resample image cubes during processing.
	
	Returns:
	    dict: Default resampling dictionary for Image Cube
	"""
	resampleDict = {
						'resampleAxis': [None],
						'resamplePercentile': [None],
						'resampleSteps': [0],
						'resampleSqueeze': [False],
						'timeStepAxis': 0
					}

	return resampleDict

def dSpecIndexOptions():
	"""Options used while performing index calculations on Dynamic Spectra
	
	Returns:
	    dict: Default index options dictionary for Dynamic Spectra
	"""
	powerSpectrumOptions = {
						'errorBarSamples': 10,
						'startEndVal': [-0.7, None] # Log frequency integration region, default 0.2Hz, determine at runtime (3\% in log space)
					}

	variabilityOptions = {
						'n_ips': 1.5, #1.5s for high band, 2s for low band
						'stdMul': 7.,
						'lowPassFreq': 10.,
						'highPassFreq': 0.2,
						'filterOrder': 2
					}

	optionsDict = {
						'method': 'powerSpectrum',
						'rollingMeanLengthSeconds': 2.,
						'multiprocess': True,
						'multiprocessCoreFrac': 0.66,
						'powerSpectrum' : powerSpectrumOptions,
						'variability': variabilityOptions
					}


	return optionsDict

def imagingIndexOptions():
	"""Options used while performing index calculations on Image Cubes
	
	Returns:
	    dict: Default index options dictionary for Image Cubes
	"""
	powerSpectrumOptions = {
						'errorBarSamples': 10,
						'startEndVal': [-0.7, None] # Log frequency integration region, default 0.2Hz, determine at runtime (3\% in log space)
					}

	variabilityOptions = {
						'n_ips': 1.5, #1.5s for high band, 2s for low band
						'stdMul': 7.,
						'lowPassFreq': 10.,
						'highPassFreq': 0.2,
						'filterType': 'bandpass',
						'filterOrder': 2,

						'useNewFlags': True
					}

	optionsDict = {
						'method': 'powerSpectrum',
						'powerSpectrum' : powerSpectrumOptions,
						'variability': variabilityOptions
					}


	return optionsDict

def dSpecSegmentOptions():
	"""Options used to determine the size and overlap of Dynamic Spectrum segments taken for processing
	
	Returns:
	    dict: Default Dyanmic Spectrum segmentation options dictionary.
	"""
	optionsDict = {
						'segmentByTime': True, # If true, we will overwrtie segmentSize with the expected value based on segmentTimeBySeconds on object creation and self.segmenter() calls.
						'segmentByTimeSeconds': 600,
						'segmentSize': 60000,
						'segmentOverlapFraction': 0.2
					}

	return optionsDict

def imagingSegmentOptions():
	"""Options used to determine the size and overlap of Image Cube segments taken for processing
	
	Returns:
	    dict: Default Image Cube segmentation options dictionary.
	"""
	optionsDict = {
						'segmentByTime': True, # If true, we will overwrtie segmentSize with the expected value based on segmentTimeBySeconds on object creation and self.segmenter() calls.
						'segmentByTimeSeconds': 600,
						'segmentSize': 8192,
						'segmentOverlapFraction': 0.2
					}

	return optionsDict
	
def dSpecVisualisationOptions():
	"""Options used to determine the output of visualisation calls, via object.visualuse()
	
	Returns:
	    dict: Default beamformed data visualisation options
	"""
	powerDict = {
						'dataFmt': "bo", # Expect: colour, marker
						'plotByBeam': False,
						'beamFmt': 'o-',
						'labelName': "Beamformed Power",
						'alpha': 0.5,
						'size': 8
					}

	varDict = {
						'dataFmt': "co", # Expect: colour, marker
						'plotByBeam': False,
						'beamFmt': 'o-',
						'labelName': "Beamformed Variability",
						'alpha': 0.5,
						'size': 8
					}

	optionsDict = {
						'powerSpectrum': powerDict,
						'variability': varDict,
						'outputLoc': './results',
						'saveFigure': True,
						'saveData': True,
						'showFigure': False,
						'saveResolution': [2560,1440],
						'dpi': 100,
						'saveFormat': "png"
					}

	return optionsDict

def imagingVisualisationOptions():
	"""Options used to determine the output of visualisation calls, via object.visualuse()
	
	Returns:
	    dict: Default imaging data visualisation options
	"""
	powerDict = {
						'dataFmt': "ro", # Expect: colour, marker
						'plotByBeam': False,
						'beamFmt': 'o-',
						'labelName': "Imaging Power",
						'alpha': 0.2,
						'size': 4
					}

	varDict = {
						'dataFmt': "mo", # Expect: colour, marker
						'plotByBeam': False,
						'beamFmt': 'o-',
						'labelName': "Imaging Variability",
						'alpha': 0.2,
						'size': 4
					}

	optionsDict = {
						'powerSpectrum': powerDict,
						'variability': varDict,
						'outputLoc': './results',
						'saveFigure': True,
						'saveData': True,
						'showFigure': False,
						'saveResolution': [2560,1440],
						'dpi': 100,
						'saveFormat': "png"
					}

	return optionsDict