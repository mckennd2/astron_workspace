"""Useful tools when reading h5 files
"""
import numpy as np


def beamList(fileRef):
	"""Generate a list of beams wihin a given DynSpec v2 output h5 file.
	
	Args:
	    fileRef (h5py.File): File referencr to analyse
	
	Returns:
	    returnList (List): List of dynamic spectra data locations in the given file
	"""
	returnList = [group[0] + "/DATA" for group in fileRef.items() if ("DYNSPEC" in group[0])]
	returnList.sort(key=lambda f: int(filter(str.isdigit, f.encode("ASCII"))))
		
	return np.array(returnList)


def determineBeamType(fileRef):
	"""Analyse the stations used for observations to determine if we have off source beams available.
	
	Args:
	    fileRef (h5py.File): File reference to analyse
	
	Returns:
	    bool: Boolean of off source beam presence
	"""
	stations = fileRef.attrs["OBSERVATION_STATIONS_LIST"].tolist()

	return all([('CS' in stringVal) or ('RS' in stringVal) for stringVal in stations])
