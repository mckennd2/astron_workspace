"""Sundry tools
"""
import re
from os import listdir

def fileFlagger(dirVar, regexCheck):
	"""Find files that match a regex check
	
	Args:
	    dirVar (string): Folder to search
	    regexCheck (string): Uncompiled Regex
	
	Returns:
	    removeList (List): Flagged Files
	"""
	print(regexCheck)
	check = re.compile(regexCheck)
	filesInDir = listdir(dirVar)
	listVar = filter(check.match, filesInDir)
	removeList = [dirVar + name for name in listVar]
	return removeList 
	