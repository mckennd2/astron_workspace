
# This file was generated by sfood-graph.

strict digraph "dependencies" {
    graph [
        rankdir = "LR",
        size = "16,16",
        fontsize = "16",
        fontname = "Helvetica",
        ]

       node [
           fontsize=16
           shape=ellipse
//           style=filled
//           shape=box
       ];

//     node [
//         fontsize=7
//       style=ellipse
//     ];


"Determine Mean Values"  [style=filled, color = ".26, .51, .96"];
"Variability Calculation\n(Standard Deviation)"  [style=filled, color = ".26, .51, .96"];
"Analysis" [style= filled, color = ".7, .3, 1."]
subgraph {
rank = same; "Determine Mean Values"; "Variability Calculation\n(Standard Deviation)";
}
"Input Data" -> "Determine Mean Values";
"Input Data" -> "Butterworth Bandpass\n(0.1, 10 Hz)";
"Butterworth Bandpass\n(0.1, 10 Hz)" -> "Variability Calculation\n(Standard Deviation)";
"Variability Calculation\n(Standard Deviation)" -> "Analysis";
"Determine Mean Values" -> "Analysis";
}
