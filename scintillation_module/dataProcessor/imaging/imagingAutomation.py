"""Automated setup for the imaging of radio MS observations with WSClean.

Can be run through CLI or as a module import as importName.main(['-flag', 'value', ...]), through a manual process if no flags are provided, or have a dictionary returned if called as import.main([], getVals = True)

Attributes:
    parser (ArgumentParser): Parser constructor

"""
from __future__ import print_function

from matplotlib import use as pltUse
pltUse("Agg")

from sys import stdout # print \r only works if this is specifically imported in this script? What?
import sys
import subprocess
import time
import h5py
import os.path
import md5
import casacore.tables
import numpy as np

import astropy.time

from os import remove
from argparse import ArgumentParser

import matplotlib.animation as manimation
import matplotlib.pyplot as plt


from scintillation_module.tools import progressBar
from scintillation_module.tools import generalTools
from scintillation_module.tools import h5WriteTools as h5Writer

parser = ArgumentParser("Process a MS observation through WSClean and output the fits files to a h5 database.")
parser.add_argument('-i', help="Input MS Folder", default = "./", dest = 'input_ms')
parser.add_argument('-outputs_prefix', help = "Default WSCLEAN .fits prefix", default = "./wsclean", dest = 'wsc_prefix')
parser.add_argument('-od_cube', help = "Cube output directory", default = "./", dest = "cube_od")
parser.add_argument('-res', help = "WSClean scale parameter", default = "40as", dest = 'res')
parser.add_argument('-im_size', type = int, help = "Size of the output image in pixels", default = 100, dest = 'im_res')
parser.add_argument('-uvmax', help = "WSClean maxuv-l parameter", default = "300", dest = 'uvmax')
parser.add_argument('-wsc_param', help = "Other parameters to pass to WSClean command", default = "-j 24 -mem 30 -no-update-model-required -weight briggs 0 -pol I -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -make-psf -multiscale", dest = 'wsc_param')
parser.add_argument('-int_start', type = int, help = "WSClean starting interval", default = 0, dest = 'int_start')
parser.add_argument('-int_end', type = int, help = "WSClean end interval", default = 100, dest = 'int_end')
parser.add_argument('-int_out', type = float, help = "WSClean output interval count (fraction of segments)", default = 1., dest = 'int_out')
parser.add_argument('-segment_size', type = int, help = "Size of jobs supplied to WSClean in timesteps.", default = None, dest = 'segSize')
parser.add_argument('-film', type = bool, help = "Convert image cube to a mp4 movie via matplotlib? (REQUIRES FFMPEG)", default = False, dest = 'film')
parser.add_argument('-sigma', type = float, help = "Standard deviations above the median value used to set the minimum floor for the output movie", default = 7., dest = 'cube_std')
parser.add_argument('-wsc_extras_cleanup', type = bool, help = "Remove non-image WSClean generated .FITS (psf, dirty, etc.) after video creation?", default = True, dest = 'wsc_clean_lim')
parser.add_argument('-wsc_all_cleanup', type = bool, help = "Remove all WSClean generated .FITS files after video creation?", default = True, dest = 'wsc_clean_all')
parser.add_argument('-ffmpeg_loc', type = str, help = "Location of your FFMPeg executable if not accessible in your path and you wish to make a movie", default = u'/home/mckenna/.imageio/ffmpeg/ffmpeg-linux64-v3.3.1', dest = 'ffmpegLoc')
parser.add_argument('-ts', type = float, help = "Time step between image files", default = 0.083886, dest = 'timeStep')
parser.add_argument('-target', type = str, help = "Name of target (will check if output file exists for target)", default = "undefined_target", dest = 'sky_target')
parser.add_argument('-col', type = str, help = "Column to read from in MS set when imaging", default = "DATA", dest = 'col')
parser.add_argument('-skip', type = int, help = "Skip to the given index to start the imaging process (useful for resuming calculaions)", default = 0, dest = 'skip')

def main(argv, getVals = False):
	"""A monster used for everything imaging.
	
	Args:
	    argv (List): Input list of flags / values. CLI or module-like (through stringed flags/values) supported.
	    getVals (bool, optional): if set, return the dictionary of flags and default parameters.
	
	Returns:
	    string: Output file location
	    string: Dataset name within output file.
	
	Raises:
	    RuntimeError: Raised if the ffmpegWriter is not defined for film creation.
	"""

	# CLI parser
	args = parser.parse_args(argv)

	#Initialisation for step skip modification (allowing for cancelled operations to be continued with the same observation hash)
	firstPass = True

	# Return the CLI parameters if requested
	if getVals:
		return args.__dict__
	
	# If we got nothing, assume we want some form of contruction (and CLI assumed)
	if not argv:
		print("You didnt pass any arguments? Here are my default values.")
		print(args.__dict__)
		inputVar = raw_input("Manually set arguments? y/[n]") or 'n'
		if inputVar.lower() == 'y':
			for key in args.__dict__:
				value = args.__dict__[key]
				args.__dict__[key] = type(value)(raw_input("{0}: {1}, {2}".format(key, value, type(value))) or value)
		else: 
			return None

	#FFMPeG initialisation steps if we are generting source videos
	if args.film:
		try:
			ffmpegWriter = manimation.writers['ffmpeg']
		except (RuntimeError, KeyError):
				print("FFMPeg not found in path, trying -ffmpeg_loc flag, {0}".format(args.ffmpegLoc))
				plt.rcParams['animation.ffmpeg_path'] = args.ffmpegLoc
				ffmpegWriter = manimation.writers['ffmpeg']


	# Save a copy of the unsanitised prefix
	cleanPrefix = args.wsc_prefix

	# Grab useful information from the measurement set
	casatable = casacore.tables.table(args.input_ms)
	timeStep = casacore.tables.tablecolumn(casatable, "INTERVAL").getcol(0, 1)[0]
	startTime, endTime = casatable.OBSERVATION.TIME_RANGE.getcol()[0] / (60. * 60. * 24.) # Why does nobody except LOFAR use Modified Julian Date in seconds???
	skyTarget = casatable.OBSERVATION.LOFAR_TARGET.getcol()['array'][0]
	#casatableDataLength = casatable.DATA.getdminfo()['SPEC']['CubeShape'][2] / # Find a way to extract the amount of timesteps in a measurement set for full imaging automation

	# Extract observation time information
	obsStartTime = astropy.time.Time(startTime, format = 'mjd')
	obsStartTime.format = 'isot'
	obsStartTime = str(obsStartTime)

	obsEndTime = astropy.time.Time(endTime, format = 'mjd')
	obsEndTime.format = 'isot'
	obsEndTime = str(obsEndTime)

	# Generate an output file name
	outputFileLocPrefix = args.sky_target + "-" + obsStartTime[:10] # Include observation date in filename
	outputFileLoc = outputFileLocPrefix + ".h5"
	imagingDir = args.wsc_prefix[:(len("/".join(args.wsc_prefix.split("/")[:-1]))) + 1]


	# Sanitise the imaging directory
	if imagingDir == '':
		imagingDir = "./"
		filePrefix = args.wsc_prefix
	else:
		filePrefix = args.wsc_prefix.split("/")[-1]

	# Check if we might be overwriting existing FITS files in the current folder.
	# We will not overwrtie them, so append an offset to the provided prefix
	currFolder = generalTools.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))
	offsetNum = 1
	idxVar = None
	while currFolder:
		args.wsc_prefix = args.wsc_prefix[:idxVar] + '-' + str(offsetNum)
		filePrefix = filePrefix[:idxVar] + '-' + str(offsetNum)
		idxVar = (len(str(offsetNum)) + 1) * -1
		offsetNum += 1
		currFolder = generalTools.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))
	
	if offsetNum != 1:
		print("We suspect there may be imaging files with the same prefix in your current path; we are adding a '-{0}' suffix to your given name.".format(offsetNum))

	# Generate the CLI command to get it's hash
	initHashReference = "wsclean -maxuv-l {0} -scale {1} -interval {2} {3} -intervals-out {4} -name {5} {6} -size {7} {7} -data-column {8} {9}".format( args.uvmax, args.res, args.int_start, args.int_end, args.int_out, cleanPrefix, args.wsc_param, args.im_res, args.col, args.input_ms)
	culledWSC = initHashReference.replace(' ', '')
	headCmdHash = md5.new(initHashReference).hexdigest()[:7]
	print("We are using command {0} with md5 hash {1}".format(initHashReference, headCmdHash[:7]))


	# Handle the skip case
	segmentSize = args.segSize
	if args.skip != 0:
		segments = (args.int_end - args.int_start) / segmentSize + 1
		segStart = segments - ((args.int_end - args.skip) / segmentSize) - 1
	else:
		segStart = 0
		segments = (args.int_end - args.int_start) / segmentSize + 1


	# Start the imaging process
	for segmentIdx in range(segStart, segments):

		# Generate the start/end indicies of the current segment
		startIdx = args.int_start + segmentIdx * segmentSize
		endIdx = args.int_start + (segmentIdx + 1) * segmentSize

		# And their non-offset equivilents
		rawStartIdx = (segStart + segmentIdx) * segmentSize
		rawEndIdx = (segStart + segmentIdx + 1) * segmentSize

		# Generate the WSClean command
		wscleanCmd = "wsclean -maxuv-l {0} -scale {1} -interval {2} {3} -intervals-out {4} -name {5} {6} -size {7} {7} -data-column {8} {9}".format( args.uvmax, args.res, startIdx, endIdx, int(args.int_out * segmentSize), args.wsc_prefix, args.wsc_param, args.im_res, args.col, args.input_ms)

		# Check if we have run the operation previusly, skip if we have
		skipClean = False
		if os.path.isfile(outputFileLoc):
			with h5py.File(outputFileLoc) as testFile:
				for key, val in testFile["command_reference"].attrs.items():
					if culledWSC in val.replace(' ', ''):
						skipClean = True
						hashVal = key
				if skipClean:
					for key in testFile["observations"].keys():
						if hashVal in key:
							datasetLoc = "observations/{0}/image_data".format(key)



		if not skipClean:

			# BUG: Resanitisation is needed, determine why.
			if imagingDir == '':
				imagingDir = "./"
				filePrefix = args.wsc_prefix
			else:
				filePrefix = args.wsc_prefix.split("/")[-1]

			# Print / execute the current command	
			print("Begining WSClean command: {0}".format(wscleanCmd))
			wsproc = progressBar.execute(wscleanCmd)
	
			# Generate a progress bar of the segment completion
			progressBar.handle(wsproc, "=== IMAGING TABLE ===", int(args.int_out * segmentSize))
	
			while wsproc.poll() is None:
				continue

			# Handle some exit codes with helpful information
			exitCode = wsproc.poll()

			print("WSClean exit by signal {0}".format(exitCode))
			if exitCode == 127:
				raise RuntimeError("You didn't load WSClean.")

			# TODO: rerun command with pipe changed to output console
			if exitCode == 255:
				raise RuntimeError("There was an issue with one of your passed flags.")

			# Sort the generated files
			cubeFiles = generalTools.fileFlagger(imagingDir, "{0}-t.*image\.fits".format(filePrefix))
			cubeFiles.sort(key=lambda f: int(filter(str.isdigit, f)))

			# On first pass, generate the processed file structure
			if segmentIdx == 0 or (firstPass and args.skip != 0):
				firstPass = False
				attrsKeys = ['segmentSize', 'dataCount', 'timeStep', 'init_time', 'end_time', 'target']
				attrsVals = [segmentSize, args.int_end - args.int_start, timeStep, obsStartTime, obsEndTime, skyTarget]
				attrs = dict(zip(attrsKeys, attrsVals))
				datasetLoc = h5Writer.initialiseObservation(outputFileLoc, cubeFiles[0], [culledWSC, headCmdHash], attrs)

			# Write the data to disk
			h5Writer.processChunk(outputFileLoc, cubeFiles, datasetLoc, [rawStartIdx, rawEndIdx], [startIdx, endIdx])
			
			# This is a horrible way of keeping the raw FITS files if needed. Improve.
			currFolder = generalTools.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))
			idxVar = None
			while currFolder and not args.wsc_clean_all:
				args.wsc_prefix = args.wsc_prefix[:idxVar] + '-' + str(offsetNum)
				filePrefix = filePrefix[:idxVar] + '-' + str(offsetNum)
				idxVar = (len(str(offsetNum)) + 1) * -1
				offsetNum += 1
				currFolder = generalTools.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))
				print("As you chose to keep some of the fits files, we are adding a '-{0}' suffix to your given name for the {1} segment.".format(offsetNum, segmentIdx))


		else:
			print("Command detected in command history, attempting to skip cleaning process. If an error occurs, remove the command reference and processed data sets from your h5 file.")


	# After processing, if needed generate the output video.
	if args.film:
		outputName = args.cube_od + args.sky_target + "-" + datasetLoc.split("/")[1]
		print("Converting Image Cube to Movie: {0}.mp4, {1} frames taking minimum of {2} std above the median value.".format(outputName, args.int_out, args.cube_std))
		makeVideo(ffmpegWriter, args.cube_od + outputFileLoc, datasetLoc, args.cube_std, outputName, fpsV = 1. / timeStep, startIdx = args.int_start, title = "{0}, $\sigma_{{min}}={1}$, {2}, {3}".format(args.sky_target, args.cube_std, args.res, "-".join(datasetLoc.split("-")[1:]).split("/")[-2]))

	# Cleanup files as needed
	if args.wsc_clean_lim or args.wsc_clean_all:
		print("Begining Cleanup proces")
		print("Limited Cleaning: {0}, Full Cleaning: {1}".format(str(args.wsc_clean_lim), str(args.wsc_clean_all)))

		if args.wsc_clean_lim or args.wsc_clean_all:
			if imagingDir == "":
				imagingDir = "./"
			if args.wsc_clean_all:
				removeList = generalTools.fileFlagger(imagingDir, "{0}-t.*\.fits".format(filePrefix))

				print("Removing all WSClean generated .FITS files: {0}".format(removeList))

				__ = [remove(fileVar) for fileVar in removeList] 
			else:
				removeList = generalTools.fileFlagger(imagingDir, "{0}-t.*(?<!image)\.fits".format(filePrefix))
				
				print("Removing extra WSClean generated .FITS files: {0}".format(removeList))

				__ = [remove(fileVar) for fileVar in removeList]

		print("Cleanup Finished")
	print("Exiting")
	return outputFileLoc, datasetLoc

def makeVideo(ffmpegWriter, imageCubeLoc, datasetName, sigma, outputName, spectralChannel = 0, fpsV = 12.5, dpi = 240, title = "Scintillation", author = "ASTRON", comment = "Scintillating.", startIdx = 0):
	"""Generate a video based on the data within a h5 dataset.
	
	Args:
	    ffmpegWriter (FFMPeGWriter): Pre-generated matplotlib ffmpeg output writer.
	    imageCubeLoc (string): h5 File location
	    datasetName (string): h5 Dataset location in file
	    sigma (float): Threshold for noise floor when imaging.
	    outputName (string): Output video name
	    spectralChannel (int, optional): Spectral channel in dataset to analyse.
	    fpsV (float, optional): Output framerate (if below 5fps this will be modified to allow for playback, most video players cannot handle extremely low framerates)
	    dpi (int, optional): DPI of the output video.
	    title (str, optional): Plot title
	    author (str, optional): Video 'autor' for metadata
	    comment (str, optional): Video 'comment' for metadata
	    startIdx (int, optional): Timestep to begin rendering video at.
	"""
	with h5py.File(imageCubeLoc, 'r') as refFile:

		# Construction
		imageCube = refFile[datasetName]
		metadataV = dict(title = title, artist = author, comment = comment)
		writer = ffmpegWriter(fps = fpsV, metadata = metadataV)
		fig = plt.figure()
		
		# Raw data processed
		dataMed = np.median(imageCube)
		dataStd = np.std(imageCube)
		dataMax = np.max(imageCube)

		frames = imageCube[0].shape[0]
		currentPixelIdx = 0
		startTime = time.time()

		with writer.saving(fig, outputName + ".mp4", dpi):
			for idx, frame in enumerate(imageCube[spectralChannel, :]):

				# Update the title every frame
				plt.title("{0}\nframe {1}".format(title, idx + startIdx))

				# Geneerate axis, etc on the first frame then only update data for subsequent frames
				if idx == 0:
					imVar = plt.imshow(frame, interpolation = 'nearest',  vmax = dataMax, vmin = dataMed + sigma * dataStd, cmap = 'gist_gray')
					plt.colorbar()
				else:
					imVar.set_data(frame)

				# Grab the frame
				writer.grab_frame()
			
				# Uptdate the progress bar.
				width = int(subprocess.check_output(['stty', 'size']).split()[1])
				currentPixelIdx = progressBar.handleBar(startTime, float(idx) / float(frames), width, currentPixelIdx)




# If called via CLI, pass along args.
if __name__ == '__main__':
		main(sys.argv[1:])
