
# This file was generated by sfood-graph.

strict digraph "dependencies" {
    graph [
        rankdir = "LR",
        size = "16,16",
        fontsize = "16",
        fontname = "Roboto",
        ]

       node [
           fontsize=16
           shape=ellipse
//           style=filled
//           shape=box
       ];

//     node [
//         fontsize=7
//       style=ellipse
//     ];


"Determine Mean Intensity"  [style=filled, color = ".26, .51, .96"];
"Integrate Over\nPower Spectrum"  [style=filled, color = ".26, .51, .96"];
"Analysis" [style= filled, color = ".7, .3, 1."]


subgraph {
rank = same; "Analysis"; "Integrate Over\nPower Spectrum";
}

"Input Data" -> "Sampling and Cleaning";
"Sampling and Cleaning" -> "Power Spectrum";
"Power Spectrum" -> "Sigmoid Fit\n(Identify Fresnel Knee / Noise Floor)";
"Sigmoid Fit\n(Identify Fresnel Knee / Noise Floor)" -> "Integrate Over\nPower Spectrum";
"Sampling and Cleaning" -> "Determine Mean Intensity";
"Determine Mean Intensity" -> "Analysis";
"Integrate Over\nPower Spectrum" -> "Analysis";
}
