import scintillation_module.dataProcessor.classHandler as ch
import scintillation_module.tools.defaultParams as dp
import numpy as np

resultsidx = []

for obs, date in [['3C48', 'april'], ['3C48', 'march'], ['3C147', 'june22'], ['3C147', 'june28'], ['3C147', 'june04']]:
    if date != 'june04':
        imgloc, imggroup, bfloc = dp.returnVals(obs, date)
    else:
        bfloc = dp.returnVals(obs, date)
        imgloc = False
    if imgloc:
        twinvar = ch.createAnalysisTwin(bfloc, imgloc, imggroup, skip_image_analysis = False, skip_ds_cleaning = True)
        break
        resultsidx.append(twinvar.determineIndex())
    else:
        dsobj = ch.createDynamicSpectrumObject(source_file=bfloc, autoClean = False)
        resultsidx.append(dsobj.determineIndex())

res = np.load("./massresults.npy")
twinvar.results = res[0]
twinvar.visualise()

