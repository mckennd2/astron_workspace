#!/bin/python

import module_rework.imaging.processing.imaging_automation as ia 
'''
step_size = 22
size = 10800
for i in range(int(size / step_size)):
	stepSize = step_size
	endStep = (i + 1) * stepSize
	startStep = i * stepSize
	print("Starting Observation 3C48 segment {0} to {1} (of {2})".format(startStep, endStep, size))
	locs = ia.main(['-target', '3C48', '-int_out', str(stepSize), '-sigma', '3', '-int_end', str(endStep), '-int_start', str(startStep), '-uvmax', '100000000', '-res', '35arcsec', '-wsc_param', '-j 24 -mem 50 -no-update-model-required -weight briggs 0 -pol I -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -make-psf -multiscale', '-i', '../imaging/uv-16june/concat_100sb.MS/', '-col', 'CORRECTED_DATA'])
'''


#locs = ia.main(['-target', '3C147', '-segment_size', '20', '-int_out', '1.', '-sigma', '3', '-int_end', '21', '-int_start', '0', '-uvmax', '450', '-res', '25arcsec', '-wsc_param', '-j 24 -mem 50 -no-update-model-required -weight briggs 0 -pol I -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -multiscale', '-i', '../imaging/3C147-28thjune/observation_avg.MS/', '-col', 'CORRECTED_DATA', '-film', 'True'])
locs = ia.main(['-target', '3C48', '-segment_size', '1000', '-int_out', '1.', '-sigma', '3', '-int_end', '64373', '-int_start', '0', '-uvmax', '350', '-res', '25arcsec', '-wsc_param', '-j 24 -mem 50 -no-update-model-required -weight briggs 0 -pol I -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -multiscale', '-outputs_prefix', '/data001/scratch/mckenna/imaging/wsclean-march-', '-i', '/data001/scratch/mckenna/imaging/observation_100sb.MS/', '-col', 'CORRECTED_DATA'])


'''



step_size = 200
size = 472200
for i in range(int(size / step_size)):
	stepSize = step_size
	endStep = (i + 1) * stepSize
	startStep = i * stepSize

	print("Starting Observation 3C147 segment {0} to {1} (of {2})".format(startStep, endStep, size))
	locs = ia.main(['-target', '3C147-avg600test', '-int_out', str(stepSize), '-sigma', '3', '-int_end', str(endStep), '-int_start', str(startStep), '-uvmax', '100000000', '-res', '20arcsec', '-wsc_param', '-j 24 -mem 50 -no-update-model-required -weight briggs 0 -pol I -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -make-psf -multiscale', '-i', '../imaging/uv-22june-full/observation_600step_cal.MS/', '-col', 'DATA'])

'''
