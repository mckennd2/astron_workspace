import matplotlib
matplotlib.use('Agg')

import scintillation_module.dataProcessor.classHandler as ch; import scintillation_module.tools.defaultParams as dp
import numpy as np

#sourcesList = [['3C48', 'april'], ['3C48', 'march'], ['3C147', 'june22'], ['3C147', 'june28']]
#sourcesList = [['3C48', 'march'], ['3C147', 'june22'], ['3C147', 'june28']]
#sourcesList = [['3C48', 'march'], ['3C48', 'april']]
sourcesList = [['3C147', 'june28']]

for target, date in sourcesList:
   imgloc, imggroup, h5loc = dp.returnVals(target, date)
   twinobj = ch.createAnalysisTwin(image_group_name=imggroup, source_image_cube=imgloc, source_dynamic_spectrum=h5loc)

   twinobj.determineIndex()
   np.save(target+date+"twinresults.npy", twinobj.results)   
#   try:
   twinobj.visualise(freqIdx = 'all'); twinobj.visualise(medianVals = [True, True], freqIdx = 'all')
   twinobj.visualise(); twinobj.visualise(medianVals = [True, True])
   twinobj.visualise(freqIdx = 'all', medianVals = [True, True], errorBars = False); twinobj.visualise(medianVals = [True, True], errorBars = False)
#   except:
#	print('viserr')
