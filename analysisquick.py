import numpy as np
import module_cleanup.dynspec.tools.dynamicSpectrum as dS
import matplotlib.pyplot as plt
from astropy.time import Time
import h5py
import matplotlib.pyplot as plt
reload(dS)
#run -i ./quick.py

def runme(currIdx, offon = True, segsize = 15000, sourceName = "3C147", methodName = 'power'):
	offsourcen = 6

	#294, 212	

	#fileLocStr = "/data001/scratch/fallows/IPS_CME_20170910/2017-09-12/3C147/L609{:03d}_3C147_CS_110to189MHz.h5".format(currIdx)
#	if sourceName == "3C196":
#		fileLocStr = "/data001/scratch/fallows/IPS_CME_20170910/2017-09-11/3C196/L609{:03d}_3C196_CS_110to189MHz.h5".format(currIdx)
#	if sourceName == "3C186":
#		fileLocStr = "/data001/scratch/fallows/IPS_CME_20170910/2017-09-11/3C186/L609{:03d}_3C186_CS_110to189MHz.h5".format(currIdx)
#	if sourceName == "3C159-2":
#		fileLocStr = "/data001/scratch/fallows/IPS_CME_20170910/2017-09-12/3C159/L609{:03d}_3C159_CS_110to189MHz.h5".format(currIdx)
#	if sourceName == "3C159":
#		fileLocStr = "/data001/scratch/fallows/IPS_CME_20170910/2017-09-11/3C159/L609{:03d}_3C159_CS_110to189MHz.h5".format(currIdx)
	if sourceName == "3C147":
		fileLocStr = "/media/chrx/16a416df-eb7b-42cc-be13-400117b8d5c1/work/Dynspec_rebinned_L658652_SAP000.h5"
	elif sourceName == "flagged":
		fileLocStr = "/media/chrx/16a416df-eb7b-42cc-be13-400117b8d5c1/work/Dynspec_rebinned_L658652_SAP000_rfi_flagged.h5"
	elif sourceName == "3C48-0":
		fileLocStr = "/media/chrx/16a416df-eb7b-42cc-be13-400117b8d5c1/work/L645445_3C48_CS_148to167MHz.h5"
	elif sourceName == "3C48-1":
		fileLocStr = "/media/chrx/16a416df-eb7b-42cc-be13-400117b8d5c1/work/L649109_3C48_CS_148to167MHz.h5"
	elif sourceName == "3C48-2":
		fileLocStr = "/media/chrx/16a416df-eb7b-42cc-be13-400117b8d5c1/work/Dynspec_rebinned_L656426_SAP000.h5"

	#fileLocStr = './L609168_3C196_CS_110to189MHz_rfi_processed.h5'
	#fileLocStr = "/data001/scratch/fallows/IPS_3C48_2018/2018-04-18/3C48/L649109_3C48_CS_148to167MHz.h5"

	refFile = h5py.File(fileLocStr, 'r')
	tS = refFile.attrs["SAMPLING_TIME"] / 1e6
	print(refFile["DYNSPEC_000/DATA"].shape)
	obstimeStr = refFile.attrs['OBSERVATION_START_UTC'][:-9]
	obsendStr = refFile.attrs['OBSERVATION_END_UTC'][:-9]

	print(obstimeStr)

	#deltaT = (obsendStr - obstimeStr).value * 24

	#obsTime = obstimeStr + np.linspace(0, deltaT, steps) * u.hour
	
	#sourceName = refFile.attrs['TARGET']

	#elongarr = np.array(solarElongation(sourceName, obsTime))

	cleanspec = dS.CleanSpectrum(fileLoc = fileLocStr)
	#cleanspec = cleanspec.clean()
	cleanspec.segment[2] = segsize
	cleanspec.offsetBeam = offon

	testarr = cleanspec.determine_index([methodName, True,[True, 0, 2, 'mean'], 50.])
	#idxes = np.array(testarr[0]).reshape(-1, offsourcen)
	#arr.append(idxes)
	#start.append(obstimeStr)
	#end.append(obsendStr)
	#plt.plot(idxes)
	#plt.show()
	return testarr, obstimeStr, obsendStr


def quickplot(y1, y2):
	x1 = np.linspace(0, 100, y1.shape[0] + 1)[1:-1]
	x2 = np.linspace(0, 100, y2.shape[0])

	plt.errorbar(x1, y1[1:, 0], yerr = y1[1:, 1], fmt = 'ro')
	plt.plot(x2, y2)

	plt.show()
