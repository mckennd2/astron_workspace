import numpy as np 
import astropy.units as u 
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, get_sun, GCRS


biasterp = EarthLocation(lat=52.8897*u.deg, lon=6.7419*u.deg, height=13*u.m)   

def altAzObj(targetName, startTime, endHours, spacing, location = [52.8897, 6.7419, 13]):
	targetobj = SkyCoord.from_name(targetName)
	location = EarthLocation(lat=location[0] * u.deg, lon= location[1] * u.deg, height = location[2] * u.m)

	timeSet = Time(startTime) + np.linspace(0, endHours, spacing) * u.hour

	frame = AltAz(obstime = timeSet, location = location)
	return targetobj.transform_to(frame), timeSet


def solarElongation(targetName, obstime):
	skyObj = SkyCoord.from_name(targetName, GCRS)
	sun = get_sun(obstime)

	position_ang = abs(sun.position_angle(skyObj))

	print(position_ang)

	elong = abs(np.sin(position_ang))
	print(elong)
	approx = 0.06 * 1.9 * np.power(elong, -1.6)


	return approx