import matplotlib
matplotlib.use('Agg')

import scintillation_module.dataProcessor.classHandler as ch; import scintillation_module.tools.defaultDicts as dd; import os; tempDict = dd.dSpecCleaningOptions(); currWorking = "/data001/scratch/fallows/IPS_CME_20170910/2017-09-11/3C159/"; outputDirLoc = "/data001/scratch/mckenna/IPS_proc/"; debug = True;
import numpy as np

filePrefixes = ["_3C196_", "_3C147_", "_3C186_", "_3C159_"]
#filePrefixes = ["_3C159_", "_3C186_"]

for filePrefix in filePrefixes:
	fileList = [outputDirLoc + fileVar for fileVar in os.listdir(outputDirLoc) if (filePrefix in fileVar) and (not "L609548" in fileVar)];
	chObj = ch.createSpectralSeries(fileList)
	for method in ['powerSpectrum', "variability"]:
		chObj.processingOptions['method'] = method
		chObj.determineIndex()
		np.save(filePrefix+method+"seriesresults.npy", chObj.indices)
		chObj.visualise(freqIdx = 'all'); chObj.visualise();
		chObj.visualise(freqIdx = 'all', medianVals = [True, True]); chObj.visualise(medianVals = [True, True])
		chObj.visualise(freqIdx = 'all', medianVals = [True, True], errorBars = False); chObj.visualise(medianVals = [True, True], errorBars = False)
