#!/bin/python

import module_rework.imaging.processing.imaging_automation as ia 


'''
step_size = 400
size = 64373
for i in range(int(size / step_size)):
	stepSize = step_size
	endStep = (i + 1) * stepSize
	startStep = i * stepSize

	print("Starting Observation 3C147 segment {0} to {1} (of {2})".format(startStep, endStep, size))
	locs = ia.main(['-target', '3C48', '-int_out', str(stepSize), '-sigma', '3', '-int_end', str(endStep), '-int_start', str(startStep), '-uvmax', '100000000', '-res', '20arcsec', '-wsc_param', '-j 24 -mem 50 -no-update-model-required -weight briggs 0 -pol I -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -make-psf -multiscale', '-i', '../imaging/uv-22june-full/observation_600step_cal.MS/', '-col', 'DATA'])
'''

size = 64373
segsize = 400
locs = ia.main(['-target', '3C48', '-segment_size', str(segsize), '-int_out', '1.0', '-sigma', '3', '-int_end', str(size), '-int_start', '64000', '-uvmax',  '450', '-res', '40arcsec', '-wsc_param', '-j 24 -mem 50 -no-update-model-required -weight briggs 0 -pol I -auto-threshold 0.1mJy -auto-mask 7 -mgain 0.8 -niter 10000 -fit-beam -make-psf -multiscale', '-i', '../imaging/3C48-april/observation_raw.MS', '-col', 'CORRECTED_DATA', '-ts', '0.671089'])
