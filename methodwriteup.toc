\contentsline {section}{\numberline {1}Methodology}{3}
\contentsline {subsection}{\numberline {1.1}The Variability Method}{3}
\contentsline {subsubsection}{\numberline {1.1.1}Imaging Process}{3}
\contentsline {subsubsection}{\numberline {1.1.2}Beamformed Adaptation}{4}
\contentsline {subsection}{\numberline {1.2}The Power Spectrum Method}{4}
\contentsline {subsubsection}{\numberline {1.2.1}Imaging}{4}
\contentsline {subsubsection}{\numberline {1.2.2}Beamformed}{5}
\contentsline {section}{\numberline {2}Conclusions}{6}
\contentsline {subsection}{\numberline {2.1}References}{6}
\contentsline {section}{\numberline {A}Pseudo-code Implementation}{7}
