\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amssymb,amsmath,amsthm,amsfonts}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[left=2cm,right=2cm,bottom=2cm,top=2cm]{geometry}
\usepackage{mathrsfs}
\usepackage{graphicx}
\usepackage{float}
\usepackage{caption}
\usepackage{multicol}
\usepackage{subfig}
\usepackage{listings}
\usepackage{color}
\usepackage{appendix}
\usepackage{url}
\usepackage{natbib}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\graphicspath{source/}

\newcommand{\Rho}{\mathrm{P}}
\newcommand{\Chi}{\mathrm{X}}
\newcommand{\mo}[1]{\left<\hat{#1}\right>}
\newcommand{\infint}{\int^\infty_{-\infty}}
\newcommand{\ffint}{\frac{1}{\sqrt{2\pi}}\int^\infty_{-\infty}}
\newcommand{\deriv}[2]{\frac{\delta #1}{\delta #2}}
\newcommand{\dderiv}[2]{\frac{\delta^2 #1}{\delta #2^2}}
\newcommand{\commh}[2]{\left[\hat{#1}, \hat{#2}\right]}
\newcommand{\comm}[2]{\left[#1, #2\right]}
\newcommand{\mean}[1]{\left<#1\right>}
\newcommand{\AMatrix}[4]{\left(\begin{matrix} #1 & #2 \\ #3 & #4\end{matrix}\right)}
\newcommand{\VMatrix}[2]{\left(\begin{matrix} #1 \\ #2\end{matrix}\right)}
\newcommand{\sinc}{sinc}
\begin{document}
\title{Extracting Scintillation Indices from LOFAR Datasets}
\maketitle
\begin{abstract}
Something
\end{abstract}
\clearpage
\tableofcontents
\newpage
\section{Methodology}\label{sec:methodology}
With the aim of extracting scintillation indices from LOFAR observations, beamformed and imaging datasets were obtained simultaneously across several observations in Spring 2018. While the outputs of DPPP and WSClean cresult in a relatively low-noise image that can be directly sampled, the beamformed data requires the use of off sources beams to obtain an approximation of the signal. This piece will discuss the methodology used to implement the "variability" and "power spectrum" methodologies for extracting scintillation indices from observations at a high sampling rate. 

\subsection{The Variability Method}
With an aim to replicate the work of \citet{morgan_interplanetary_2018-1}, the imaging data was processed and a pipeline was created to extract scintillation indices through their "variability" method introduced as discussed in \cite[\S.~2.3]{morgan_interplanetary_2018-1}. 

This involves filtering the data series through a Butterworth filter to downweight the variations on frequencies outside of the expected interplanetary scintillation signature. This meant we filtered the data with a high pass filter of 0.2Hz and a low pass filter of 10Hz before performing any further analysis. 

The standard deviation of the data series is then taken to give a "variability image", where $P_{x,y}$ denotes a pixel with a known value at $x,\ y$ in the imaging data, or $P_n$ denotes the resulting beam with off source beam $n$ used as a reference. The mean ($\mu_P$) and standard deviation ($\sigma_P$) of the variability image is taken.

The scintillating flux density ($\Delta S_{x,y}$) can then be determined through equation \ref{eq:scintfluximg}, from the variability image and it's mean value.
\begin{equation}\label{eq:scintfluximg}
\Delta S_{x,y} = \sqrt{P^2_{x,y} - \mu^2_P}
\end{equation}
The scintillation index for the observation can then be determined from this scintillating flux density and the mean of the raw dataset for a given pixel ($\mu_{x,y}$), as shown in equation \ref{eq:scintindexvarimg}.
\begin{equation}\label{eq:scintindexvarimg}
m = \frac{\Delta S_{x,y}}{\mu_{x,y}}
\end{equation}
This methodology tends to overestimate the scintillation index in images as compared to other methodologies used.
\subsubsection{Imaging Process}
The image cube is passed through the pipeline, where the scintillating pixels are flagged by using the mean and standard deviation of the variability image as defined in \ref{eq:flagimg}. There are no other deviations from the methodology.
\begin{equation}\label{eq:flagimg}
P_{x,y} - \mu_{P} > 5\sigma_{P}\ \ \ \ \ \ 
\begin{cases} \text{True} & \text{Scintillating} \\
\text{False} & \text{Background}\end{cases}
\end{equation}
\subsubsection{Beamformed Adaptation}
To extract the source signal from beamformed data, both on-source ($T_{source}$) and off-source beams($T_{sys}$) were used to estimate the flux of a given source. Their dynamic spectra are percentile-sampled to give two 1D time series.

The off-source beam undergoes a rolling mean operation to approximate the background noise in the observation ($\mu_{sys}$). This filtered time series is subtracted from the raw time series of the off-source beam and the standard deviation of the result is taken ($\sigma_{sys}$).

The background mean is subtracted from the on-source beam to approximate a signal that contains only the source signal, without background contributions from the array and sky. The resulting signal's mean is sampled ($\mu_{source}$) and is passed through the pipeline similarly to the image cube, being filtered by a Butterworth bandpass attenuating variations outside of the 0.2hz to 10Hz range, with the standard deviation of the result representing a single pixel $P_n$ in the variability image. 

Unlike before, in order to calculate the scintillating flux density, $\sigma_{sys}$ is used to represent the standard deviation of the variability image as it only contains a single sample otherwise.

Equation \ref{eq:scintfluximg} can then be used to determine the scintillating flux density, using $\Delta S$ and $\mu_{source}$ in equation \ref{eq:scintindexvarimg}.

\subsection{The Power Spectrum Method}
High sampling rates are now available with the LOFAR core stations, allowing for the power spectrum method introduced by \citet{manoharan_three-dimensional_1993}, which was previously only applicable to single dish observations, to be applied to the taken observations.

As discussed in \cite[\S.~2]{manoharan_three-dimensional_1993}, the power spectrum of the signal can be integrated from a low frequency below the Fresnel knee to the noise floor of the observation. This was implemented by using Welch's method for obtaining the power spectrum utilizing 512 segments per window attenuated using a Kaiser window with a factor of 8.6 for all time series to supress power contributions from the sidelobes of the observation.

While the start of the data is chosen to be 0.2Hz (${f_0}$), in order to identify the end of the noise floor the spectrum is normalised in log space and a reversed sigmoid, as defined in equation \ref{eq:sigmoid}, is used to fit to the start of the noise floor ($f_c$) by detecting when the sigmoid drops below 3\% of the original power. The data set is temporarily modified for this operation by setting the minimum of the noise floor to the 70th percentile over the last 10\% of the data to improve the fit.
\begin{equation}\label{eq:sigmoid}
\sigma(f) = \frac{1}{1 + e^{a(x - c)}}
\end{equation}
Once the start and end of the power law has been identified, the power spectrum is integrated on the region to determine the scintillating flux density, which is then divided by the square of the mean intensity to find the squared scintillation index in equation \ref{eq:scintpower}
\begin{equation}\label{eq:scintpower}
m = \sqrt{\frac{\int^{f_c}_{f_0}P_f df}{\left<I\right>^2}}
\end{equation}
\subsubsection{Imaging}
In the case of the imaging data, we randomly sample time segments of the image cube and use a logical $AND$ operation between chunks to determine the scintillating pixels. The time series of these pixels are then fed through the pipeline described above to determine a scintillation index on a per-pixel level.
\subsubsection{Beamformed}
In order to process the beamformed data, the rolling mean filter is applied to the off-source beam ($\mu_R$). This is then subtracted from the on-source beam ($T_{source}$) after which the source mean is calculated ($\mu_{source}$) and passed through our pipeline to determine the source power ($P_{source}$). Before the scintillation index is calculated, equation \ref{eq:scintpower} is modified slightly to consider the power in the off source beam ($P_{off}$) which has similarly had the roling mean subtracted, redefining $P$ as seen in equation \ref{eq:newp}.
\begin{equation}\label{eq:newp}
P = P_{source} - P_{off}
\end{equation}
The scintillation index can then be calculated through equation \ref{eq:scintpower} by using the new definition for $P$ and $\mu_{source}$.
\newpage
\section{Conclusions}\label{sec:conclusion}

\subsection{References}\label{sec:ref}
\bibliographystyle{astron}
\bibliography{ref_scintillation.bib}
\newpage
\appendix
\section{Pseudo-code Implementation}\label{ap:fft}


\end{document}

